﻿namespace PGELibraryAddin
{
    partial class FormMPEResourceDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.chkInadvertent = new System.Windows.Forms.CheckBox();
            this.mperesourceBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.batchDataset = new PGELibraryAddin.BatchDataset();
            this.btnOK = new System.Windows.Forms.Button();
            this.lookukpmperpmBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pgeresourcesDataSet = new PGELibraryAddin.pgeresourcesDataSet();
            this.chkMonitored = new System.Windows.Forms.CheckBox();
            this.lookukpmperpmTableAdapter = new PGELibraryAddin.pgeresourcesDataSetTableAdapters.lookukpmperpmTableAdapter();
            this.mpereportTableAdapter1 = new PGELibraryAddin.BatchDatasetTableAdapters.mpereportTableAdapter();
            this.mperesourceTableAdapter = new PGELibraryAddin.BatchDatasetTableAdapters.mpeResourceTableAdapter();
            this.label2 = new System.Windows.Forms.Label();
            this.chkf11a = new System.Windows.Forms.CheckBox();
            this.chkf12 = new System.Windows.Forms.CheckBox();
            this.chkf131 = new System.Windows.Forms.CheckBox();
            this.chkf14 = new System.Windows.Forms.CheckBox();
            this.chkf15 = new System.Windows.Forms.CheckBox();
            this.chkf21a = new System.Windows.Forms.CheckBox();
            this.chkf21b = new System.Windows.Forms.CheckBox();
            this.chk22b1c = new System.Windows.Forms.CheckBox();
            this.chk22a5 = new System.Windows.Forms.CheckBox();
            this.chk22a4 = new System.Windows.Forms.CheckBox();
            this.chk22a3 = new System.Windows.Forms.CheckBox();
            this.chk22a1 = new System.Windows.Forms.CheckBox();
            this.chk21d = new System.Windows.Forms.CheckBox();
            this.chkf21c = new System.Windows.Forms.CheckBox();
            this.chk22b1j = new System.Windows.Forms.CheckBox();
            this.chk22b1i = new System.Windows.Forms.CheckBox();
            this.chk22b1g = new System.Windows.Forms.CheckBox();
            this.chk22b1d = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.mperesourceBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.batchDataset)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookukpmperpmBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pgeresourcesDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // chkInadvertent
            // 
            this.chkInadvertent.AutoSize = true;
            this.chkInadvertent.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.mperesourceBindingSource, "inadvertent", true));
            this.chkInadvertent.Location = new System.Drawing.Point(15, 531);
            this.chkInadvertent.Name = "chkInadvertent";
            this.chkInadvertent.Size = new System.Drawing.Size(130, 17);
            this.chkInadvertent.TabIndex = 0;
            this.chkInadvertent.Text = "Inadvertent Discovery";
            this.chkInadvertent.UseVisualStyleBackColor = true;
            this.chkInadvertent.CheckedChanged += new System.EventHandler(this.chkInadvertent_CheckedChanged);
            // 
            // mperesourceBindingSource
            // 
            this.mperesourceBindingSource.DataMember = "mperesource";
            this.mperesourceBindingSource.DataSource = this.batchDataset;
            // 
            // batchDataset
            // 
            this.batchDataset.DataSetName = "BatchDataset";
            this.batchDataset.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(232, 559);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 1;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // lookukpmperpmBindingSource
            // 
            this.lookukpmperpmBindingSource.DataMember = "lookukpmperpm";
            this.lookukpmperpmBindingSource.DataSource = this.pgeresourcesDataSet;
            // 
            // pgeresourcesDataSet
            // 
            this.pgeresourcesDataSet.DataSetName = "pgeresourcesDataSet";
            this.pgeresourcesDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // chkMonitored
            // 
            this.chkMonitored.AutoSize = true;
            this.chkMonitored.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.mperesourceBindingSource, "monitored", true));
            this.chkMonitored.Location = new System.Drawing.Point(15, 508);
            this.chkMonitored.Name = "chkMonitored";
            this.chkMonitored.Size = new System.Drawing.Size(73, 17);
            this.chkMonitored.TabIndex = 3;
            this.chkMonitored.Text = "Monitored";
            this.chkMonitored.UseVisualStyleBackColor = true;
            // 
            // lookukpmperpmTableAdapter
            // 
            this.lookukpmperpmTableAdapter.ClearBeforeFill = true;
            // 
            // mpereportTableAdapter1
            // 
            this.mpereportTableAdapter1.ClearBeforeFill = true;
            // 
            // mperesourceTableAdapter
            // 
            this.mperesourceTableAdapter.ClearBeforeFill = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "label2";
            // 
            // chkf11a
            // 
            this.chkf11a.AutoSize = true;
            this.chkf11a.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.mperesourceBindingSource, "f11a", true));
            this.chkf11a.Location = new System.Drawing.Point(15, 67);
            this.chkf11a.Name = "chkf11a";
            this.chkf11a.Size = new System.Drawing.Size(220, 17);
            this.chkf11a.TabIndex = 6;
            this.chkf11a.Text = "1.1(a) Avoidance; standard 20-foot buffer";
            this.chkf11a.UseVisualStyleBackColor = true;
            // 
            // chkf12
            // 
            this.chkf12.AutoSize = true;
            this.chkf12.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.mperesourceBindingSource, "f12", true));
            this.chkf12.Location = new System.Drawing.Point(15, 90);
            this.chkf12.Name = "chkf12";
            this.chkf12.Size = new System.Drawing.Size(281, 17);
            this.chkf12.TabIndex = 7;
            this.chkf12.Text = "1.2 Restrict activities to existing transportation systems";
            this.chkf12.UseVisualStyleBackColor = true;
            // 
            // chkf131
            // 
            this.chkf131.AutoSize = true;
            this.chkf131.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.mperesourceBindingSource, "f131", true));
            this.chkf131.Location = new System.Drawing.Point(15, 113);
            this.chkf131.Name = "chkf131";
            this.chkf131.Size = new System.Drawing.Size(186, 17);
            this.chkf131.TabIndex = 8;
            this.chkf131.Text = "1.3(1) Historic property delineation";
            this.chkf131.UseVisualStyleBackColor = true;
            // 
            // chkf14
            // 
            this.chkf14.AutoSize = true;
            this.chkf14.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.mperesourceBindingSource, "f14", true));
            this.chkf14.Location = new System.Drawing.Point(15, 136);
            this.chkf14.Name = "chkf14";
            this.chkf14.Size = new System.Drawing.Size(121, 17);
            this.chkf14.TabIndex = 9;
            this.chkf14.Text = "1.4 Project changes";
            this.chkf14.UseVisualStyleBackColor = true;
            // 
            // chkf15
            // 
            this.chkf15.AutoSize = true;
            this.chkf15.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.mperesourceBindingSource, "f15", true));
            this.chkf15.Location = new System.Drawing.Point(15, 159);
            this.chkf15.Name = "chkf15";
            this.chkf15.Size = new System.Drawing.Size(165, 17);
            this.chkf15.TabIndex = 10;
            this.chkf15.Text = "1.5 Archaeological monitoring";
            this.chkf15.UseVisualStyleBackColor = true;
            // 
            // chkf21a
            // 
            this.chkf21a.AutoSize = true;
            this.chkf21a.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.mperesourceBindingSource, "f21a", true));
            this.chkf21a.Location = new System.Drawing.Point(15, 182);
            this.chkf21a.Name = "chkf21a";
            this.chkf21a.Size = new System.Drawing.Size(205, 17);
            this.chkf21a.TabIndex = 11;
            this.chkf21a.Text = "2.1(a) Limit crossings of linear features";
            this.chkf21a.UseVisualStyleBackColor = true;
            // 
            // chkf21b
            // 
            this.chkf21b.AutoSize = true;
            this.chkf21b.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.mperesourceBindingSource, "f21b", true));
            this.chkf21b.Location = new System.Drawing.Point(15, 205);
            this.chkf21b.Name = "chkf21b";
            this.chkf21b.Size = new System.Drawing.Size(176, 17);
            this.chkf21b.TabIndex = 12;
            this.chkf21b.Text = "2.1(b) Sufficient snow coverage";
            this.chkf21b.UseVisualStyleBackColor = true;
            // 
            // chk22b1c
            // 
            this.chk22b1c.AutoSize = true;
            this.chk22b1c.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.mperesourceBindingSource, "f22b1c", true));
            this.chk22b1c.Location = new System.Drawing.Point(15, 366);
            this.chk22b1c.Name = "chk22b1c";
            this.chk22b1c.Size = new System.Drawing.Size(229, 17);
            this.chk22b1c.TabIndex = 21;
            this.chk22b1c.Text = "2.2(b)(1)(c) Vegetation removal (hand tools)";
            this.chk22b1c.UseVisualStyleBackColor = true;
            // 
            // chk22a5
            // 
            this.chk22a5.AutoSize = true;
            this.chk22a5.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.mperesourceBindingSource, "f22a5", true));
            this.chk22a5.Location = new System.Drawing.Point(15, 343);
            this.chk22a5.Name = "chk22a5";
            this.chk22a5.Size = new System.Drawing.Size(227, 17);
            this.chk22a5.TabIndex = 20;
            this.chk22a5.Text = "2.2(a)(5) No skidding or tracked equipment";
            this.chk22a5.UseVisualStyleBackColor = true;
            // 
            // chk22a4
            // 
            this.chk22a4.AutoSize = true;
            this.chk22a4.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.mperesourceBindingSource, "f22a4", true));
            this.chk22a4.Location = new System.Drawing.Point(15, 320);
            this.chk22a4.Name = "chk22a4";
            this.chk22a4.Size = new System.Drawing.Size(197, 17);
            this.chk22a4.TabIndex = 19;
            this.chk22a4.Text = "2.2(a)(4) Equipment operator training";
            this.chk22a4.UseVisualStyleBackColor = true;
            // 
            // chk22a3
            // 
            this.chk22a3.AutoSize = true;
            this.chk22a3.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.mperesourceBindingSource, "f22a3", true));
            this.chk22a3.Location = new System.Drawing.Point(15, 297);
            this.chk22a3.Name = "chk22a3";
            this.chk22a3.Size = new System.Drawing.Size(292, 17);
            this.chk22a3.TabIndex = 18;
            this.chk22a3.Text = "2.2(a)(3) Tree removal by non-ground-disturbing methods";
            this.chk22a3.UseVisualStyleBackColor = true;
            // 
            // chk22a1
            // 
            this.chk22a1.AutoSize = true;
            this.chk22a1.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.mperesourceBindingSource, "f22a1", true));
            this.chk22a1.Location = new System.Drawing.Point(15, 274);
            this.chk22a1.Name = "chk22a1";
            this.chk22a1.Size = new System.Drawing.Size(170, 17);
            this.chk22a1.TabIndex = 17;
            this.chk22a1.Text = "2.2(a)(1) Topping/limbing trees";
            this.chk22a1.UseVisualStyleBackColor = true;
            // 
            // chk21d
            // 
            this.chk21d.AutoSize = true;
            this.chk21d.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.mperesourceBindingSource, "f21d", true));
            this.chk21d.Location = new System.Drawing.Point(15, 251);
            this.chk21d.Name = "chk21d";
            this.chk21d.Size = new System.Drawing.Size(141, 17);
            this.chk21d.TabIndex = 15;
            this.chk21d.Text = "2.1(d) Protective barriers";
            this.chk21d.UseVisualStyleBackColor = true;
            // 
            // chkf21c
            // 
            this.chkf21c.AutoSize = true;
            this.chkf21c.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.mperesourceBindingSource, "f21c", true));
            this.chkf21c.Location = new System.Drawing.Point(15, 228);
            this.chkf21c.Name = "chkf21c";
            this.chkf21c.Size = new System.Drawing.Size(218, 17);
            this.chkf21c.TabIndex = 14;
            this.chkf21c.Text = "2.1(c) Foreign material surface protection";
            this.chkf21c.UseVisualStyleBackColor = true;
            // 
            // chk22b1j
            // 
            this.chk22b1j.AutoSize = true;
            this.chk22b1j.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.mperesourceBindingSource, "f22b1j", true));
            this.chk22b1j.Location = new System.Drawing.Point(15, 466);
            this.chk22b1j.Name = "chk22b1j";
            this.chk22b1j.Size = new System.Drawing.Size(190, 17);
            this.chk22b1j.TabIndex = 25;
            this.chk22b1j.Text = "2.2(b)(1)(j) Chipping woody material";
            this.chk22b1j.UseVisualStyleBackColor = true;
            // 
            // chk22b1i
            // 
            this.chk22b1i.AutoSize = true;
            this.chk22b1i.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.mperesourceBindingSource, "f22b1i", true));
            this.chk22b1i.Location = new System.Drawing.Point(15, 435);
            this.chk22b1i.Name = "chk22b1i";
            this.chk22b1i.Size = new System.Drawing.Size(229, 30);
            this.chk22b1i.TabIndex = 24;
            this.chk22b1i.Text = "2.2(b)(1)(i) Vegetation removal (hand carry, \r\noffsite equipment, or rubber tired" +
    " vehicle)";
            this.chk22b1i.UseVisualStyleBackColor = true;
            // 
            // chk22b1g
            // 
            this.chk22b1g.AutoSize = true;
            this.chk22b1g.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.mperesourceBindingSource, "f22b1g", true));
            this.chk22b1g.Location = new System.Drawing.Point(15, 412);
            this.chk22b1g.Name = "chk22b1g";
            this.chk22b1g.Size = new System.Drawing.Size(216, 17);
            this.chk22b1g.TabIndex = 23;
            this.chk22b1g.Text = "2.2(b)(1)(g) Hazard tree directional felling";
            this.chk22b1g.UseVisualStyleBackColor = true;
            // 
            // chk22b1d
            // 
            this.chk22b1d.AutoSize = true;
            this.chk22b1d.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.mperesourceBindingSource, "f22b1d", true));
            this.chk22b1d.Location = new System.Drawing.Point(15, 389);
            this.chk22b1d.Name = "chk22b1d";
            this.chk22b1d.Size = new System.Drawing.Size(236, 17);
            this.chk22b1d.TabIndex = 22;
            this.chk22b1d.Text = "2.2(b)(1)(d) Protective materials or equipment";
            this.chk22b1d.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(153, 13);
            this.label1.TabIndex = 26;
            this.label1.Text = "Resource Protection Measures";
            // 
            // FormMPEResourceDetail
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(318, 599);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chk22b1j);
            this.Controls.Add(this.chk22b1i);
            this.Controls.Add(this.chk22b1g);
            this.Controls.Add(this.chk22b1d);
            this.Controls.Add(this.chk22b1c);
            this.Controls.Add(this.chk22a5);
            this.Controls.Add(this.chk22a4);
            this.Controls.Add(this.chk22a3);
            this.Controls.Add(this.chk22a1);
            this.Controls.Add(this.chk21d);
            this.Controls.Add(this.chkf21c);
            this.Controls.Add(this.chkf21b);
            this.Controls.Add(this.chkf21a);
            this.Controls.Add(this.chkf15);
            this.Controls.Add(this.chkf14);
            this.Controls.Add(this.chkf131);
            this.Controls.Add(this.chkf12);
            this.Controls.Add(this.chkf11a);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.chkMonitored);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.chkInadvertent);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormMPEResourceDetail";
            this.Text = "MPE Resource Detail";
            this.Load += new System.EventHandler(this.FormMPEResourceDetail_Load);
            ((System.ComponentModel.ISupportInitialize)(this.mperesourceBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.batchDataset)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookukpmperpmBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pgeresourcesDataSet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chkInadvertent;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.CheckBox chkMonitored;
        private pgeresourcesDataSet pgeresourcesDataSet;
        private System.Windows.Forms.BindingSource lookukpmperpmBindingSource;
        private pgeresourcesDataSetTableAdapters.lookukpmperpmTableAdapter lookukpmperpmTableAdapter;
        private BatchDatasetTableAdapters.mpereportTableAdapter mpereportTableAdapter1;
        private BatchDataset batchDataset;
        private System.Windows.Forms.BindingSource mperesourceBindingSource;
        private BatchDatasetTableAdapters.mpeResourceTableAdapter mperesourceTableAdapter;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox chk22b1j;
        private System.Windows.Forms.CheckBox chk22b1i;
        private System.Windows.Forms.CheckBox chk22b1g;
        private System.Windows.Forms.CheckBox chk22b1d;
        private System.Windows.Forms.CheckBox chk22b1c;
        private System.Windows.Forms.CheckBox chk22a5;
        private System.Windows.Forms.CheckBox chk22a4;
        private System.Windows.Forms.CheckBox chk22a3;
        private System.Windows.Forms.CheckBox chk22a1;
        private System.Windows.Forms.CheckBox chk21d;
        private System.Windows.Forms.CheckBox chkf21c;
        private System.Windows.Forms.CheckBox chkf21b;
        private System.Windows.Forms.CheckBox chkf21a;
        private System.Windows.Forms.CheckBox chkf15;
        private System.Windows.Forms.CheckBox chkf14;
        private System.Windows.Forms.CheckBox chkf131;
        private System.Windows.Forms.CheckBox chkf12;
        private System.Windows.Forms.CheckBox chkf11a;
        private System.Windows.Forms.Label label1;
    }
}