﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;


namespace PGELibraryAddin
{
    public class btnShowSettings : ESRI.ArcGIS.Desktop.AddIns.Button
    {
        FormSettings f;
        public btnShowSettings()
        {
        }

        protected override void OnClick()
        {
            f = new FormSettings();
            f.ShowDialog(System.Windows.Forms.Control.FromHandle((IntPtr)ArcMap.Application.hWnd));

        }

        protected override void OnUpdate()
        {
        }
    }
}
