﻿using System;
using System.Windows.Forms;

namespace PGELibraryAddin
{
	partial class DockPGEData
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label lblComment;
            System.Windows.Forms.Label lblTempTitle;
            System.Windows.Forms.Label lblOrigPath;
            System.Windows.Forms.Label lblPDF;
            System.Windows.Forms.Label lblLinearWidth;
            System.Windows.Forms.Label lblReportArea;
            System.Windows.Forms.Label lblPreparedFor;
            System.Windows.Forms.Label lblLinearBlock;
            System.Windows.Forms.Label lblAuthor;
            System.Windows.Forms.Label lblReportLabel;
            System.Windows.Forms.Label lblAgencyCompany;
            System.Windows.Forms.Label lblYear;
            System.Windows.Forms.Label lblReportTitle;
            System.Windows.Forms.Label lblReportNumber;
            System.Windows.Forms.Label lblReportID;
            System.Windows.Forms.Label label13;
            System.Windows.Forms.Label label14;
            System.Windows.Forms.Label siteidLabel;
            System.Windows.Forms.Label othernameLabel;
            System.Windows.Forms.Label ageLabel;
            System.Windows.Forms.Label descriptionLabel;
            System.Windows.Forms.Label nrhp_eligibilityLabel;
            System.Windows.Forms.Label areaLabel;
            System.Windows.Forms.Label land_ownerLabel;
            System.Windows.Forms.Label commentsLabel;
            System.Windows.Forms.Label pdfLabel;
            System.Windows.Forms.Label trinhLabel;
            System.Windows.Forms.Label label8;
            System.Windows.Forms.Label label7;
            System.Windows.Forms.Label label6;
            System.Windows.Forms.Label hrinumberLabel;
            System.Windows.Forms.Label label9;
            System.Windows.Forms.Label label24;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DockPGEData));
            this.batchResourceBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.batchDataset1 = new PGELibraryAddin.BatchDataset();
            this.lookupcountycodesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pgeresourcesDataSet = new PGELibraryAddin.pgeresourcesDataSet();
            this.lookupeligibilityBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.batchReportBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lookupagencyBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.resourcesTableAdapter = new PGELibraryAddin.BatchDatasetTableAdapters.resourcesTableAdapter();
            this.tableAdapterManager1 = new PGELibraryAddin.BatchDatasetTableAdapters.TableAdapterManager();
            this.reportsTableAdapter = new PGELibraryAddin.BatchDatasetTableAdapters.reportsTableAdapter();
            this.tableAdapterManager = new PGELibraryAddin.BatchDatasetTableAdapters.TableAdapterManager();
            this.npgsqlCommand1 = new Npgsql.NpgsqlCommand();
            this.lookupagencyTableAdapter = new PGELibraryAddin.pgeresourcesDataSetTableAdapters.lookupagencyTableAdapter();
            this.lookupeligibilityTableAdapter = new PGELibraryAddin.pgeresourcesDataSetTableAdapters.lookupeligibilityTableAdapter();
            this.ofdPDF = new System.Windows.Forms.OpenFileDialog();
            this.lookupcountycodesTableAdapter1 = new PGELibraryAddin.pgeresourcesDataSetTableAdapters.lookupcountycodesTableAdapter();
            this.lookupReportTypebindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tableAdapterManager2 = new PGELibraryAddin.pgeresourcesDataSetTableAdapters.TableAdapterManager();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.buttonImportSitePDFs = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.optReportNumber = new System.Windows.Forms.RadioButton();
            this.optReportTitle = new System.Windows.Forms.RadioButton();
            this.button2 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.btnComplete = new System.Windows.Forms.Button();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.checkExistingVersion = new System.Windows.Forms.CheckBox();
            this.buttonSetVersion = new System.Windows.Forms.Button();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.comboBatchNames = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonReload = new System.Windows.Forms.Button();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.checkNewVersion = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.textNewBatchName = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textFWJobNumber = new System.Windows.Forms.TextBox();
            this.textPGENumber = new System.Windows.Forms.TextBox();
            this.textComment = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.buttonCreateVersion = new System.Windows.Forms.Button();
            this.txtBatchPath = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.resourceTabCntl = new System.Windows.Forms.TabControl();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.buttonRefresh1 = new System.Windows.Forms.Button();
            this.btnAddFilter = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.drpSelection = new System.Windows.Forms.ComboBox();
            this.sitesDataGridView = new System.Windows.Forms.DataGridView();
            this.resourceidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.primcoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.primnoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hrinumberDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.trinnoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.trinno2DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.trinhDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fsregionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fsforestDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fsnump1DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fsnump2DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.othernameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pdfDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ageDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descriptionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nrhpeligibilityDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.resareaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.landownerDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.commentsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.preeligDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.histeligDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sitedatastatusDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.origfilepathDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.origsiteidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.createduserDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.createddateDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastediteduserDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastediteddateDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fslabelDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reportgroupDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnFilter = new System.Windows.Forms.Button();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.lblFilter = new System.Windows.Forms.Label();
            this.chkMissingSiteGIS = new System.Windows.Forms.CheckBox();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.chkSitesMatchingDisabled = new System.Windows.Forms.CheckBox();
            this.label15 = new System.Windows.Forms.Label();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.btnSitesRefreshMatch = new System.Windows.Forms.Button();
            this.btnOpenOrigPDf = new System.Windows.Forms.Button();
            this.textOrigPDF = new System.Windows.Forms.TextBox();
            this.areaTextBox = new System.Windows.Forms.TextBox();
            this.pdfTextBox = new System.Windows.Forms.TextBox();
            this.btnAddResourceReportRef = new System.Windows.Forms.TextBox();
            this.descriptionTextBox = new System.Windows.Forms.TextBox();
            this.othernameTextBox = new System.Windows.Forms.TextBox();
            this.siteidTextBox = new System.Windows.Forms.TextBox();
            this.btnSelectSitePDF = new System.Windows.Forms.Button();
            this.btnOpenSitePDF = new System.Windows.Forms.Button();
            this.ageTextBox = new System.Windows.Forms.ComboBox();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.dgvResourceReport = new System.Windows.Forms.DataGridView();
            this.btnResourceAddReport = new System.Windows.Forms.Button();
            this.tabPage12 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.button1 = new System.Windows.Forms.Button();
            this.dgvResourceXref = new System.Windows.Forms.DataGridView();
            this.tabPage17 = new System.Windows.Forms.TabPage();
            this.land_ownerTextBox = new System.Windows.Forms.ComboBox();
            this.nrhp_eligibilityTextBox = new System.Windows.Forms.ComboBox();
            this.tabControl3 = new System.Windows.Forms.TabControl();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.hrinumberTextBox = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.tabPage11 = new System.Windows.Forms.TabPage();
            this.label10 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.trinhTextBox = new System.Windows.Forms.ComboBox();
            this.txtfsnump2 = new System.Windows.Forms.TextBox();
            this.txtfsnump1 = new System.Windows.Forms.TextBox();
            this.txtfsforest = new System.Windows.Forms.TextBox();
            this.txtfsregion = new System.Windows.Forms.TextBox();
            this.txttrinno = new System.Windows.Forms.TextBox();
            this.txtprimno = new System.Windows.Forms.TextBox();
            this.primcoLabel = new System.Windows.Forms.Label();
            this.fsnump2Label = new System.Windows.Forms.Label();
            this.fsnump1Label = new System.Windows.Forms.Label();
            this.forestLabel = new System.Windows.Forms.Label();
            this.fsregionLabel = new System.Windows.Forms.Label();
            this.trinnoLabel = new System.Windows.Forms.Label();
            this.primnoLabel = new System.Windows.Forms.Label();
            this.txtprimco = new System.Windows.Forms.ComboBox();
            this.bindingNavigator1 = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.reportList = new System.Windows.Forms.TabControl();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.chkMissingReportGIS = new System.Windows.Forms.CheckBox();
            this.buttonRefresh2 = new System.Windows.Forms.Button();
            this.btnReportAddFilter = new System.Windows.Forms.Button();
            this.btnReportClearFilter = new System.Windows.Forms.Button();
            this.btnReportFilter = new System.Windows.Forms.Button();
            this.txtReportSelectValue = new System.Windows.Forms.TextBox();
            this.drpReportSelect = new System.Windows.Forms.ComboBox();
            this.lblReportFilter = new System.Windows.Forms.Label();
            this.reportsDataGridView = new System.Windows.Forms.DataGridView();
            this.reportidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.labelDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reportnumberDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reporttitleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.yearDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.authorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.agencycompanyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.preparedforDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.linearblockDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.linearwidthDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reportareaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.areaunitsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.transectspacingDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.silDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.commentsDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.temptitleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reporttypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pdfDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.origpathDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reportdatastatusDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sitevisitDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.createdbyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.createddateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.editedbyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.editeddateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.origreportidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.createduserDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.createddateDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastediteduserDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastediteddateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabReportDetail = new System.Windows.Forms.TabPage();
            this.chkReportMatchDisabled = new System.Windows.Forms.CheckBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.comboReportStatus = new System.Windows.Forms.ComboBox();
            this.comboReportGroup = new System.Windows.Forms.ComboBox();
            this.cboReportType = new System.Windows.Forms.ComboBox();
            this.chkSiteVisit = new System.Windows.Forms.CheckBox();
            this.btnReportsUpdateMatch = new System.Windows.Forms.Button();
            this.btnOpenOrigReportPDF = new System.Windows.Forms.Button();
            this.btnSelectReportPDF = new System.Windows.Forms.Button();
            this.btnOpenReportPDF = new System.Windows.Forms.Button();
            this.tabControl4 = new System.Windows.Forms.TabControl();
            this.tabPage13 = new System.Windows.Forms.TabPage();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.tabPage14 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.tnReportAddResource = new System.Windows.Forms.Button();
            this.dgvReportResource = new System.Windows.Forms.DataGridView();
            this.tabPage15 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.btnReportAddXref = new System.Windows.Forms.Button();
            this.dgvReportXref = new System.Windows.Forms.DataGridView();
            this.txtComment = new System.Windows.Forms.TextBox();
            this.txtOrigPath = new System.Windows.Forms.TextBox();
            this.txtSil = new System.Windows.Forms.ComboBox();
            this.lookupSILBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.txtTransectSpacing = new System.Windows.Forms.TextBox();
            this.lblAreaUnits = new System.Windows.Forms.Label();
            this.lblReportType = new System.Windows.Forms.Label();
            this.lblSil = new System.Windows.Forms.Label();
            this.lblTransectSpacing = new System.Windows.Forms.Label();
            this.txtAreaUnits = new System.Windows.Forms.TextBox();
            this.txtReportArea = new System.Windows.Forms.TextBox();
            this.txtPDF = new System.Windows.Forms.TextBox();
            this.txtLinearWidth = new System.Windows.Forms.TextBox();
            this.txtLinearBlock = new System.Windows.Forms.TextBox();
            this.txtPreparedFor = new System.Windows.Forms.ComboBox();
            this.txtAuthor = new System.Windows.Forms.TextBox();
            this.txtReportLabel = new System.Windows.Forms.TextBox();
            this.txtAgencyCompany = new System.Windows.Forms.ComboBox();
            this.txtYear = new System.Windows.Forms.TextBox();
            this.txtReportTitle = new System.Windows.Forms.TextBox();
            this.txtReportNumber = new System.Windows.Forms.TextBox();
            this.txtReportID = new System.Windows.Forms.TextBox();
            this.bindingNavigator2 = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem1 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem1 = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem1 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem1 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem1 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem1 = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem1 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem1 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.tabReportMPE = new System.Windows.Forms.TabPage();
            this.btnDeleteMPERecord = new System.Windows.Forms.Button();
            this.btnAddMPERecord = new System.Windows.Forms.Button();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.txtNonintensiveAcres = new System.Windows.Forms.TextBox();
            this.mpereportBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.txtIntensiveAcres = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.cboForest = new System.Windows.Forms.ComboBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.txtNotificationID = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.notificationDate = new System.Windows.Forms.DateTimePicker();
            this.txtReportIDMPE = new System.Windows.Forms.TextBox();
            this.btnAddMPEResource = new System.Windows.Forms.Button();
            this.dgvPersonnel = new System.Windows.Forms.DataGridView();
            this.dgvMPEResource = new System.Windows.Forms.DataGridView();
            this.btnAddMPEPerson = new System.Windows.Forms.Button();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtNumSites = new System.Windows.Forms.TextBox();
            this.cboClass = new System.Windows.Forms.ComboBox();
            this.cboActivity = new System.Windows.Forms.ComboBox();
            this.bindingSourceActivity = new System.Windows.Forms.BindingSource(this.components);
            this.chkActMonitor = new System.Windows.Forms.CheckBox();
            this.surveyStart = new System.Windows.Forms.DateTimePicker();
            this.surveyEnd = new System.Windows.Forms.DateTimePicker();
            this.txtContraints = new System.Windows.Forms.TextBox();
            this.chkActTest = new System.Windows.Forms.CheckBox();
            this.chkActSurvey = new System.Windows.Forms.CheckBox();
            this.chkActDesktop = new System.Windows.Forms.CheckBox();
            this.tabPage16 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.dgvMatchTable = new System.Windows.Forms.DataGridView();
            this.dgvGISTable = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel14 = new System.Windows.Forms.TableLayoutPanel();
            this.cboMatchType = new System.Windows.Forms.ComboBox();
            this.btnLoadGIS = new System.Windows.Forms.Button();
            this.cboGISLayer = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.lookuppersonnelBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mperesourceBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mpepersonnelBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gisTableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.lookupreporttypeTableAdapter = new PGELibraryAddin.pgeresourcesDataSetTableAdapters.lookupreporttypeTableAdapter();
            this.mpereportTableAdapter1 = new PGELibraryAddin.BatchDatasetTableAdapters.mpereportTableAdapter();
            this.mpepersonnelTableAdapter = new PGELibraryAddin.BatchDatasetTableAdapters.mpepersonnelTableAdapter();
            this.lookuppersonnelTableAdapter = new PGELibraryAddin.pgeresourcesDataSetTableAdapters.lookuppersonnelTableAdapter();
            this.ConfigDS = new System.Data.DataSet();
            this.MPEResource = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn2 = new System.Data.DataColumn();
            this.dataColumn3 = new System.Data.DataColumn();
            this.dataColumn4 = new System.Data.DataColumn();
            this.MPEResourceColumns = new System.Data.DataTable();
            this.dataColumn5 = new System.Data.DataColumn();
            this.dataColumn6 = new System.Data.DataColumn();
            this.dataColumn7 = new System.Data.DataColumn();
            this.lookupmpeactivityTableAdapter = new PGELibraryAddin.pgeresourcesDataSetTableAdapters.lookupmpeactivityTableAdapter();
            this.lookupSILTableAdapter = new PGELibraryAddin.pgeresourcesDataSetTableAdapters.lookupSILTableAdapter();
            lblComment = new System.Windows.Forms.Label();
            lblTempTitle = new System.Windows.Forms.Label();
            lblOrigPath = new System.Windows.Forms.Label();
            lblPDF = new System.Windows.Forms.Label();
            lblLinearWidth = new System.Windows.Forms.Label();
            lblReportArea = new System.Windows.Forms.Label();
            lblPreparedFor = new System.Windows.Forms.Label();
            lblLinearBlock = new System.Windows.Forms.Label();
            lblAuthor = new System.Windows.Forms.Label();
            lblReportLabel = new System.Windows.Forms.Label();
            lblAgencyCompany = new System.Windows.Forms.Label();
            lblYear = new System.Windows.Forms.Label();
            lblReportTitle = new System.Windows.Forms.Label();
            lblReportNumber = new System.Windows.Forms.Label();
            lblReportID = new System.Windows.Forms.Label();
            label13 = new System.Windows.Forms.Label();
            label14 = new System.Windows.Forms.Label();
            siteidLabel = new System.Windows.Forms.Label();
            othernameLabel = new System.Windows.Forms.Label();
            ageLabel = new System.Windows.Forms.Label();
            descriptionLabel = new System.Windows.Forms.Label();
            nrhp_eligibilityLabel = new System.Windows.Forms.Label();
            areaLabel = new System.Windows.Forms.Label();
            land_ownerLabel = new System.Windows.Forms.Label();
            commentsLabel = new System.Windows.Forms.Label();
            pdfLabel = new System.Windows.Forms.Label();
            trinhLabel = new System.Windows.Forms.Label();
            label8 = new System.Windows.Forms.Label();
            label7 = new System.Windows.Forms.Label();
            label6 = new System.Windows.Forms.Label();
            hrinumberLabel = new System.Windows.Forms.Label();
            label9 = new System.Windows.Forms.Label();
            label24 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.batchResourceBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.batchDataset1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupcountycodesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pgeresourcesDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupeligibilityBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.batchReportBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupagencyBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupReportTypebindingSource)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            this.tableLayoutPanel12.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.resourceTabCntl.SuspendLayout();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sitesDataGridView)).BeginInit();
            this.tabPage5.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.tabPage9.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvResourceReport)).BeginInit();
            this.tabPage12.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvResourceXref)).BeginInit();
            this.tabControl3.SuspendLayout();
            this.tabPage10.SuspendLayout();
            this.tabPage11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).BeginInit();
            this.bindingNavigator1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.reportList.SuspendLayout();
            this.tabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.reportsDataGridView)).BeginInit();
            this.tabReportDetail.SuspendLayout();
            this.tabControl4.SuspendLayout();
            this.tabPage13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.tabPage14.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReportResource)).BeginInit();
            this.tabPage15.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReportXref)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupSILBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator2)).BeginInit();
            this.bindingNavigator2.SuspendLayout();
            this.tabReportMPE.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mpereportBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPersonnel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMPEResource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceActivity)).BeginInit();
            this.tabPage16.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMatchTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGISTable)).BeginInit();
            this.tableLayoutPanel14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookuppersonnelBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mperesourceBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mpepersonnelBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gisTableBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConfigDS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MPEResource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MPEResourceColumns)).BeginInit();
            this.SuspendLayout();
            // 
            // lblComment
            // 
            lblComment.AutoSize = true;
            lblComment.Location = new System.Drawing.Point(13, 221);
            lblComment.Name = "lblComment";
            lblComment.Size = new System.Drawing.Size(53, 13);
            lblComment.TabIndex = 88;
            lblComment.Text = "comment:";
            // 
            // lblTempTitle
            // 
            lblTempTitle.AutoSize = true;
            lblTempTitle.Location = new System.Drawing.Point(42, 301);
            lblTempTitle.Name = "lblTempTitle";
            lblTempTitle.Size = new System.Drawing.Size(0, 13);
            lblTempTitle.TabIndex = 86;
            // 
            // lblOrigPath
            // 
            lblOrigPath.AutoSize = true;
            lblOrigPath.Location = new System.Drawing.Point(15, 261);
            lblOrigPath.Name = "lblOrigPath";
            lblOrigPath.Size = new System.Drawing.Size(51, 13);
            lblOrigPath.TabIndex = 84;
            lblOrigPath.Text = "orig path:";
            // 
            // lblPDF
            // 
            lblPDF.AutoSize = true;
            lblPDF.Location = new System.Drawing.Point(41, 236);
            lblPDF.Name = "lblPDF";
            lblPDF.Size = new System.Drawing.Size(25, 13);
            lblPDF.TabIndex = 56;
            lblPDF.Text = "pdf:";
            // 
            // lblLinearWidth
            // 
            lblLinearWidth.AutoSize = true;
            lblLinearWidth.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblLinearWidth.Location = new System.Drawing.Point(316, 172);
            lblLinearWidth.Name = "lblLinearWidth";
            lblLinearWidth.Size = new System.Drawing.Size(35, 26);
            lblLinearWidth.TabIndex = 52;
            lblLinearWidth.Text = "linear\r\nwidth:";
            // 
            // lblReportArea
            // 
            lblReportArea.AutoSize = true;
            lblReportArea.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblReportArea.Location = new System.Drawing.Point(16, 148);
            lblReportArea.Name = "lblReportArea";
            lblReportArea.Size = new System.Drawing.Size(34, 26);
            lblReportArea.TabIndex = 51;
            lblReportArea.Text = "report\r\narea:";
            // 
            // lblPreparedFor
            // 
            lblPreparedFor.AutoSize = true;
            lblPreparedFor.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblPreparedFor.Location = new System.Drawing.Point(345, 98);
            lblPreparedFor.Name = "lblPreparedFor";
            lblPreparedFor.Size = new System.Drawing.Size(70, 13);
            lblPreparedFor.TabIndex = 46;
            lblPreparedFor.Text = "prepared_for:";
            // 
            // lblLinearBlock
            // 
            lblLinearBlock.AutoSize = true;
            lblLinearBlock.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblLinearBlock.Location = new System.Drawing.Point(168, 172);
            lblLinearBlock.Name = "lblLinearBlock";
            lblLinearBlock.Size = new System.Drawing.Size(34, 26);
            lblLinearBlock.TabIndex = 45;
            lblLinearBlock.Text = "linear\r\nblock:";
            lblLinearBlock.Click += new System.EventHandler(this.lblLinearBlock_Click);
            // 
            // lblAuthor
            // 
            lblAuthor.AutoSize = true;
            lblAuthor.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblAuthor.Location = new System.Drawing.Point(24, 76);
            lblAuthor.Name = "lblAuthor";
            lblAuthor.Size = new System.Drawing.Size(40, 13);
            lblAuthor.TabIndex = 43;
            lblAuthor.Text = "author:";
            // 
            // lblReportLabel
            // 
            lblReportLabel.AutoSize = true;
            lblReportLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblReportLabel.Location = new System.Drawing.Point(32, 13);
            lblReportLabel.Name = "lblReportLabel";
            lblReportLabel.Size = new System.Drawing.Size(32, 13);
            lblReportLabel.TabIndex = 41;
            lblReportLabel.Text = "label:";
            // 
            // lblAgencyCompany
            // 
            lblAgencyCompany.AutoSize = true;
            lblAgencyCompany.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblAgencyCompany.Location = new System.Drawing.Point(11, 95);
            lblAgencyCompany.Name = "lblAgencyCompany";
            lblAgencyCompany.Size = new System.Drawing.Size(52, 26);
            lblAgencyCompany.TabIndex = 37;
            lblAgencyCompany.Text = "agency\r\ncompany:";
            // 
            // lblYear
            // 
            lblYear.AutoSize = true;
            lblYear.Location = new System.Drawing.Point(552, 73);
            lblYear.Name = "lblYear";
            lblYear.Size = new System.Drawing.Size(30, 13);
            lblYear.TabIndex = 35;
            lblYear.Text = "year:";
            // 
            // lblReportTitle
            // 
            lblReportTitle.AutoSize = true;
            lblReportTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblReportTitle.Location = new System.Drawing.Point(8, 36);
            lblReportTitle.Name = "lblReportTitle";
            lblReportTitle.Size = new System.Drawing.Size(56, 13);
            lblReportTitle.TabIndex = 33;
            lblReportTitle.Text = "report title:";
            // 
            // lblReportNumber
            // 
            lblReportNumber.AutoSize = true;
            lblReportNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblReportNumber.Location = new System.Drawing.Point(168, 13);
            lblReportNumber.Name = "lblReportNumber";
            lblReportNumber.Size = new System.Drawing.Size(75, 13);
            lblReportNumber.TabIndex = 31;
            lblReportNumber.Text = "report number:";
            // 
            // lblReportID
            // 
            lblReportID.AutoSize = true;
            lblReportID.Location = new System.Drawing.Point(552, 6);
            lblReportID.Name = "lblReportID";
            lblReportID.Size = new System.Drawing.Size(45, 13);
            lblReportID.TabIndex = 29;
            lblReportID.Text = "reportid:";
            // 
            // label13
            // 
            label13.AutoSize = true;
            label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label13.Location = new System.Drawing.Point(473, 176);
            label13.Name = "label13";
            label13.Size = new System.Drawing.Size(61, 13);
            label13.TabIndex = 0;
            label13.Text = "adequate?:";
            // 
            // label14
            // 
            label14.AutoSize = true;
            label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label14.Location = new System.Drawing.Point(343, 13);
            label14.Name = "label14";
            label14.Size = new System.Drawing.Size(58, 13);
            label14.TabIndex = 101;
            label14.Text = "short desc:";
            // 
            // siteidLabel
            // 
            siteidLabel.AutoSize = true;
            siteidLabel.Location = new System.Drawing.Point(591, 110);
            siteidLabel.Name = "siteidLabel";
            siteidLabel.Size = new System.Drawing.Size(59, 13);
            siteidLabel.TabIndex = 15;
            siteidLabel.Text = "resourceid:";
            // 
            // othernameLabel
            // 
            othernameLabel.AutoSize = true;
            othernameLabel.Location = new System.Drawing.Point(12, 76);
            othernameLabel.Name = "othernameLabel";
            othernameLabel.Size = new System.Drawing.Size(60, 13);
            othernameLabel.TabIndex = 21;
            othernameLabel.Text = "othername:";
            // 
            // ageLabel
            // 
            ageLabel.AutoSize = true;
            ageLabel.Location = new System.Drawing.Point(186, 95);
            ageLabel.Name = "ageLabel";
            ageLabel.Size = new System.Drawing.Size(53, 13);
            ageLabel.TabIndex = 22;
            ageLabel.Text = "age/type:";
            // 
            // descriptionLabel
            // 
            descriptionLabel.AutoSize = true;
            descriptionLabel.Location = new System.Drawing.Point(11, 143);
            descriptionLabel.Name = "descriptionLabel";
            descriptionLabel.Size = new System.Drawing.Size(61, 13);
            descriptionLabel.TabIndex = 23;
            descriptionLabel.Text = "description:";
            // 
            // nrhp_eligibilityLabel
            // 
            nrhp_eligibilityLabel.AutoSize = true;
            nrhp_eligibilityLabel.Location = new System.Drawing.Point(1, 105);
            nrhp_eligibilityLabel.Name = "nrhp_eligibilityLabel";
            nrhp_eligibilityLabel.Size = new System.Drawing.Size(72, 13);
            nrhp_eligibilityLabel.TabIndex = 24;
            nrhp_eligibilityLabel.Text = "nrhp eligibility:";
            // 
            // areaLabel
            // 
            areaLabel.AutoSize = true;
            areaLabel.Location = new System.Drawing.Point(185, 121);
            areaLabel.Name = "areaLabel";
            areaLabel.Size = new System.Drawing.Size(31, 13);
            areaLabel.TabIndex = 25;
            areaLabel.Text = "area:";
            // 
            // land_ownerLabel
            // 
            land_ownerLabel.AutoSize = true;
            land_ownerLabel.Location = new System.Drawing.Point(11, 120);
            land_ownerLabel.Name = "land_ownerLabel";
            land_ownerLabel.Size = new System.Drawing.Size(62, 13);
            land_ownerLabel.TabIndex = 26;
            land_ownerLabel.Text = "land owner:";
            // 
            // commentsLabel
            // 
            commentsLabel.AutoSize = true;
            commentsLabel.Location = new System.Drawing.Point(14, 194);
            commentsLabel.Name = "commentsLabel";
            commentsLabel.Size = new System.Drawing.Size(58, 13);
            commentsLabel.TabIndex = 27;
            commentsLabel.Text = "comments:";
            // 
            // pdfLabel
            // 
            pdfLabel.AutoSize = true;
            pdfLabel.Location = new System.Drawing.Point(47, 231);
            pdfLabel.Name = "pdfLabel";
            pdfLabel.Size = new System.Drawing.Size(25, 13);
            pdfLabel.TabIndex = 28;
            pdfLabel.Text = "pdf:";
            // 
            // trinhLabel
            // 
            trinhLabel.AutoSize = true;
            trinhLabel.Location = new System.Drawing.Point(311, 3);
            trinhLabel.Name = "trinhLabel";
            trinhLabel.Size = new System.Drawing.Size(30, 13);
            trinhLabel.TabIndex = 58;
            trinhLabel.Text = "trinh:";
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Location = new System.Drawing.Point(6, 3);
            label8.Name = "label8";
            label8.Size = new System.Drawing.Size(43, 13);
            label8.TabIndex = 21;
            label8.Text = "primary:";
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Location = new System.Drawing.Point(115, 3);
            label7.Name = "label7";
            label7.Size = new System.Drawing.Size(48, 13);
            label7.TabIndex = 22;
            label7.Text = "trinomial:";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new System.Drawing.Point(247, 3);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(53, 13);
            label6.TabIndex = 24;
            label6.Text = "fsnumber:";
            // 
            // hrinumberLabel
            // 
            hrinumberLabel.AutoSize = true;
            hrinumberLabel.Location = new System.Drawing.Point(436, 2);
            hrinumberLabel.Name = "hrinumberLabel";
            hrinumberLabel.Size = new System.Drawing.Size(56, 13);
            hrinumberLabel.TabIndex = 26;
            hrinumberLabel.Text = "hrinumber:";
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Location = new System.Drawing.Point(28, 252);
            label9.Name = "label9";
            label9.Size = new System.Drawing.Size(45, 13);
            label9.TabIndex = 50;
            label9.Text = "orig pdf:";
            // 
            // label24
            // 
            label24.AutoSize = true;
            label24.Location = new System.Drawing.Point(586, 4);
            label24.Name = "label24";
            label24.Size = new System.Drawing.Size(45, 13);
            label24.TabIndex = 35;
            label24.Text = "reportid:";
            // 
            // batchResourceBindingSource
            // 
            this.batchResourceBindingSource.DataMember = "resources";
            this.batchResourceBindingSource.DataSource = this.batchDataset1;
            this.batchResourceBindingSource.BindingComplete += new System.Windows.Forms.BindingCompleteEventHandler(this.batchResourceBindingSource_BindingComplete);
            this.batchResourceBindingSource.CurrentChanged += new System.EventHandler(this.batchResourceBindingSource_CurrentChanged);
            // 
            // batchDataset1
            // 
            this.batchDataset1.DataSetName = "BatchDataset";
            this.batchDataset1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // lookupcountycodesBindingSource
            // 
            this.lookupcountycodesBindingSource.DataMember = "lookupcountycodes";
            this.lookupcountycodesBindingSource.DataSource = this.pgeresourcesDataSet;
            // 
            // pgeresourcesDataSet
            // 
            this.pgeresourcesDataSet.DataSetName = "pgeresourcesDataSet";
            this.pgeresourcesDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // lookupeligibilityBindingSource
            // 
            this.lookupeligibilityBindingSource.DataMember = "lookupeligibility";
            this.lookupeligibilityBindingSource.DataSource = this.pgeresourcesDataSet;
            // 
            // batchReportBindingSource
            // 
            this.batchReportBindingSource.DataMember = "reports";
            this.batchReportBindingSource.DataSource = this.batchDataset1;
            // 
            // lookupagencyBindingSource
            // 
            this.lookupagencyBindingSource.DataMember = "lookupagency";
            this.lookupagencyBindingSource.DataSource = this.pgeresourcesDataSet;
            // 
            // resourcesTableAdapter
            // 
            this.resourcesTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager1
            // 
            this.tableAdapterManager1.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager1.Connection = null;
            this.tableAdapterManager1.lookupmperpmTableAdapter = null;
            this.tableAdapterManager1.lookupresourcexrefsTableAdapter = null;
            this.tableAdapterManager1.mpepersonnelTableAdapter = null;
            this.tableAdapterManager1.mpereportpersonnelTableAdapter = null;
            this.tableAdapterManager1.reportsTableAdapter = null;
            this.tableAdapterManager1.UpdateOrder = PGELibraryAddin.BatchDatasetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // reportsTableAdapter
            // 
            this.reportsTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.lookupmperpmTableAdapter = null;
            this.tableAdapterManager.lookupresourcexrefsTableAdapter = null;
            this.tableAdapterManager.mpepersonnelTableAdapter = null;
            this.tableAdapterManager.mpereportpersonnelTableAdapter = null;
            this.tableAdapterManager.reportsTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = PGELibraryAddin.BatchDatasetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // npgsqlCommand1
            // 
            this.npgsqlCommand1.AllResultTypesAreUnknown = false;
            this.npgsqlCommand1.Transaction = null;
            this.npgsqlCommand1.UnknownResultTypeList = null;
            // 
            // lookupagencyTableAdapter
            // 
            this.lookupagencyTableAdapter.ClearBeforeFill = true;
            // 
            // lookupeligibilityTableAdapter
            // 
            this.lookupeligibilityTableAdapter.ClearBeforeFill = true;
            // 
            // ofdPDF
            // 
            this.ofdPDF.FileName = "openFileDialog1";
            // 
            // lookupcountycodesTableAdapter1
            // 
            this.lookupcountycodesTableAdapter1.ClearBeforeFill = true;
            // 
            // lookupReportTypebindingSource
            // 
            this.lookupReportTypebindingSource.DataMember = "lookupreporttype";
            this.lookupReportTypebindingSource.DataSource = this.pgeresourcesDataSet;
            // 
            // tableAdapterManager2
            // 
            this.tableAdapterManager2.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager2.lookupeligibilityTableAdapter = this.lookupeligibilityTableAdapter;
            this.tableAdapterManager2.lookupreporttypeTableAdapter = null;
            this.tableAdapterManager2.lookupresourcexrefsTableAdapter = null;
            this.tableAdapterManager2.UpdateOrder = PGELibraryAddin.pgeresourcesDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage16);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(764, 564);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPage1.Controls.Add(this.tableLayoutPanel5);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(756, 538);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Processing";
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Controls.Add(this.tableLayoutPanel11, 0, 3);
            this.tableLayoutPanel5.Controls.Add(this.tableLayoutPanel6, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.tableLayoutPanel8, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.tableLayoutPanel10, 0, 2);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 4;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(750, 532);
            this.tableLayoutPanel5.TabIndex = 20;
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.BackColor = System.Drawing.Color.Gainsboro;
            this.tableLayoutPanel11.ColumnCount = 3;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 119F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 133F));
            this.tableLayoutPanel11.Controls.Add(this.buttonImportSitePDFs, 0, 0);
            this.tableLayoutPanel11.Controls.Add(this.button3, 2, 0);
            this.tableLayoutPanel11.Controls.Add(this.tableLayoutPanel12, 0, 2);
            this.tableLayoutPanel11.Controls.Add(this.button4, 2, 2);
            this.tableLayoutPanel11.Controls.Add(this.btnComplete, 2, 3);
            this.tableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel11.Location = new System.Drawing.Point(3, 309);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 4;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(744, 220);
            this.tableLayoutPanel11.TabIndex = 21;
            // 
            // buttonImportSitePDFs
            // 
            this.buttonImportSitePDFs.Location = new System.Drawing.Point(3, 3);
            this.buttonImportSitePDFs.Name = "buttonImportSitePDFs";
            this.buttonImportSitePDFs.Size = new System.Drawing.Size(113, 22);
            this.buttonImportSitePDFs.TabIndex = 0;
            this.buttonImportSitePDFs.Text = "Import Site PDFs";
            this.buttonImportSitePDFs.UseVisualStyleBackColor = true;
            this.buttonImportSitePDFs.Click += new System.EventHandler(this.buttonImportSitePDFs_Click);
            // 
            // button3
            // 
            this.button3.Enabled = false;
            this.button3.Location = new System.Drawing.Point(614, 3);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(106, 22);
            this.button3.TabIndex = 13;
            this.button3.Text = "Import Site GIS";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.ColumnCount = 1;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel12.Controls.Add(this.optReportNumber, 0, 0);
            this.tableLayoutPanel12.Controls.Add(this.optReportTitle, 0, 1);
            this.tableLayoutPanel12.Controls.Add(this.button2, 0, 2);
            this.tableLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel12.Location = new System.Drawing.Point(3, 53);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 3;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 48.21429F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 51.78571F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(113, 119);
            this.tableLayoutPanel12.TabIndex = 20;
            // 
            // optReportNumber
            // 
            this.optReportNumber.AutoSize = true;
            this.optReportNumber.Location = new System.Drawing.Point(3, 3);
            this.optReportNumber.Name = "optReportNumber";
            this.optReportNumber.Size = new System.Drawing.Size(97, 15);
            this.optReportNumber.TabIndex = 0;
            this.optReportNumber.Text = "Report Number";
            this.optReportNumber.UseVisualStyleBackColor = true;
            // 
            // optReportTitle
            // 
            this.optReportTitle.AutoSize = true;
            this.optReportTitle.Checked = true;
            this.optReportTitle.Location = new System.Drawing.Point(3, 24);
            this.optReportTitle.Name = "optReportTitle";
            this.optReportTitle.Size = new System.Drawing.Size(80, 16);
            this.optReportTitle.TabIndex = 1;
            this.optReportTitle.TabStop = true;
            this.optReportTitle.Text = "Report Title";
            this.optReportTitle.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(3, 46);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(107, 22);
            this.button2.TabIndex = 2;
            this.button2.Text = "Import Report PDFs";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button4
            // 
            this.button4.Enabled = false;
            this.button4.Location = new System.Drawing.Point(614, 53);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(106, 23);
            this.button4.TabIndex = 14;
            this.button4.Text = "Import Report GIS";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // btnComplete
            // 
            this.btnComplete.Location = new System.Drawing.Point(614, 178);
            this.btnComplete.Name = "btnComplete";
            this.btnComplete.Size = new System.Drawing.Size(106, 25);
            this.btnComplete.TabIndex = 1;
            this.btnComplete.Text = "Complete Batch";
            this.btnComplete.UseVisualStyleBackColor = true;
            this.btnComplete.Click += new System.EventHandler(this.btnComplete_Click);
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.BackColor = System.Drawing.Color.Gainsboro;
            this.tableLayoutPanel6.ColumnCount = 4;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.77667F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 81.22333F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 101F));
            this.tableLayoutPanel6.Controls.Add(this.checkExistingVersion, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.buttonSetVersion, 3, 0);
            this.tableLayoutPanel6.Controls.Add(this.tableLayoutPanel7, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.buttonReload, 2, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(744, 34);
            this.tableLayoutPanel6.TabIndex = 0;
            // 
            // checkExistingVersion
            // 
            this.checkExistingVersion.AutoSize = true;
            this.checkExistingVersion.Location = new System.Drawing.Point(3, 3);
            this.checkExistingVersion.Name = "checkExistingVersion";
            this.checkExistingVersion.Size = new System.Drawing.Size(62, 17);
            this.checkExistingVersion.TabIndex = 2;
            this.checkExistingVersion.Text = "Existing";
            this.checkExistingVersion.UseVisualStyleBackColor = true;
            // 
            // buttonSetVersion
            // 
            this.buttonSetVersion.Location = new System.Drawing.Point(645, 3);
            this.buttonSetVersion.Name = "buttonSetVersion";
            this.buttonSetVersion.Size = new System.Drawing.Size(75, 27);
            this.buttonSetVersion.TabIndex = 1;
            this.buttonSetVersion.Text = "Set Version";
            this.buttonSetVersion.UseVisualStyleBackColor = true;
            this.buttonSetVersion.Click += new System.EventHandler(this.buttonSetVersion_Click);
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel7.ColumnCount = 2;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 13.27434F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 86.72566F));
            this.tableLayoutPanel7.Controls.Add(this.comboBatchNames, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this.label4, 0, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(117, 3);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(488, 28);
            this.tableLayoutPanel7.TabIndex = 11;
            // 
            // comboBatchNames
            // 
            this.comboBatchNames.FormattingEnabled = true;
            this.comboBatchNames.Location = new System.Drawing.Point(67, 3);
            this.comboBatchNames.Name = "comboBatchNames";
            this.comboBatchNames.Size = new System.Drawing.Size(412, 21);
            this.comboBatchNames.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 26);
            this.label4.TabIndex = 21;
            this.label4.Text = "Batch Name";
            // 
            // buttonReload
            // 
            this.buttonReload.Image = global::PGELibraryAddin.Properties.Resources.arrow_refresh;
            this.buttonReload.Location = new System.Drawing.Point(611, 3);
            this.buttonReload.Name = "buttonReload";
            this.buttonReload.Size = new System.Drawing.Size(27, 26);
            this.buttonReload.TabIndex = 0;
            this.buttonReload.UseVisualStyleBackColor = true;
            this.buttonReload.Click += new System.EventHandler(this.buttonReload_Click);
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tableLayoutPanel8.ColumnCount = 2;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.05376F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 84.94624F));
            this.tableLayoutPanel8.Controls.Add(this.checkNewVersion, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.tableLayoutPanel9, 1, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(3, 43);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 1;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(744, 34);
            this.tableLayoutPanel8.TabIndex = 1;
            // 
            // checkNewVersion
            // 
            this.checkNewVersion.AutoSize = true;
            this.checkNewVersion.Location = new System.Drawing.Point(3, 3);
            this.checkNewVersion.Name = "checkNewVersion";
            this.checkNewVersion.Size = new System.Drawing.Size(80, 17);
            this.checkNewVersion.TabIndex = 0;
            this.checkNewVersion.Text = "New Import";
            this.checkNewVersion.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tableLayoutPanel9.ColumnCount = 2;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 561F));
            this.tableLayoutPanel9.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.textNewBatchName, 1, 0);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(114, 3);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 1;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(627, 28);
            this.tableLayoutPanel9.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "Batch Name";
            // 
            // textNewBatchName
            // 
            this.textNewBatchName.Location = new System.Drawing.Point(69, 3);
            this.textNewBatchName.Name = "textNewBatchName";
            this.textNewBatchName.Size = new System.Drawing.Size(413, 20);
            this.textNewBatchName.TabIndex = 0;
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tableLayoutPanel10.ColumnCount = 5;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 118F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 82F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 134F));
            this.tableLayoutPanel10.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel10.Controls.Add(this.label3, 2, 0);
            this.tableLayoutPanel10.Controls.Add(this.textFWJobNumber, 1, 0);
            this.tableLayoutPanel10.Controls.Add(this.textPGENumber, 3, 0);
            this.tableLayoutPanel10.Controls.Add(this.textComment, 1, 2);
            this.tableLayoutPanel10.Controls.Add(this.label5, 0, 2);
            this.tableLayoutPanel10.Controls.Add(this.buttonCreateVersion, 4, 2);
            this.tableLayoutPanel10.Controls.Add(this.txtBatchPath, 1, 1);
            this.tableLayoutPanel10.Controls.Add(this.label11, 0, 1);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(3, 83);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 3;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(744, 220);
            this.tableLayoutPanel10.TabIndex = 21;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "FW Job Number";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(203, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "PG&E Number";
            // 
            // textFWJobNumber
            // 
            this.textFWJobNumber.Location = new System.Drawing.Point(121, 3);
            this.textFWJobNumber.Name = "textFWJobNumber";
            this.textFWJobNumber.Size = new System.Drawing.Size(76, 20);
            this.textFWJobNumber.TabIndex = 0;
            // 
            // textPGENumber
            // 
            this.textPGENumber.Location = new System.Drawing.Point(303, 3);
            this.textPGENumber.Name = "textPGENumber";
            this.textPGENumber.Size = new System.Drawing.Size(155, 20);
            this.textPGENumber.TabIndex = 1;
            // 
            // textComment
            // 
            this.tableLayoutPanel10.SetColumnSpan(this.textComment, 3);
            this.textComment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textComment.Location = new System.Drawing.Point(121, 63);
            this.textComment.Multiline = true;
            this.textComment.Name = "textComment";
            this.textComment.Size = new System.Drawing.Size(486, 154);
            this.textComment.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 60);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Comment";
            // 
            // buttonCreateVersion
            // 
            this.buttonCreateVersion.Location = new System.Drawing.Point(613, 63);
            this.buttonCreateVersion.Name = "buttonCreateVersion";
            this.buttonCreateVersion.Size = new System.Drawing.Size(107, 21);
            this.buttonCreateVersion.TabIndex = 4;
            this.buttonCreateVersion.Text = "Create Version";
            this.buttonCreateVersion.UseVisualStyleBackColor = true;
            this.buttonCreateVersion.Click += new System.EventHandler(this.buttonCreateVersion_Click_1);
            // 
            // txtBatchPath
            // 
            this.tableLayoutPanel10.SetColumnSpan(this.txtBatchPath, 3);
            this.txtBatchPath.Location = new System.Drawing.Point(121, 33);
            this.txtBatchPath.Name = "txtBatchPath";
            this.txtBatchPath.Size = new System.Drawing.Size(486, 20);
            this.txtBatchPath.TabIndex = 2;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 30);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(60, 13);
            this.label11.TabIndex = 12;
            this.label11.Text = "Batch Path";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.resourceTabCntl);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(756, 538);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Batch Sites";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // resourceTabCntl
            // 
            this.resourceTabCntl.Controls.Add(this.tabPage4);
            this.resourceTabCntl.Controls.Add(this.tabPage5);
            this.resourceTabCntl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.resourceTabCntl.Location = new System.Drawing.Point(3, 3);
            this.resourceTabCntl.Name = "resourceTabCntl";
            this.resourceTabCntl.SelectedIndex = 0;
            this.resourceTabCntl.Size = new System.Drawing.Size(750, 532);
            this.resourceTabCntl.TabIndex = 1;
            this.resourceTabCntl.Click += new System.EventHandler(this.resourceTabCntl_Click);
            // 
            // tabPage4
            // 
            this.tabPage4.AutoScroll = true;
            this.tabPage4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPage4.Controls.Add(this.buttonRefresh1);
            this.tabPage4.Controls.Add(this.btnAddFilter);
            this.tabPage4.Controls.Add(this.btnClear);
            this.tabPage4.Controls.Add(this.drpSelection);
            this.tabPage4.Controls.Add(this.sitesDataGridView);
            this.tabPage4.Controls.Add(this.btnFilter);
            this.tabPage4.Controls.Add(this.txtSearch);
            this.tabPage4.Controls.Add(this.lblFilter);
            this.tabPage4.Controls.Add(this.chkMissingSiteGIS);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(742, 506);
            this.tabPage4.TabIndex = 0;
            this.tabPage4.Text = "List View";
            // 
            // buttonRefresh1
            // 
            this.buttonRefresh1.Image = global::PGELibraryAddin.Properties.Resources.arrow_refresh;
            this.buttonRefresh1.Location = new System.Drawing.Point(708, 1);
            this.buttonRefresh1.Name = "buttonRefresh1";
            this.buttonRefresh1.Size = new System.Drawing.Size(32, 23);
            this.buttonRefresh1.TabIndex = 17;
            this.buttonRefresh1.UseVisualStyleBackColor = true;
            this.buttonRefresh1.Click += new System.EventHandler(this.buttonRefresh1_Click);
            // 
            // btnAddFilter
            // 
            this.btnAddFilter.Location = new System.Drawing.Point(467, 1);
            this.btnAddFilter.Name = "btnAddFilter";
            this.btnAddFilter.Size = new System.Drawing.Size(83, 23);
            this.btnAddFilter.TabIndex = 16;
            this.btnAddFilter.Text = "Add To Filter";
            this.btnAddFilter.UseVisualStyleBackColor = true;
            this.btnAddFilter.Click += new System.EventHandler(this.btnAddFilter_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(631, 1);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 23);
            this.btnClear.TabIndex = 15;
            this.btnClear.Text = "Clear filters";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // drpSelection
            // 
            this.drpSelection.FormattingEnabled = true;
            this.drpSelection.Items.AddRange(new object[] {
            "resourceid",
            "age",
            "primarylabel",
            "trinomiallabel",
            "fslabel",
            "othername",
            "nrhp_eligibility",
            "hrinumber",
            "land_owner",
            "area"});
            this.drpSelection.Location = new System.Drawing.Point(102, 3);
            this.drpSelection.Name = "drpSelection";
            this.drpSelection.Size = new System.Drawing.Size(157, 21);
            this.drpSelection.TabIndex = 14;
            // 
            // sitesDataGridView
            // 
            this.sitesDataGridView.AllowUserToAddRows = false;
            this.sitesDataGridView.AllowUserToDeleteRows = false;
            this.sitesDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sitesDataGridView.AutoGenerateColumns = false;
            this.sitesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.sitesDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.resourceidDataGridViewTextBoxColumn,
            this.primcoDataGridViewTextBoxColumn,
            this.primnoDataGridViewTextBoxColumn,
            this.hrinumberDataGridViewTextBoxColumn,
            this.trinnoDataGridViewTextBoxColumn,
            this.trinno2DataGridViewTextBoxColumn,
            this.trinhDataGridViewTextBoxColumn,
            this.fsregionDataGridViewTextBoxColumn,
            this.fsforestDataGridViewTextBoxColumn,
            this.fsnump1DataGridViewTextBoxColumn,
            this.fsnump2DataGridViewTextBoxColumn,
            this.othernameDataGridViewTextBoxColumn,
            this.pdfDataGridViewTextBoxColumn,
            this.ageDataGridViewTextBoxColumn,
            this.descriptionDataGridViewTextBoxColumn,
            this.nrhpeligibilityDataGridViewTextBoxColumn,
            this.resareaDataGridViewTextBoxColumn,
            this.landownerDataGridViewTextBoxColumn,
            this.commentsDataGridViewTextBoxColumn,
            this.preeligDataGridViewTextBoxColumn,
            this.histeligDataGridViewTextBoxColumn,
            this.sitedatastatusDataGridViewTextBoxColumn,
            this.origfilepathDataGridViewTextBoxColumn,
            this.origsiteidDataGridViewTextBoxColumn,
            this.createduserDataGridViewTextBoxColumn1,
            this.createddateDataGridViewTextBoxColumn2,
            this.lastediteduserDataGridViewTextBoxColumn1,
            this.lastediteddateDataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.fslabelDataGridViewTextBoxColumn,
            this.reportgroupDataGridViewTextBoxColumn});
            this.sitesDataGridView.DataSource = this.batchResourceBindingSource;
            this.sitesDataGridView.Location = new System.Drawing.Point(3, 26);
            this.sitesDataGridView.Name = "sitesDataGridView";
            this.sitesDataGridView.Size = new System.Drawing.Size(736, 480);
            this.sitesDataGridView.TabIndex = 0;
            this.sitesDataGridView.VirtualMode = true;
            this.sitesDataGridView.UserAddedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.sitesDataGridView_UserAddedRow);
            // 
            // resourceidDataGridViewTextBoxColumn
            // 
            this.resourceidDataGridViewTextBoxColumn.DataPropertyName = "resourceid";
            this.resourceidDataGridViewTextBoxColumn.HeaderText = "resourceid";
            this.resourceidDataGridViewTextBoxColumn.Name = "resourceidDataGridViewTextBoxColumn";
            // 
            // primcoDataGridViewTextBoxColumn
            // 
            this.primcoDataGridViewTextBoxColumn.DataPropertyName = "primco";
            this.primcoDataGridViewTextBoxColumn.HeaderText = "primco";
            this.primcoDataGridViewTextBoxColumn.Name = "primcoDataGridViewTextBoxColumn";
            // 
            // primnoDataGridViewTextBoxColumn
            // 
            this.primnoDataGridViewTextBoxColumn.DataPropertyName = "primno";
            this.primnoDataGridViewTextBoxColumn.HeaderText = "primno";
            this.primnoDataGridViewTextBoxColumn.Name = "primnoDataGridViewTextBoxColumn";
            // 
            // hrinumberDataGridViewTextBoxColumn
            // 
            this.hrinumberDataGridViewTextBoxColumn.DataPropertyName = "hrinumber";
            this.hrinumberDataGridViewTextBoxColumn.HeaderText = "hrinumber";
            this.hrinumberDataGridViewTextBoxColumn.Name = "hrinumberDataGridViewTextBoxColumn";
            // 
            // trinnoDataGridViewTextBoxColumn
            // 
            this.trinnoDataGridViewTextBoxColumn.DataPropertyName = "trinno";
            this.trinnoDataGridViewTextBoxColumn.HeaderText = "trinno";
            this.trinnoDataGridViewTextBoxColumn.Name = "trinnoDataGridViewTextBoxColumn";
            // 
            // trinno2DataGridViewTextBoxColumn
            // 
            this.trinno2DataGridViewTextBoxColumn.DataPropertyName = "trinno2";
            this.trinno2DataGridViewTextBoxColumn.HeaderText = "trinno2";
            this.trinno2DataGridViewTextBoxColumn.Name = "trinno2DataGridViewTextBoxColumn";
            // 
            // trinhDataGridViewTextBoxColumn
            // 
            this.trinhDataGridViewTextBoxColumn.DataPropertyName = "trinh";
            this.trinhDataGridViewTextBoxColumn.HeaderText = "trinh";
            this.trinhDataGridViewTextBoxColumn.Name = "trinhDataGridViewTextBoxColumn";
            // 
            // fsregionDataGridViewTextBoxColumn
            // 
            this.fsregionDataGridViewTextBoxColumn.DataPropertyName = "fsregion";
            this.fsregionDataGridViewTextBoxColumn.HeaderText = "fsregion";
            this.fsregionDataGridViewTextBoxColumn.Name = "fsregionDataGridViewTextBoxColumn";
            // 
            // fsforestDataGridViewTextBoxColumn
            // 
            this.fsforestDataGridViewTextBoxColumn.DataPropertyName = "fsforest";
            this.fsforestDataGridViewTextBoxColumn.HeaderText = "fsforest";
            this.fsforestDataGridViewTextBoxColumn.Name = "fsforestDataGridViewTextBoxColumn";
            // 
            // fsnump1DataGridViewTextBoxColumn
            // 
            this.fsnump1DataGridViewTextBoxColumn.DataPropertyName = "fsnump1";
            this.fsnump1DataGridViewTextBoxColumn.HeaderText = "fsnump1";
            this.fsnump1DataGridViewTextBoxColumn.Name = "fsnump1DataGridViewTextBoxColumn";
            // 
            // fsnump2DataGridViewTextBoxColumn
            // 
            this.fsnump2DataGridViewTextBoxColumn.DataPropertyName = "fsnump2";
            this.fsnump2DataGridViewTextBoxColumn.HeaderText = "fsnump2";
            this.fsnump2DataGridViewTextBoxColumn.Name = "fsnump2DataGridViewTextBoxColumn";
            // 
            // othernameDataGridViewTextBoxColumn
            // 
            this.othernameDataGridViewTextBoxColumn.DataPropertyName = "othername";
            this.othernameDataGridViewTextBoxColumn.HeaderText = "othername";
            this.othernameDataGridViewTextBoxColumn.Name = "othernameDataGridViewTextBoxColumn";
            // 
            // pdfDataGridViewTextBoxColumn
            // 
            this.pdfDataGridViewTextBoxColumn.DataPropertyName = "pdf";
            this.pdfDataGridViewTextBoxColumn.HeaderText = "pdf";
            this.pdfDataGridViewTextBoxColumn.Name = "pdfDataGridViewTextBoxColumn";
            // 
            // ageDataGridViewTextBoxColumn
            // 
            this.ageDataGridViewTextBoxColumn.DataPropertyName = "age";
            this.ageDataGridViewTextBoxColumn.HeaderText = "age";
            this.ageDataGridViewTextBoxColumn.Name = "ageDataGridViewTextBoxColumn";
            // 
            // descriptionDataGridViewTextBoxColumn
            // 
            this.descriptionDataGridViewTextBoxColumn.DataPropertyName = "description";
            this.descriptionDataGridViewTextBoxColumn.HeaderText = "description";
            this.descriptionDataGridViewTextBoxColumn.Name = "descriptionDataGridViewTextBoxColumn";
            // 
            // nrhpeligibilityDataGridViewTextBoxColumn
            // 
            this.nrhpeligibilityDataGridViewTextBoxColumn.DataPropertyName = "nrhp_eligibility";
            this.nrhpeligibilityDataGridViewTextBoxColumn.HeaderText = "nrhp_eligibility";
            this.nrhpeligibilityDataGridViewTextBoxColumn.Name = "nrhpeligibilityDataGridViewTextBoxColumn";
            // 
            // resareaDataGridViewTextBoxColumn
            // 
            this.resareaDataGridViewTextBoxColumn.DataPropertyName = "res_area";
            this.resareaDataGridViewTextBoxColumn.HeaderText = "res_area";
            this.resareaDataGridViewTextBoxColumn.Name = "resareaDataGridViewTextBoxColumn";
            // 
            // landownerDataGridViewTextBoxColumn
            // 
            this.landownerDataGridViewTextBoxColumn.DataPropertyName = "land_owner";
            this.landownerDataGridViewTextBoxColumn.HeaderText = "land_owner";
            this.landownerDataGridViewTextBoxColumn.Name = "landownerDataGridViewTextBoxColumn";
            // 
            // commentsDataGridViewTextBoxColumn
            // 
            this.commentsDataGridViewTextBoxColumn.DataPropertyName = "comments";
            this.commentsDataGridViewTextBoxColumn.HeaderText = "comments";
            this.commentsDataGridViewTextBoxColumn.Name = "commentsDataGridViewTextBoxColumn";
            // 
            // preeligDataGridViewTextBoxColumn
            // 
            this.preeligDataGridViewTextBoxColumn.DataPropertyName = "preelig";
            this.preeligDataGridViewTextBoxColumn.HeaderText = "preelig";
            this.preeligDataGridViewTextBoxColumn.Name = "preeligDataGridViewTextBoxColumn";
            // 
            // histeligDataGridViewTextBoxColumn
            // 
            this.histeligDataGridViewTextBoxColumn.DataPropertyName = "histelig";
            this.histeligDataGridViewTextBoxColumn.HeaderText = "histelig";
            this.histeligDataGridViewTextBoxColumn.Name = "histeligDataGridViewTextBoxColumn";
            // 
            // sitedatastatusDataGridViewTextBoxColumn
            // 
            this.sitedatastatusDataGridViewTextBoxColumn.DataPropertyName = "sitedatastatus";
            this.sitedatastatusDataGridViewTextBoxColumn.HeaderText = "sitedatastatus";
            this.sitedatastatusDataGridViewTextBoxColumn.Name = "sitedatastatusDataGridViewTextBoxColumn";
            // 
            // origfilepathDataGridViewTextBoxColumn
            // 
            this.origfilepathDataGridViewTextBoxColumn.DataPropertyName = "origfilepath";
            this.origfilepathDataGridViewTextBoxColumn.HeaderText = "origfilepath";
            this.origfilepathDataGridViewTextBoxColumn.Name = "origfilepathDataGridViewTextBoxColumn";
            // 
            // origsiteidDataGridViewTextBoxColumn
            // 
            this.origsiteidDataGridViewTextBoxColumn.DataPropertyName = "origsiteid";
            this.origsiteidDataGridViewTextBoxColumn.HeaderText = "origsiteid";
            this.origsiteidDataGridViewTextBoxColumn.Name = "origsiteidDataGridViewTextBoxColumn";
            // 
            // createduserDataGridViewTextBoxColumn1
            // 
            this.createduserDataGridViewTextBoxColumn1.DataPropertyName = "created_user";
            this.createduserDataGridViewTextBoxColumn1.HeaderText = "created_user";
            this.createduserDataGridViewTextBoxColumn1.Name = "createduserDataGridViewTextBoxColumn1";
            // 
            // createddateDataGridViewTextBoxColumn2
            // 
            this.createddateDataGridViewTextBoxColumn2.DataPropertyName = "created_date";
            this.createddateDataGridViewTextBoxColumn2.HeaderText = "created_date";
            this.createddateDataGridViewTextBoxColumn2.Name = "createddateDataGridViewTextBoxColumn2";
            // 
            // lastediteduserDataGridViewTextBoxColumn1
            // 
            this.lastediteduserDataGridViewTextBoxColumn1.DataPropertyName = "last_edited_user";
            this.lastediteduserDataGridViewTextBoxColumn1.HeaderText = "last_edited_user";
            this.lastediteduserDataGridViewTextBoxColumn1.Name = "lastediteduserDataGridViewTextBoxColumn1";
            // 
            // lastediteddateDataGridViewTextBoxColumn1
            // 
            this.lastediteddateDataGridViewTextBoxColumn1.DataPropertyName = "last_edited_date";
            this.lastediteddateDataGridViewTextBoxColumn1.HeaderText = "last_edited_date";
            this.lastediteddateDataGridViewTextBoxColumn1.Name = "lastediteddateDataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "primarylabel";
            this.dataGridViewTextBoxColumn1.HeaderText = "primarylabel";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "trinomiallabel";
            this.dataGridViewTextBoxColumn2.HeaderText = "trinomiallabel";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // fslabelDataGridViewTextBoxColumn
            // 
            this.fslabelDataGridViewTextBoxColumn.DataPropertyName = "fslabel";
            this.fslabelDataGridViewTextBoxColumn.HeaderText = "fslabel";
            this.fslabelDataGridViewTextBoxColumn.Name = "fslabelDataGridViewTextBoxColumn";
            // 
            // reportgroupDataGridViewTextBoxColumn
            // 
            this.reportgroupDataGridViewTextBoxColumn.DataPropertyName = "reportgroup";
            this.reportgroupDataGridViewTextBoxColumn.HeaderText = "reportgroup";
            this.reportgroupDataGridViewTextBoxColumn.Name = "reportgroupDataGridViewTextBoxColumn";
            // 
            // btnFilter
            // 
            this.btnFilter.Location = new System.Drawing.Point(390, 1);
            this.btnFilter.Name = "btnFilter";
            this.btnFilter.Size = new System.Drawing.Size(75, 23);
            this.btnFilter.TabIndex = 2;
            this.btnFilter.Text = "Filter";
            this.btnFilter.UseVisualStyleBackColor = true;
            this.btnFilter.Click += new System.EventHandler(this.btnFilter_Click);
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(265, 3);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(120, 20);
            this.txtSearch.TabIndex = 13;
            // 
            // lblFilter
            // 
            this.lblFilter.Location = new System.Drawing.Point(6, 3);
            this.lblFilter.Name = "lblFilter";
            this.lblFilter.Size = new System.Drawing.Size(90, 20);
            this.lblFilter.TabIndex = 3;
            this.lblFilter.Text = "Select filter value";
            // 
            // chkMissingSiteGIS
            // 
            this.chkMissingSiteGIS.AutoSize = true;
            this.chkMissingSiteGIS.Location = new System.Drawing.Point(551, 4);
            this.chkMissingSiteGIS.Name = "chkMissingSiteGIS";
            this.chkMissingSiteGIS.Size = new System.Drawing.Size(82, 17);
            this.chkMissingSiteGIS.TabIndex = 25;
            this.chkMissingSiteGIS.Text = "Missing GIS";
            this.chkMissingSiteGIS.UseVisualStyleBackColor = true;
            this.chkMissingSiteGIS.CheckedChanged += new System.EventHandler(this.chkMissingSiteGIS_CheckedChanged);
            // 
            // tabPage5
            // 
            this.tabPage5.AutoScroll = true;
            this.tabPage5.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPage5.Controls.Add(this.chkSitesMatchingDisabled);
            this.tabPage5.Controls.Add(this.label15);
            this.tabPage5.Controls.Add(this.comboBox3);
            this.tabPage5.Controls.Add(this.btnSitesRefreshMatch);
            this.tabPage5.Controls.Add(this.btnOpenOrigPDf);
            this.tabPage5.Controls.Add(label9);
            this.tabPage5.Controls.Add(this.textOrigPDF);
            this.tabPage5.Controls.Add(this.areaTextBox);
            this.tabPage5.Controls.Add(this.pdfTextBox);
            this.tabPage5.Controls.Add(this.btnAddResourceReportRef);
            this.tabPage5.Controls.Add(this.descriptionTextBox);
            this.tabPage5.Controls.Add(this.othernameTextBox);
            this.tabPage5.Controls.Add(this.siteidTextBox);
            this.tabPage5.Controls.Add(this.btnSelectSitePDF);
            this.tabPage5.Controls.Add(this.btnOpenSitePDF);
            this.tabPage5.Controls.Add(this.ageTextBox);
            this.tabPage5.Controls.Add(this.tabControl2);
            this.tabPage5.Controls.Add(pdfLabel);
            this.tabPage5.Controls.Add(commentsLabel);
            this.tabPage5.Controls.Add(land_ownerLabel);
            this.tabPage5.Controls.Add(this.land_ownerTextBox);
            this.tabPage5.Controls.Add(areaLabel);
            this.tabPage5.Controls.Add(nrhp_eligibilityLabel);
            this.tabPage5.Controls.Add(this.nrhp_eligibilityTextBox);
            this.tabPage5.Controls.Add(descriptionLabel);
            this.tabPage5.Controls.Add(ageLabel);
            this.tabPage5.Controls.Add(othernameLabel);
            this.tabPage5.Controls.Add(siteidLabel);
            this.tabPage5.Controls.Add(this.tabControl3);
            this.tabPage5.Controls.Add(this.bindingNavigator1);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(742, 506);
            this.tabPage5.TabIndex = 1;
            this.tabPage5.Text = "Form View";
            // 
            // chkSitesMatchingDisabled
            // 
            this.chkSitesMatchingDisabled.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkSitesMatchingDisabled.AutoSize = true;
            this.chkSitesMatchingDisabled.FlatAppearance.CheckedBackColor = System.Drawing.Color.Red;
            this.chkSitesMatchingDisabled.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSitesMatchingDisabled.Location = new System.Drawing.Point(578, 272);
            this.chkSitesMatchingDisabled.Name = "chkSitesMatchingDisabled";
            this.chkSitesMatchingDisabled.Size = new System.Drawing.Size(89, 22);
            this.chkSitesMatchingDisabled.TabIndex = 103;
            this.chkSitesMatchingDisabled.Text = "Matching Enabled";
            this.chkSitesMatchingDisabled.UseVisualStyleBackColor = true;
            this.chkSitesMatchingDisabled.CheckedChanged += new System.EventHandler(this.chkSitesMatchingDisabled_CheckedChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(405, 94);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(48, 26);
            this.label15.TabIndex = 54;
            this.label15.Text = "reporting\r\ngroup:";
            // 
            // comboBox3
            // 
            this.comboBox3.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.batchResourceBindingSource, "reportgroup", true));
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Items.AddRange(new object[] {
            "None",
            "MSUP"});
            this.comboBox3.Location = new System.Drawing.Point(466, 94);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(121, 21);
            this.comboBox3.TabIndex = 5;
            // 
            // btnSitesRefreshMatch
            // 
            this.btnSitesRefreshMatch.Image = global::PGELibraryAddin.Properties.Resources.arrow_refresh;
            this.btnSitesRefreshMatch.Location = new System.Drawing.Point(670, 271);
            this.btnSitesRefreshMatch.Name = "btnSitesRefreshMatch";
            this.btnSitesRefreshMatch.Size = new System.Drawing.Size(20, 23);
            this.btnSitesRefreshMatch.TabIndex = 53;
            this.btnSitesRefreshMatch.UseVisualStyleBackColor = true;
            this.btnSitesRefreshMatch.Click += new System.EventHandler(this.btnSitesRefreshMatch_Click);
            // 
            // btnOpenOrigPDf
            // 
            this.btnOpenOrigPDf.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOpenOrigPDf.Location = new System.Drawing.Point(593, 250);
            this.btnOpenOrigPDf.Name = "btnOpenOrigPDf";
            this.btnOpenOrigPDf.Size = new System.Drawing.Size(43, 18);
            this.btnOpenOrigPDf.TabIndex = 52;
            this.btnOpenOrigPDf.Text = "Open";
            this.btnOpenOrigPDf.UseVisualStyleBackColor = true;
            this.btnOpenOrigPDf.Click += new System.EventHandler(this.btnOpenOrigPDf_Click_1);
            // 
            // textOrigPDF
            // 
            this.textOrigPDF.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.batchResourceBindingSource, "origfilepath", true));
            this.textOrigPDF.Location = new System.Drawing.Point(78, 249);
            this.textOrigPDF.Name = "textOrigPDF";
            this.textOrigPDF.Size = new System.Drawing.Size(510, 20);
            this.textOrigPDF.TabIndex = 9;
            this.textOrigPDF.Leave += new System.EventHandler(this.textOrigPDF_Leave);
            // 
            // areaTextBox
            // 
            this.areaTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.batchResourceBindingSource, "res_area", true));
            this.areaTextBox.Location = new System.Drawing.Point(240, 118);
            this.areaTextBox.Name = "areaTextBox";
            this.areaTextBox.Size = new System.Drawing.Size(100, 20);
            this.areaTextBox.TabIndex = 4;
            // 
            // pdfTextBox
            // 
            this.pdfTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.batchResourceBindingSource, "pdf", true));
            this.pdfTextBox.Location = new System.Drawing.Point(78, 228);
            this.pdfTextBox.Name = "pdfTextBox";
            this.pdfTextBox.Size = new System.Drawing.Size(510, 20);
            this.pdfTextBox.TabIndex = 8;
            // 
            // btnAddResourceReportRef
            // 
            this.btnAddResourceReportRef.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.batchResourceBindingSource, "comments", true));
            this.btnAddResourceReportRef.Location = new System.Drawing.Point(78, 191);
            this.btnAddResourceReportRef.Multiline = true;
            this.btnAddResourceReportRef.Name = "btnAddResourceReportRef";
            this.btnAddResourceReportRef.Size = new System.Drawing.Size(612, 35);
            this.btnAddResourceReportRef.TabIndex = 7;
            // 
            // descriptionTextBox
            // 
            this.descriptionTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.batchResourceBindingSource, "description", true));
            this.descriptionTextBox.Location = new System.Drawing.Point(78, 140);
            this.descriptionTextBox.Multiline = true;
            this.descriptionTextBox.Name = "descriptionTextBox";
            this.descriptionTextBox.Size = new System.Drawing.Size(612, 49);
            this.descriptionTextBox.TabIndex = 6;
            // 
            // othernameTextBox
            // 
            this.othernameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.batchResourceBindingSource, "othername", true));
            this.othernameTextBox.Location = new System.Drawing.Point(77, 73);
            this.othernameTextBox.Name = "othernameTextBox";
            this.othernameTextBox.Size = new System.Drawing.Size(613, 20);
            this.othernameTextBox.TabIndex = 0;
            // 
            // siteidTextBox
            // 
            this.siteidTextBox.BackColor = System.Drawing.SystemColors.HighlightText;
            this.siteidTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.siteidTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.batchResourceBindingSource, "resourceid", true));
            this.siteidTextBox.Enabled = false;
            this.siteidTextBox.Location = new System.Drawing.Point(594, 126);
            this.siteidTextBox.Name = "siteidTextBox";
            this.siteidTextBox.ReadOnly = true;
            this.siteidTextBox.Size = new System.Drawing.Size(96, 13);
            this.siteidTextBox.TabIndex = 16;
            // 
            // btnSelectSitePDF
            // 
            this.btnSelectSitePDF.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelectSitePDF.Location = new System.Drawing.Point(635, 229);
            this.btnSelectSitePDF.Name = "btnSelectSitePDF";
            this.btnSelectSitePDF.Size = new System.Drawing.Size(43, 18);
            this.btnSelectSitePDF.TabIndex = 49;
            this.btnSelectSitePDF.Text = "Select";
            this.btnSelectSitePDF.UseVisualStyleBackColor = true;
            this.btnSelectSitePDF.Click += new System.EventHandler(this.btnSelectSitePDF_Click_1);
            // 
            // btnOpenSitePDF
            // 
            this.btnOpenSitePDF.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOpenSitePDF.Location = new System.Drawing.Point(593, 229);
            this.btnOpenSitePDF.Name = "btnOpenSitePDF";
            this.btnOpenSitePDF.Size = new System.Drawing.Size(43, 18);
            this.btnOpenSitePDF.TabIndex = 48;
            this.btnOpenSitePDF.Text = "Open";
            this.btnOpenSitePDF.UseVisualStyleBackColor = true;
            this.btnOpenSitePDF.Click += new System.EventHandler(this.btnOpenSitePDF_Click);
            // 
            // ageTextBox
            // 
            this.ageTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.batchResourceBindingSource, "age", true));
            this.ageTextBox.FormattingEnabled = true;
            this.ageTextBox.Items.AddRange(new object[] {
            "P",
            "H",
            "P/H",
            "B"});
            this.ageTextBox.Location = new System.Drawing.Point(240, 95);
            this.ageTextBox.Name = "ageTextBox";
            this.ageTextBox.Size = new System.Drawing.Size(90, 21);
            this.ageTextBox.TabIndex = 3;
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPage8);
            this.tabControl2.Controls.Add(this.tabPage9);
            this.tabControl2.Controls.Add(this.tabPage12);
            this.tabControl2.Controls.Add(this.tabPage17);
            this.tabControl2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl2.Location = new System.Drawing.Point(77, 275);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(613, 206);
            this.tabControl2.TabIndex = 10;
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.dataGridView2);
            this.tabPage8.Location = new System.Drawing.Point(4, 21);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage8.Size = new System.Drawing.Size(605, 181);
            this.tabPage8.TabIndex = 0;
            this.tabPage8.Text = "Match List";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView2.Location = new System.Drawing.Point(3, 3);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.Size = new System.Drawing.Size(599, 175);
            this.dataGridView2.TabIndex = 31;
            this.dataGridView2.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2__CellDoubleClick);
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this.tableLayoutPanel1);
            this.tabPage9.Location = new System.Drawing.Point(4, 21);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage9.Size = new System.Drawing.Size(605, 181);
            this.tabPage9.TabIndex = 1;
            this.tabPage9.Text = "Reports";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.dgvResourceReport, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnResourceAddReport, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(599, 175);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // dgvResourceReport
            // 
            this.dgvResourceReport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvResourceReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvResourceReport.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvResourceReport.Location = new System.Drawing.Point(3, 3);
            this.dgvResourceReport.Name = "dgvResourceReport";
            this.dgvResourceReport.Size = new System.Drawing.Size(593, 146);
            this.dgvResourceReport.TabIndex = 0;
            // 
            // btnResourceAddReport
            // 
            this.btnResourceAddReport.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnResourceAddReport.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnResourceAddReport.Location = new System.Drawing.Point(535, 155);
            this.btnResourceAddReport.Name = "btnResourceAddReport";
            this.btnResourceAddReport.Size = new System.Drawing.Size(61, 17);
            this.btnResourceAddReport.TabIndex = 1;
            this.btnResourceAddReport.Text = "Add";
            this.btnResourceAddReport.UseVisualStyleBackColor = true;
            this.btnResourceAddReport.Click += new System.EventHandler(this.btnResourceAddReport_Click);
            // 
            // tabPage12
            // 
            this.tabPage12.Controls.Add(this.tableLayoutPanel2);
            this.tabPage12.Location = new System.Drawing.Point(4, 21);
            this.tabPage12.Name = "tabPage12";
            this.tabPage12.Size = new System.Drawing.Size(605, 181);
            this.tabPage12.TabIndex = 2;
            this.tabPage12.Text = "Cross Reference";
            this.tabPage12.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.button1, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.dgvResourceXref, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(605, 181);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Right;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(541, 161);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(61, 17);
            this.button1.TabIndex = 2;
            this.button1.Text = "Add";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // dgvResourceXref
            // 
            this.dgvResourceXref.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvResourceXref.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvResourceXref.Location = new System.Drawing.Point(3, 3);
            this.dgvResourceXref.Name = "dgvResourceXref";
            this.dgvResourceXref.Size = new System.Drawing.Size(599, 152);
            this.dgvResourceXref.TabIndex = 0;
            // 
            // tabPage17
            // 
            this.tabPage17.Location = new System.Drawing.Point(4, 21);
            this.tabPage17.Name = "tabPage17";
            this.tabPage17.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage17.Size = new System.Drawing.Size(605, 181);
            this.tabPage17.TabIndex = 3;
            this.tabPage17.Text = "MSUP";
            this.tabPage17.UseVisualStyleBackColor = true;
            // 
            // land_ownerTextBox
            // 
            this.land_ownerTextBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.land_ownerTextBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.land_ownerTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.batchResourceBindingSource, "land_owner", true));
            this.land_ownerTextBox.Location = new System.Drawing.Point(79, 117);
            this.land_ownerTextBox.Name = "land_ownerTextBox";
            this.land_ownerTextBox.Size = new System.Drawing.Size(100, 21);
            this.land_ownerTextBox.TabIndex = 2;
            // 
            // nrhp_eligibilityTextBox
            // 
            this.nrhp_eligibilityTextBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.nrhp_eligibilityTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.batchResourceBindingSource, "nrhp_eligibility", true));
            this.nrhp_eligibilityTextBox.DataSource = this.lookupeligibilityBindingSource;
            this.nrhp_eligibilityTextBox.DisplayMember = "lutext";
            this.nrhp_eligibilityTextBox.Location = new System.Drawing.Point(78, 95);
            this.nrhp_eligibilityTextBox.Name = "nrhp_eligibilityTextBox";
            this.nrhp_eligibilityTextBox.Size = new System.Drawing.Size(101, 21);
            this.nrhp_eligibilityTextBox.TabIndex = 1;
            this.nrhp_eligibilityTextBox.ValueMember = "lutext";
            // 
            // tabControl3
            // 
            this.tabControl3.Controls.Add(this.tabPage10);
            this.tabControl3.Controls.Add(this.tabPage11);
            this.tabControl3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl3.Location = new System.Drawing.Point(17, 3);
            this.tabControl3.Name = "tabControl3";
            this.tabControl3.SelectedIndex = 0;
            this.tabControl3.Size = new System.Drawing.Size(673, 70);
            this.tabControl3.TabIndex = 46;
            // 
            // tabPage10
            // 
            this.tabPage10.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPage10.Controls.Add(hrinumberLabel);
            this.tabPage10.Controls.Add(this.hrinumberTextBox);
            this.tabPage10.Controls.Add(label6);
            this.tabPage10.Controls.Add(this.textBox1);
            this.tabPage10.Controls.Add(label7);
            this.tabPage10.Controls.Add(this.textBox2);
            this.tabPage10.Controls.Add(label8);
            this.tabPage10.Controls.Add(this.textBox3);
            this.tabPage10.Location = new System.Drawing.Point(4, 21);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage10.Size = new System.Drawing.Size(665, 45);
            this.tabPage10.TabIndex = 0;
            this.tabPage10.Text = "Labels";
            // 
            // hrinumberTextBox
            // 
            this.hrinumberTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.batchResourceBindingSource, "hrinumber", true));
            this.hrinumberTextBox.Location = new System.Drawing.Point(436, 18);
            this.hrinumberTextBox.Name = "hrinumberTextBox";
            this.hrinumberTextBox.Size = new System.Drawing.Size(212, 18);
            this.hrinumberTextBox.TabIndex = 25;
            // 
            // textBox1
            // 
            this.textBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.batchResourceBindingSource, "fslabel", true));
            this.textBox1.Enabled = false;
            this.textBox1.Location = new System.Drawing.Point(244, 18);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(189, 18);
            this.textBox1.TabIndex = 2;
            // 
            // textBox2
            // 
            this.textBox2.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.batchResourceBindingSource, "trinomiallabel", true));
            this.textBox2.Enabled = false;
            this.textBox2.Location = new System.Drawing.Point(112, 18);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(129, 18);
            this.textBox2.TabIndex = 1;
            // 
            // textBox3
            // 
            this.textBox3.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.batchResourceBindingSource, "primarylabel", true));
            this.textBox3.Enabled = false;
            this.textBox3.Location = new System.Drawing.Point(9, 18);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(100, 18);
            this.textBox3.TabIndex = 0;
            // 
            // tabPage11
            // 
            this.tabPage11.AutoScroll = true;
            this.tabPage11.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPage11.Controls.Add(this.label10);
            this.tabPage11.Controls.Add(this.comboBox1);
            this.tabPage11.Controls.Add(trinhLabel);
            this.tabPage11.Controls.Add(this.trinhTextBox);
            this.tabPage11.Controls.Add(this.txtfsnump2);
            this.tabPage11.Controls.Add(this.txtfsnump1);
            this.tabPage11.Controls.Add(this.txtfsforest);
            this.tabPage11.Controls.Add(this.txtfsregion);
            this.tabPage11.Controls.Add(this.txttrinno);
            this.tabPage11.Controls.Add(this.txtprimno);
            this.tabPage11.Controls.Add(this.primcoLabel);
            this.tabPage11.Controls.Add(this.fsnump2Label);
            this.tabPage11.Controls.Add(this.fsnump1Label);
            this.tabPage11.Controls.Add(this.forestLabel);
            this.tabPage11.Controls.Add(this.fsregionLabel);
            this.tabPage11.Controls.Add(this.trinnoLabel);
            this.tabPage11.Controls.Add(this.primnoLabel);
            this.tabPage11.Controls.Add(this.txtprimco);
            this.tabPage11.Location = new System.Drawing.Point(4, 21);
            this.tabPage11.Name = "tabPage11";
            this.tabPage11.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage11.Size = new System.Drawing.Size(665, 45);
            this.tabPage11.TabIndex = 1;
            this.tabPage11.Text = "Edit Labels";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(161, 3);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(35, 13);
            this.label10.TabIndex = 61;
            this.label10.Text = "trinco:";
            // 
            // comboBox1
            // 
            this.comboBox1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.comboBox1.CausesValidation = false;
            this.comboBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.batchResourceBindingSource, "primco", true));
            this.comboBox1.DataSource = this.lookupcountycodesBindingSource;
            this.comboBox1.DisplayMember = "trinco";
            this.comboBox1.Location = new System.Drawing.Point(164, 19);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(49, 20);
            this.comboBox1.TabIndex = 4;
            this.comboBox1.ValueMember = "primco";
            // 
            // trinhTextBox
            // 
            this.trinhTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.batchResourceBindingSource, "trinh", true));
            this.trinhTextBox.Items.AddRange(new object[] {
            "H",
            "/H"});
            this.trinhTextBox.Location = new System.Drawing.Point(314, 19);
            this.trinhTextBox.Name = "trinhTextBox";
            this.trinhTextBox.Size = new System.Drawing.Size(39, 20);
            this.trinhTextBox.TabIndex = 6;
            // 
            // txtfsnump2
            // 
            this.txtfsnump2.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.batchResourceBindingSource, "fsnump2", true));
            this.txtfsnump2.Location = new System.Drawing.Point(507, 19);
            this.txtfsnump2.Name = "txtfsnump2";
            this.txtfsnump2.Size = new System.Drawing.Size(56, 18);
            this.txtfsnump2.TabIndex = 9;
            // 
            // txtfsnump1
            // 
            this.txtfsnump1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.batchResourceBindingSource, "fsnump1", true));
            this.txtfsnump1.Location = new System.Drawing.Point(446, 19);
            this.txtfsnump1.Name = "txtfsnump1";
            this.txtfsnump1.Size = new System.Drawing.Size(59, 18);
            this.txtfsnump1.TabIndex = 8;
            // 
            // txtfsforest
            // 
            this.txtfsforest.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.batchResourceBindingSource, "fsforest", true));
            this.txtfsforest.Location = new System.Drawing.Point(402, 19);
            this.txtfsforest.Name = "txtfsforest";
            this.txtfsforest.Size = new System.Drawing.Size(43, 18);
            this.txtfsforest.TabIndex = 7;
            // 
            // txtfsregion
            // 
            this.txtfsregion.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.batchResourceBindingSource, "fsregion", true));
            this.txtfsregion.Location = new System.Drawing.Point(359, 19);
            this.txtfsregion.Name = "txtfsregion";
            this.txtfsregion.Size = new System.Drawing.Size(41, 18);
            this.txtfsregion.TabIndex = 55;
            // 
            // txttrinno
            // 
            this.txttrinno.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.batchResourceBindingSource, "trinno", true));
            this.txttrinno.Location = new System.Drawing.Point(213, 19);
            this.txttrinno.Name = "txttrinno";
            this.txttrinno.Size = new System.Drawing.Size(100, 18);
            this.txttrinno.TabIndex = 5;
            // 
            // txtprimno
            // 
            this.txtprimno.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.batchResourceBindingSource, "primno", true));
            this.txtprimno.Location = new System.Drawing.Point(63, 19);
            this.txtprimno.Name = "txtprimno";
            this.txtprimno.Size = new System.Drawing.Size(100, 18);
            this.txtprimno.TabIndex = 3;
            // 
            // primcoLabel
            // 
            this.primcoLabel.AutoSize = true;
            this.primcoLabel.Location = new System.Drawing.Point(9, 3);
            this.primcoLabel.Name = "primcoLabel";
            this.primcoLabel.Size = new System.Drawing.Size(40, 13);
            this.primcoLabel.TabIndex = 52;
            this.primcoLabel.Text = "primco:";
            // 
            // fsnump2Label
            // 
            this.fsnump2Label.AutoSize = true;
            this.fsnump2Label.Location = new System.Drawing.Point(507, 3);
            this.fsnump2Label.Name = "fsnump2Label";
            this.fsnump2Label.Size = new System.Drawing.Size(50, 13);
            this.fsnump2Label.TabIndex = 51;
            this.fsnump2Label.Text = "fsnump2:";
            // 
            // fsnump1Label
            // 
            this.fsnump1Label.AutoSize = true;
            this.fsnump1Label.Location = new System.Drawing.Point(451, 3);
            this.fsnump1Label.Name = "fsnump1Label";
            this.fsnump1Label.Size = new System.Drawing.Size(50, 13);
            this.fsnump1Label.TabIndex = 50;
            this.fsnump1Label.Text = "fsnump1:";
            // 
            // forestLabel
            // 
            this.forestLabel.AutoSize = true;
            this.forestLabel.Location = new System.Drawing.Point(399, 3);
            this.forestLabel.Name = "forestLabel";
            this.forestLabel.Size = new System.Drawing.Size(44, 13);
            this.forestLabel.TabIndex = 49;
            this.forestLabel.Text = "fsforest:";
            // 
            // fsregionLabel
            // 
            this.fsregionLabel.AutoSize = true;
            this.fsregionLabel.Location = new System.Drawing.Point(359, 3);
            this.fsregionLabel.Name = "fsregionLabel";
            this.fsregionLabel.Size = new System.Drawing.Size(47, 13);
            this.fsregionLabel.TabIndex = 48;
            this.fsregionLabel.Text = "fsregion:";
            // 
            // trinnoLabel
            // 
            this.trinnoLabel.AutoSize = true;
            this.trinnoLabel.Location = new System.Drawing.Point(210, 3);
            this.trinnoLabel.Name = "trinnoLabel";
            this.trinnoLabel.Size = new System.Drawing.Size(36, 13);
            this.trinnoLabel.TabIndex = 47;
            this.trinnoLabel.Text = "trinno:";
            // 
            // primnoLabel
            // 
            this.primnoLabel.AutoSize = true;
            this.primnoLabel.Location = new System.Drawing.Point(60, 3);
            this.primnoLabel.Name = "primnoLabel";
            this.primnoLabel.Size = new System.Drawing.Size(41, 13);
            this.primnoLabel.TabIndex = 46;
            this.primnoLabel.Text = "primno:";
            // 
            // txtprimco
            // 
            this.txtprimco.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.batchResourceBindingSource, "primco", true));
            this.txtprimco.DataSource = this.lookupcountycodesBindingSource;
            this.txtprimco.DisplayMember = "primco";
            this.txtprimco.Location = new System.Drawing.Point(12, 19);
            this.txtprimco.Name = "txtprimco";
            this.txtprimco.Size = new System.Drawing.Size(49, 20);
            this.txtprimco.TabIndex = 0;
            // 
            // bindingNavigator1
            // 
            this.bindingNavigator1.AddNewItem = this.bindingNavigatorAddNewItem;
            this.bindingNavigator1.BindingSource = this.batchResourceBindingSource;
            this.bindingNavigator1.CountItem = this.bindingNavigatorCountItem;
            this.bindingNavigator1.DeleteItem = this.bindingNavigatorDeleteItem;
            this.bindingNavigator1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bindingNavigator1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem});
            this.bindingNavigator1.Location = new System.Drawing.Point(3, 478);
            this.bindingNavigator1.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.bindingNavigator1.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.bindingNavigator1.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.bindingNavigator1.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.bindingNavigator1.Name = "bindingNavigator1";
            this.bindingNavigator1.PositionItem = this.bindingNavigatorPositionItem;
            this.bindingNavigator1.Size = new System.Drawing.Size(736, 25);
            this.bindingNavigator1.TabIndex = 14;
            this.bindingNavigator1.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            this.bindingNavigatorAddNewItem.Click += new System.EventHandler(this.bindingNavigatorAddNewItem_Click);
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            this.bindingNavigatorDeleteItem.Click += new System.EventHandler(this.bindingNavigatorDeleteItem_Click);
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            this.bindingNavigatorMoveFirstItem.Click += new System.EventHandler(this.bindingNavigatorMoveFirstItem_Click);
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            this.bindingNavigatorMovePreviousItem.Click += new System.EventHandler(this.bindingNavigatorMovePreviousItem_Click);
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            this.bindingNavigatorPositionItem.Click += new System.EventHandler(this.bindingNavigatorPositionItem_Click);
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            this.bindingNavigatorMoveNextItem.Click += new System.EventHandler(this.bindingNavigatorMoveNextItem_Click_1);
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            this.bindingNavigatorMoveLastItem.Click += new System.EventHandler(this.bindingNavigatorMoveLastItem_Click);
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // tabPage3
            // 
            this.tabPage3.AutoScroll = true;
            this.tabPage3.Controls.Add(this.reportList);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(756, 538);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Batch Reports";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // reportList
            // 
            this.reportList.Controls.Add(this.tabPage6);
            this.reportList.Controls.Add(this.tabReportDetail);
            this.reportList.Controls.Add(this.tabReportMPE);
            this.reportList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.reportList.Location = new System.Drawing.Point(0, 0);
            this.reportList.Name = "reportList";
            this.reportList.SelectedIndex = 0;
            this.reportList.Size = new System.Drawing.Size(756, 538);
            this.reportList.TabIndex = 0;
            this.reportList.Click += new System.EventHandler(this.reportList_Click);
            // 
            // tabPage6
            // 
            this.tabPage6.AutoScroll = true;
            this.tabPage6.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPage6.Controls.Add(this.chkMissingReportGIS);
            this.tabPage6.Controls.Add(this.buttonRefresh2);
            this.tabPage6.Controls.Add(this.btnReportAddFilter);
            this.tabPage6.Controls.Add(this.btnReportClearFilter);
            this.tabPage6.Controls.Add(this.btnReportFilter);
            this.tabPage6.Controls.Add(this.txtReportSelectValue);
            this.tabPage6.Controls.Add(this.drpReportSelect);
            this.tabPage6.Controls.Add(this.lblReportFilter);
            this.tabPage6.Controls.Add(this.reportsDataGridView);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(748, 512);
            this.tabPage6.TabIndex = 0;
            this.tabPage6.Text = "List View";
            // 
            // chkMissingReportGIS
            // 
            this.chkMissingReportGIS.AutoSize = true;
            this.chkMissingReportGIS.Location = new System.Drawing.Point(551, 3);
            this.chkMissingReportGIS.Name = "chkMissingReportGIS";
            this.chkMissingReportGIS.Size = new System.Drawing.Size(82, 17);
            this.chkMissingReportGIS.TabIndex = 24;
            this.chkMissingReportGIS.Text = "Missing GIS";
            this.chkMissingReportGIS.UseVisualStyleBackColor = true;
            this.chkMissingReportGIS.CheckedChanged += new System.EventHandler(this.chkMissingReportGIS_CheckedChanged);
            // 
            // buttonRefresh2
            // 
            this.buttonRefresh2.Image = global::PGELibraryAddin.Properties.Resources.arrow_refresh;
            this.buttonRefresh2.Location = new System.Drawing.Point(713, 3);
            this.buttonRefresh2.Name = "buttonRefresh2";
            this.buttonRefresh2.Size = new System.Drawing.Size(32, 23);
            this.buttonRefresh2.TabIndex = 21;
            this.buttonRefresh2.UseVisualStyleBackColor = true;
            this.buttonRefresh2.Click += new System.EventHandler(this.buttonRefresh2_Click);
            // 
            // btnReportAddFilter
            // 
            this.btnReportAddFilter.Location = new System.Drawing.Point(469, 1);
            this.btnReportAddFilter.Name = "btnReportAddFilter";
            this.btnReportAddFilter.Size = new System.Drawing.Size(75, 23);
            this.btnReportAddFilter.TabIndex = 20;
            this.btnReportAddFilter.Text = "Add to Filter";
            this.btnReportAddFilter.UseVisualStyleBackColor = true;
            this.btnReportAddFilter.Click += new System.EventHandler(this.btnReportAddFilter_Click);
            // 
            // btnReportClearFilter
            // 
            this.btnReportClearFilter.Location = new System.Drawing.Point(636, 1);
            this.btnReportClearFilter.Name = "btnReportClearFilter";
            this.btnReportClearFilter.Size = new System.Drawing.Size(75, 23);
            this.btnReportClearFilter.TabIndex = 19;
            this.btnReportClearFilter.Text = "Clear filters";
            this.btnReportClearFilter.UseVisualStyleBackColor = true;
            this.btnReportClearFilter.Click += new System.EventHandler(this.btnReportClearFilter_Click_1);
            // 
            // btnReportFilter
            // 
            this.btnReportFilter.Location = new System.Drawing.Point(391, 1);
            this.btnReportFilter.Name = "btnReportFilter";
            this.btnReportFilter.Size = new System.Drawing.Size(75, 23);
            this.btnReportFilter.TabIndex = 17;
            this.btnReportFilter.Text = "Filter";
            this.btnReportFilter.UseVisualStyleBackColor = true;
            this.btnReportFilter.Click += new System.EventHandler(this.btnReportFilter_Click);
            // 
            // txtReportSelectValue
            // 
            this.txtReportSelectValue.Location = new System.Drawing.Point(265, 3);
            this.txtReportSelectValue.Name = "txtReportSelectValue";
            this.txtReportSelectValue.Size = new System.Drawing.Size(120, 20);
            this.txtReportSelectValue.TabIndex = 18;
            // 
            // drpReportSelect
            // 
            this.drpReportSelect.FormattingEnabled = true;
            this.drpReportSelect.Items.AddRange(new object[] {
            "reportid",
            "reportnumber",
            "label",
            "author",
            "year",
            "reporttitle"});
            this.drpReportSelect.Location = new System.Drawing.Point(102, 3);
            this.drpReportSelect.Name = "drpReportSelect";
            this.drpReportSelect.Size = new System.Drawing.Size(157, 21);
            this.drpReportSelect.TabIndex = 15;
            // 
            // lblReportFilter
            // 
            this.lblReportFilter.AutoSize = true;
            this.lblReportFilter.Location = new System.Drawing.Point(6, 3);
            this.lblReportFilter.Name = "lblReportFilter";
            this.lblReportFilter.Size = new System.Drawing.Size(88, 13);
            this.lblReportFilter.TabIndex = 1;
            this.lblReportFilter.Text = "Select filter value";
            // 
            // reportsDataGridView
            // 
            this.reportsDataGridView.AllowUserToAddRows = false;
            this.reportsDataGridView.AllowUserToDeleteRows = false;
            this.reportsDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.reportsDataGridView.AutoGenerateColumns = false;
            this.reportsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.reportsDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.reportidDataGridViewTextBoxColumn,
            this.labelDataGridViewTextBoxColumn,
            this.reportnumberDataGridViewTextBoxColumn,
            this.reporttitleDataGridViewTextBoxColumn,
            this.yearDataGridViewTextBoxColumn,
            this.authorDataGridViewTextBoxColumn,
            this.agencycompanyDataGridViewTextBoxColumn,
            this.preparedforDataGridViewTextBoxColumn,
            this.linearblockDataGridViewTextBoxColumn,
            this.linearwidthDataGridViewTextBoxColumn,
            this.reportareaDataGridViewTextBoxColumn,
            this.areaunitsDataGridViewTextBoxColumn,
            this.transectspacingDataGridViewTextBoxColumn,
            this.silDataGridViewTextBoxColumn,
            this.commentsDataGridViewTextBoxColumn1,
            this.temptitleDataGridViewTextBoxColumn,
            this.reporttypeDataGridViewTextBoxColumn,
            this.pdfDataGridViewTextBoxColumn1,
            this.origpathDataGridViewTextBoxColumn,
            this.reportdatastatusDataGridViewTextBoxColumn,
            this.sitevisitDataGridViewTextBoxColumn,
            this.createdbyDataGridViewTextBoxColumn,
            this.createddateDataGridViewTextBoxColumn,
            this.editedbyDataGridViewTextBoxColumn,
            this.editeddateDataGridViewTextBoxColumn,
            this.origreportidDataGridViewTextBoxColumn,
            this.createduserDataGridViewTextBoxColumn,
            this.createddateDataGridViewTextBoxColumn1,
            this.lastediteduserDataGridViewTextBoxColumn,
            this.lastediteddateDataGridViewTextBoxColumn});
            this.reportsDataGridView.DataSource = this.batchReportBindingSource;
            this.reportsDataGridView.Location = new System.Drawing.Point(3, 26);
            this.reportsDataGridView.Name = "reportsDataGridView";
            this.reportsDataGridView.Size = new System.Drawing.Size(742, 473);
            this.reportsDataGridView.TabIndex = 0;
            this.reportsDataGridView.UserAddedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.reportsDataGridView_UserAddedRow);
            // 
            // reportidDataGridViewTextBoxColumn
            // 
            this.reportidDataGridViewTextBoxColumn.DataPropertyName = "reportid";
            this.reportidDataGridViewTextBoxColumn.HeaderText = "reportid";
            this.reportidDataGridViewTextBoxColumn.Name = "reportidDataGridViewTextBoxColumn";
            // 
            // labelDataGridViewTextBoxColumn
            // 
            this.labelDataGridViewTextBoxColumn.DataPropertyName = "label";
            this.labelDataGridViewTextBoxColumn.HeaderText = "label";
            this.labelDataGridViewTextBoxColumn.Name = "labelDataGridViewTextBoxColumn";
            // 
            // reportnumberDataGridViewTextBoxColumn
            // 
            this.reportnumberDataGridViewTextBoxColumn.DataPropertyName = "reportnumber";
            this.reportnumberDataGridViewTextBoxColumn.HeaderText = "reportnumber";
            this.reportnumberDataGridViewTextBoxColumn.Name = "reportnumberDataGridViewTextBoxColumn";
            // 
            // reporttitleDataGridViewTextBoxColumn
            // 
            this.reporttitleDataGridViewTextBoxColumn.DataPropertyName = "reporttitle";
            this.reporttitleDataGridViewTextBoxColumn.HeaderText = "reporttitle";
            this.reporttitleDataGridViewTextBoxColumn.Name = "reporttitleDataGridViewTextBoxColumn";
            // 
            // yearDataGridViewTextBoxColumn
            // 
            this.yearDataGridViewTextBoxColumn.DataPropertyName = "year";
            this.yearDataGridViewTextBoxColumn.HeaderText = "year";
            this.yearDataGridViewTextBoxColumn.Name = "yearDataGridViewTextBoxColumn";
            // 
            // authorDataGridViewTextBoxColumn
            // 
            this.authorDataGridViewTextBoxColumn.DataPropertyName = "author";
            this.authorDataGridViewTextBoxColumn.HeaderText = "author";
            this.authorDataGridViewTextBoxColumn.Name = "authorDataGridViewTextBoxColumn";
            // 
            // agencycompanyDataGridViewTextBoxColumn
            // 
            this.agencycompanyDataGridViewTextBoxColumn.DataPropertyName = "agencycompany";
            this.agencycompanyDataGridViewTextBoxColumn.HeaderText = "agencycompany";
            this.agencycompanyDataGridViewTextBoxColumn.Name = "agencycompanyDataGridViewTextBoxColumn";
            // 
            // preparedforDataGridViewTextBoxColumn
            // 
            this.preparedforDataGridViewTextBoxColumn.DataPropertyName = "prepared_for";
            this.preparedforDataGridViewTextBoxColumn.HeaderText = "prepared_for";
            this.preparedforDataGridViewTextBoxColumn.Name = "preparedforDataGridViewTextBoxColumn";
            // 
            // linearblockDataGridViewTextBoxColumn
            // 
            this.linearblockDataGridViewTextBoxColumn.DataPropertyName = "linearblock";
            this.linearblockDataGridViewTextBoxColumn.HeaderText = "linearblock";
            this.linearblockDataGridViewTextBoxColumn.Name = "linearblockDataGridViewTextBoxColumn";
            // 
            // linearwidthDataGridViewTextBoxColumn
            // 
            this.linearwidthDataGridViewTextBoxColumn.DataPropertyName = "linearwidth";
            this.linearwidthDataGridViewTextBoxColumn.HeaderText = "linearwidth";
            this.linearwidthDataGridViewTextBoxColumn.Name = "linearwidthDataGridViewTextBoxColumn";
            // 
            // reportareaDataGridViewTextBoxColumn
            // 
            this.reportareaDataGridViewTextBoxColumn.DataPropertyName = "reportarea";
            this.reportareaDataGridViewTextBoxColumn.HeaderText = "reportarea";
            this.reportareaDataGridViewTextBoxColumn.Name = "reportareaDataGridViewTextBoxColumn";
            // 
            // areaunitsDataGridViewTextBoxColumn
            // 
            this.areaunitsDataGridViewTextBoxColumn.DataPropertyName = "areaunits";
            this.areaunitsDataGridViewTextBoxColumn.HeaderText = "areaunits";
            this.areaunitsDataGridViewTextBoxColumn.Name = "areaunitsDataGridViewTextBoxColumn";
            // 
            // transectspacingDataGridViewTextBoxColumn
            // 
            this.transectspacingDataGridViewTextBoxColumn.DataPropertyName = "transectspacing";
            this.transectspacingDataGridViewTextBoxColumn.HeaderText = "transectspacing";
            this.transectspacingDataGridViewTextBoxColumn.Name = "transectspacingDataGridViewTextBoxColumn";
            // 
            // silDataGridViewTextBoxColumn
            // 
            this.silDataGridViewTextBoxColumn.DataPropertyName = "sil";
            this.silDataGridViewTextBoxColumn.HeaderText = "sil";
            this.silDataGridViewTextBoxColumn.Name = "silDataGridViewTextBoxColumn";
            // 
            // commentsDataGridViewTextBoxColumn1
            // 
            this.commentsDataGridViewTextBoxColumn1.DataPropertyName = "comments";
            this.commentsDataGridViewTextBoxColumn1.HeaderText = "comments";
            this.commentsDataGridViewTextBoxColumn1.Name = "commentsDataGridViewTextBoxColumn1";
            // 
            // temptitleDataGridViewTextBoxColumn
            // 
            this.temptitleDataGridViewTextBoxColumn.DataPropertyName = "temptitle";
            this.temptitleDataGridViewTextBoxColumn.HeaderText = "temptitle";
            this.temptitleDataGridViewTextBoxColumn.Name = "temptitleDataGridViewTextBoxColumn";
            // 
            // reporttypeDataGridViewTextBoxColumn
            // 
            this.reporttypeDataGridViewTextBoxColumn.DataPropertyName = "reporttype";
            this.reporttypeDataGridViewTextBoxColumn.HeaderText = "reporttype";
            this.reporttypeDataGridViewTextBoxColumn.Name = "reporttypeDataGridViewTextBoxColumn";
            // 
            // pdfDataGridViewTextBoxColumn1
            // 
            this.pdfDataGridViewTextBoxColumn1.DataPropertyName = "pdf";
            this.pdfDataGridViewTextBoxColumn1.HeaderText = "pdf";
            this.pdfDataGridViewTextBoxColumn1.Name = "pdfDataGridViewTextBoxColumn1";
            // 
            // origpathDataGridViewTextBoxColumn
            // 
            this.origpathDataGridViewTextBoxColumn.DataPropertyName = "origpath";
            this.origpathDataGridViewTextBoxColumn.HeaderText = "origpath";
            this.origpathDataGridViewTextBoxColumn.Name = "origpathDataGridViewTextBoxColumn";
            // 
            // reportdatastatusDataGridViewTextBoxColumn
            // 
            this.reportdatastatusDataGridViewTextBoxColumn.DataPropertyName = "reportdatastatus";
            this.reportdatastatusDataGridViewTextBoxColumn.HeaderText = "reportdatastatus";
            this.reportdatastatusDataGridViewTextBoxColumn.Name = "reportdatastatusDataGridViewTextBoxColumn";
            // 
            // sitevisitDataGridViewTextBoxColumn
            // 
            this.sitevisitDataGridViewTextBoxColumn.DataPropertyName = "sitevisit";
            this.sitevisitDataGridViewTextBoxColumn.HeaderText = "sitevisit";
            this.sitevisitDataGridViewTextBoxColumn.Name = "sitevisitDataGridViewTextBoxColumn";
            // 
            // createdbyDataGridViewTextBoxColumn
            // 
            this.createdbyDataGridViewTextBoxColumn.DataPropertyName = "createdby";
            this.createdbyDataGridViewTextBoxColumn.HeaderText = "createdby";
            this.createdbyDataGridViewTextBoxColumn.Name = "createdbyDataGridViewTextBoxColumn";
            // 
            // createddateDataGridViewTextBoxColumn
            // 
            this.createddateDataGridViewTextBoxColumn.DataPropertyName = "createddate";
            this.createddateDataGridViewTextBoxColumn.HeaderText = "createddate";
            this.createddateDataGridViewTextBoxColumn.Name = "createddateDataGridViewTextBoxColumn";
            // 
            // editedbyDataGridViewTextBoxColumn
            // 
            this.editedbyDataGridViewTextBoxColumn.DataPropertyName = "editedby";
            this.editedbyDataGridViewTextBoxColumn.HeaderText = "editedby";
            this.editedbyDataGridViewTextBoxColumn.Name = "editedbyDataGridViewTextBoxColumn";
            // 
            // editeddateDataGridViewTextBoxColumn
            // 
            this.editeddateDataGridViewTextBoxColumn.DataPropertyName = "editeddate";
            this.editeddateDataGridViewTextBoxColumn.HeaderText = "editeddate";
            this.editeddateDataGridViewTextBoxColumn.Name = "editeddateDataGridViewTextBoxColumn";
            // 
            // origreportidDataGridViewTextBoxColumn
            // 
            this.origreportidDataGridViewTextBoxColumn.DataPropertyName = "origreportid";
            this.origreportidDataGridViewTextBoxColumn.HeaderText = "origreportid";
            this.origreportidDataGridViewTextBoxColumn.Name = "origreportidDataGridViewTextBoxColumn";
            // 
            // createduserDataGridViewTextBoxColumn
            // 
            this.createduserDataGridViewTextBoxColumn.DataPropertyName = "created_user";
            this.createduserDataGridViewTextBoxColumn.HeaderText = "created_user";
            this.createduserDataGridViewTextBoxColumn.Name = "createduserDataGridViewTextBoxColumn";
            // 
            // createddateDataGridViewTextBoxColumn1
            // 
            this.createddateDataGridViewTextBoxColumn1.DataPropertyName = "created_date";
            this.createddateDataGridViewTextBoxColumn1.HeaderText = "created_date";
            this.createddateDataGridViewTextBoxColumn1.Name = "createddateDataGridViewTextBoxColumn1";
            // 
            // lastediteduserDataGridViewTextBoxColumn
            // 
            this.lastediteduserDataGridViewTextBoxColumn.DataPropertyName = "last_edited_user";
            this.lastediteduserDataGridViewTextBoxColumn.HeaderText = "last_edited_user";
            this.lastediteduserDataGridViewTextBoxColumn.Name = "lastediteduserDataGridViewTextBoxColumn";
            // 
            // lastediteddateDataGridViewTextBoxColumn
            // 
            this.lastediteddateDataGridViewTextBoxColumn.DataPropertyName = "last_edited_date";
            this.lastediteddateDataGridViewTextBoxColumn.HeaderText = "last_edited_date";
            this.lastediteddateDataGridViewTextBoxColumn.Name = "lastediteddateDataGridViewTextBoxColumn";
            // 
            // tabReportDetail
            // 
            this.tabReportDetail.AutoScroll = true;
            this.tabReportDetail.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabReportDetail.Controls.Add(this.chkReportMatchDisabled);
            this.tabReportDetail.Controls.Add(label14);
            this.tabReportDetail.Controls.Add(this.textBox4);
            this.tabReportDetail.Controls.Add(this.label12);
            this.tabReportDetail.Controls.Add(label13);
            this.tabReportDetail.Controls.Add(this.comboReportStatus);
            this.tabReportDetail.Controls.Add(this.comboReportGroup);
            this.tabReportDetail.Controls.Add(this.cboReportType);
            this.tabReportDetail.Controls.Add(this.chkSiteVisit);
            this.tabReportDetail.Controls.Add(this.btnReportsUpdateMatch);
            this.tabReportDetail.Controls.Add(this.btnOpenOrigReportPDF);
            this.tabReportDetail.Controls.Add(this.btnSelectReportPDF);
            this.tabReportDetail.Controls.Add(this.btnOpenReportPDF);
            this.tabReportDetail.Controls.Add(this.tabControl4);
            this.tabReportDetail.Controls.Add(lblComment);
            this.tabReportDetail.Controls.Add(this.txtComment);
            this.tabReportDetail.Controls.Add(lblTempTitle);
            this.tabReportDetail.Controls.Add(lblOrigPath);
            this.tabReportDetail.Controls.Add(this.txtOrigPath);
            this.tabReportDetail.Controls.Add(this.txtSil);
            this.tabReportDetail.Controls.Add(this.txtTransectSpacing);
            this.tabReportDetail.Controls.Add(this.lblAreaUnits);
            this.tabReportDetail.Controls.Add(this.lblReportType);
            this.tabReportDetail.Controls.Add(this.lblSil);
            this.tabReportDetail.Controls.Add(this.lblTransectSpacing);
            this.tabReportDetail.Controls.Add(this.txtAreaUnits);
            this.tabReportDetail.Controls.Add(this.txtReportArea);
            this.tabReportDetail.Controls.Add(lblPDF);
            this.tabReportDetail.Controls.Add(this.txtPDF);
            this.tabReportDetail.Controls.Add(lblLinearWidth);
            this.tabReportDetail.Controls.Add(this.txtLinearWidth);
            this.tabReportDetail.Controls.Add(lblReportArea);
            this.tabReportDetail.Controls.Add(this.txtLinearBlock);
            this.tabReportDetail.Controls.Add(lblPreparedFor);
            this.tabReportDetail.Controls.Add(this.txtPreparedFor);
            this.tabReportDetail.Controls.Add(lblLinearBlock);
            this.tabReportDetail.Controls.Add(lblAuthor);
            this.tabReportDetail.Controls.Add(this.txtAuthor);
            this.tabReportDetail.Controls.Add(lblReportLabel);
            this.tabReportDetail.Controls.Add(this.txtReportLabel);
            this.tabReportDetail.Controls.Add(lblAgencyCompany);
            this.tabReportDetail.Controls.Add(this.txtAgencyCompany);
            this.tabReportDetail.Controls.Add(lblYear);
            this.tabReportDetail.Controls.Add(this.txtYear);
            this.tabReportDetail.Controls.Add(lblReportTitle);
            this.tabReportDetail.Controls.Add(this.txtReportTitle);
            this.tabReportDetail.Controls.Add(lblReportNumber);
            this.tabReportDetail.Controls.Add(this.txtReportNumber);
            this.tabReportDetail.Controls.Add(lblReportID);
            this.tabReportDetail.Controls.Add(this.txtReportID);
            this.tabReportDetail.Controls.Add(this.bindingNavigator2);
            this.tabReportDetail.Location = new System.Drawing.Point(4, 22);
            this.tabReportDetail.Name = "tabReportDetail";
            this.tabReportDetail.Padding = new System.Windows.Forms.Padding(3);
            this.tabReportDetail.Size = new System.Drawing.Size(748, 512);
            this.tabReportDetail.TabIndex = 1;
            this.tabReportDetail.Text = "Detail View";
            // 
            // chkReportMatchDisabled
            // 
            this.chkReportMatchDisabled.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkReportMatchDisabled.AutoSize = true;
            this.chkReportMatchDisabled.FlatAppearance.CheckedBackColor = System.Drawing.Color.Red;
            this.chkReportMatchDisabled.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkReportMatchDisabled.Location = new System.Drawing.Point(538, 280);
            this.chkReportMatchDisabled.Name = "chkReportMatchDisabled";
            this.chkReportMatchDisabled.Size = new System.Drawing.Size(89, 22);
            this.chkReportMatchDisabled.TabIndex = 102;
            this.chkReportMatchDisabled.Text = "Matching Enabled";
            this.chkReportMatchDisabled.UseVisualStyleBackColor = true;
            this.chkReportMatchDisabled.CheckedChanged += new System.EventHandler(this.chkReportMatchDisabled_CheckedChanged);
            // 
            // textBox4
            // 
            this.textBox4.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.batchReportBindingSource, "temptitle", true));
            this.textBox4.Location = new System.Drawing.Point(402, 13);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(146, 20);
            this.textBox4.TabIndex = 2;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(473, 148);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(48, 26);
            this.label12.TabIndex = 0;
            this.label12.Text = "reporting\r\ngroup:";
            // 
            // comboReportStatus
            // 
            this.comboReportStatus.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.batchReportBindingSource, "surveyadeq", true));
            this.comboReportStatus.FormattingEnabled = true;
            this.comboReportStatus.Items.AddRange(new object[] {
            "Adequate",
            "Not Adequate",
            "Unevaluated"});
            this.comboReportStatus.Location = new System.Drawing.Point(534, 172);
            this.comboReportStatus.Name = "comboReportStatus";
            this.comboReportStatus.Size = new System.Drawing.Size(121, 21);
            this.comboReportStatus.TabIndex = 15;
            // 
            // comboReportGroup
            // 
            this.comboReportGroup.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.batchReportBindingSource, "reportgroup", true));
            this.comboReportGroup.FormattingEnabled = true;
            this.comboReportGroup.Items.AddRange(new object[] {
            "None",
            "MSUP"});
            this.comboReportGroup.Location = new System.Drawing.Point(534, 148);
            this.comboReportGroup.Name = "comboReportGroup";
            this.comboReportGroup.Size = new System.Drawing.Size(121, 21);
            this.comboReportGroup.TabIndex = 14;
            // 
            // cboReportType
            // 
            this.cboReportType.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.batchReportBindingSource, "reporttype", true));
            this.cboReportType.DataSource = this.lookupReportTypebindingSource;
            this.cboReportType.DisplayMember = "reporttype";
            this.cboReportType.FormattingEnabled = true;
            this.cboReportType.Location = new System.Drawing.Point(421, 118);
            this.cboReportType.Name = "cboReportType";
            this.cboReportType.Size = new System.Drawing.Size(234, 21);
            this.cboReportType.TabIndex = 7;
            // 
            // chkSiteVisit
            // 
            this.chkSiteVisit.AutoSize = true;
            this.chkSiteVisit.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.batchReportBindingSource, "sitevisit", true));
            this.chkSiteVisit.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSiteVisit.Location = new System.Drawing.Point(67, 124);
            this.chkSiteVisit.Name = "chkSiteVisit";
            this.chkSiteVisit.Size = new System.Drawing.Size(66, 17);
            this.chkSiteVisit.TabIndex = 7;
            this.chkSiteVisit.Text = "Site Visit";
            this.chkSiteVisit.UseVisualStyleBackColor = true;
            // 
            // btnReportsUpdateMatch
            // 
            this.btnReportsUpdateMatch.Image = global::PGELibraryAddin.Properties.Resources.arrow_refresh;
            this.btnReportsUpdateMatch.Location = new System.Drawing.Point(633, 279);
            this.btnReportsUpdateMatch.Name = "btnReportsUpdateMatch";
            this.btnReportsUpdateMatch.Size = new System.Drawing.Size(20, 23);
            this.btnReportsUpdateMatch.TabIndex = 99;
            this.btnReportsUpdateMatch.UseVisualStyleBackColor = true;
            this.btnReportsUpdateMatch.Click += new System.EventHandler(this.btnReportsUpdateMatch_Click);
            // 
            // btnOpenOrigReportPDF
            // 
            this.btnOpenOrigReportPDF.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOpenOrigReportPDF.Location = new System.Drawing.Point(555, 259);
            this.btnOpenOrigReportPDF.Name = "btnOpenOrigReportPDF";
            this.btnOpenOrigReportPDF.Size = new System.Drawing.Size(43, 18);
            this.btnOpenOrigReportPDF.TabIndex = 22;
            this.btnOpenOrigReportPDF.Text = "Open";
            this.btnOpenOrigReportPDF.UseVisualStyleBackColor = true;
            this.btnOpenOrigReportPDF.Click += new System.EventHandler(this.btnOpenOrigReportPDF_Click_1);
            // 
            // btnSelectReportPDF
            // 
            this.btnSelectReportPDF.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelectReportPDF.Location = new System.Drawing.Point(597, 238);
            this.btnSelectReportPDF.Name = "btnSelectReportPDF";
            this.btnSelectReportPDF.Size = new System.Drawing.Size(43, 18);
            this.btnSelectReportPDF.TabIndex = 21;
            this.btnSelectReportPDF.Text = "Select";
            this.btnSelectReportPDF.UseVisualStyleBackColor = true;
            this.btnSelectReportPDF.Click += new System.EventHandler(this.btnSelectReportPDF_Click_1);
            // 
            // btnOpenReportPDF
            // 
            this.btnOpenReportPDF.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOpenReportPDF.Location = new System.Drawing.Point(555, 238);
            this.btnOpenReportPDF.Name = "btnOpenReportPDF";
            this.btnOpenReportPDF.Size = new System.Drawing.Size(43, 18);
            this.btnOpenReportPDF.TabIndex = 20;
            this.btnOpenReportPDF.Text = "Open";
            this.btnOpenReportPDF.UseVisualStyleBackColor = true;
            this.btnOpenReportPDF.Click += new System.EventHandler(this.btnOpenReportPDF_Click_1);
            // 
            // tabControl4
            // 
            this.tabControl4.Controls.Add(this.tabPage13);
            this.tabControl4.Controls.Add(this.tabPage14);
            this.tabControl4.Controls.Add(this.tabPage15);
            this.tabControl4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl4.Location = new System.Drawing.Point(51, 283);
            this.tabControl4.Name = "tabControl4";
            this.tabControl4.SelectedIndex = 0;
            this.tabControl4.Size = new System.Drawing.Size(604, 177);
            this.tabControl4.TabIndex = 25;
            // 
            // tabPage13
            // 
            this.tabPage13.Controls.Add(this.dataGridView3);
            this.tabPage13.Location = new System.Drawing.Point(4, 21);
            this.tabPage13.Name = "tabPage13";
            this.tabPage13.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage13.Size = new System.Drawing.Size(596, 152);
            this.tabPage13.TabIndex = 0;
            this.tabPage13.Text = "Matches";
            this.tabPage13.UseVisualStyleBackColor = true;
            // 
            // dataGridView3
            // 
            this.dataGridView3.AllowUserToAddRows = false;
            this.dataGridView3.AllowUserToDeleteRows = false;
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView3.Location = new System.Drawing.Point(3, 3);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.ReadOnly = true;
            this.dataGridView3.Size = new System.Drawing.Size(590, 146);
            this.dataGridView3.TabIndex = 95;
            this.dataGridView3.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView3_CellDoubleClick);
            // 
            // tabPage14
            // 
            this.tabPage14.Controls.Add(this.tableLayoutPanel4);
            this.tabPage14.Location = new System.Drawing.Point(4, 21);
            this.tabPage14.Name = "tabPage14";
            this.tabPage14.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage14.Size = new System.Drawing.Size(596, 152);
            this.tabPage14.TabIndex = 1;
            this.tabPage14.Text = "Resources";
            this.tabPage14.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.tnReportAddResource, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.dgvReportResource, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(590, 146);
            this.tableLayoutPanel4.TabIndex = 2;
            // 
            // tnReportAddResource
            // 
            this.tnReportAddResource.Dock = System.Windows.Forms.DockStyle.Right;
            this.tnReportAddResource.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tnReportAddResource.Location = new System.Drawing.Point(526, 126);
            this.tnReportAddResource.Name = "tnReportAddResource";
            this.tnReportAddResource.Size = new System.Drawing.Size(61, 17);
            this.tnReportAddResource.TabIndex = 3;
            this.tnReportAddResource.Text = "Add";
            this.tnReportAddResource.UseVisualStyleBackColor = true;
            this.tnReportAddResource.Click += new System.EventHandler(this.tnReportAddResource_Click);
            // 
            // dgvReportResource
            // 
            this.dgvReportResource.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvReportResource.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvReportResource.Location = new System.Drawing.Point(3, 3);
            this.dgvReportResource.Name = "dgvReportResource";
            this.dgvReportResource.Size = new System.Drawing.Size(584, 117);
            this.dgvReportResource.TabIndex = 0;
            // 
            // tabPage15
            // 
            this.tabPage15.Controls.Add(this.tableLayoutPanel3);
            this.tabPage15.Location = new System.Drawing.Point(4, 21);
            this.tabPage15.Name = "tabPage15";
            this.tabPage15.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage15.Size = new System.Drawing.Size(596, 152);
            this.tabPage15.TabIndex = 2;
            this.tabPage15.Text = "Cross Reference";
            this.tabPage15.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.btnReportAddXref, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.dgvReportXref, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(590, 146);
            this.tableLayoutPanel3.TabIndex = 2;
            // 
            // btnReportAddXref
            // 
            this.btnReportAddXref.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnReportAddXref.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReportAddXref.Location = new System.Drawing.Point(526, 126);
            this.btnReportAddXref.Name = "btnReportAddXref";
            this.btnReportAddXref.Size = new System.Drawing.Size(61, 17);
            this.btnReportAddXref.TabIndex = 3;
            this.btnReportAddXref.Text = "Add";
            this.btnReportAddXref.UseVisualStyleBackColor = true;
            this.btnReportAddXref.Click += new System.EventHandler(this.btnReportAddXref_Click);
            // 
            // dgvReportXref
            // 
            this.dgvReportXref.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvReportXref.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvReportXref.Location = new System.Drawing.Point(3, 3);
            this.dgvReportXref.Name = "dgvReportXref";
            this.dgvReportXref.Size = new System.Drawing.Size(584, 117);
            this.dgvReportXref.TabIndex = 0;
            // 
            // txtComment
            // 
            this.txtComment.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.batchReportBindingSource, "comments", true));
            this.txtComment.Location = new System.Drawing.Point(69, 199);
            this.txtComment.Multiline = true;
            this.txtComment.Name = "txtComment";
            this.txtComment.Size = new System.Drawing.Size(586, 35);
            this.txtComment.TabIndex = 16;
            // 
            // txtOrigPath
            // 
            this.txtOrigPath.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.batchReportBindingSource, "origpath", true));
            this.txtOrigPath.Location = new System.Drawing.Point(69, 258);
            this.txtOrigPath.Name = "txtOrigPath";
            this.txtOrigPath.Size = new System.Drawing.Size(481, 20);
            this.txtOrigPath.TabIndex = 18;
            this.txtOrigPath.Leave += new System.EventHandler(this.txtOrigPath_Leave);
            // 
            // txtSil
            // 
            this.txtSil.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.batchReportBindingSource, "sil", true));
            this.txtSil.DataSource = this.lookupSILBindingSource;
            this.txtSil.DisplayMember = "label";
            this.txtSil.Location = new System.Drawing.Point(357, 150);
            this.txtSil.Name = "txtSil";
            this.txtSil.Size = new System.Drawing.Size(100, 21);
            this.txtSil.TabIndex = 12;
            this.txtSil.ValueMember = "luid";
            // 
            // lookupSILBindingSource
            // 
            this.lookupSILBindingSource.DataMember = "lookupSIL";
            this.lookupSILBindingSource.DataSource = this.pgeresourcesDataSet;
            // 
            // txtTransectSpacing
            // 
            this.txtTransectSpacing.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.batchReportBindingSource, "transectspacing", true));
            this.txtTransectSpacing.Location = new System.Drawing.Point(208, 150);
            this.txtTransectSpacing.Name = "txtTransectSpacing";
            this.txtTransectSpacing.Size = new System.Drawing.Size(100, 20);
            this.txtTransectSpacing.TabIndex = 10;
            // 
            // lblAreaUnits
            // 
            this.lblAreaUnits.AutoSize = true;
            this.lblAreaUnits.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAreaUnits.Location = new System.Drawing.Point(16, 172);
            this.lblAreaUnits.Name = "lblAreaUnits";
            this.lblAreaUnits.Size = new System.Drawing.Size(32, 26);
            this.lblAreaUnits.TabIndex = 79;
            this.lblAreaUnits.Text = "area\r\nunits:";
            // 
            // lblReportType
            // 
            this.lblReportType.AutoSize = true;
            this.lblReportType.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReportType.Location = new System.Drawing.Point(340, 121);
            this.lblReportType.Name = "lblReportType";
            this.lblReportType.Size = new System.Drawing.Size(60, 13);
            this.lblReportType.TabIndex = 76;
            this.lblReportType.Text = "report type:";
            // 
            // lblSil
            // 
            this.lblSil.AutoSize = true;
            this.lblSil.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSil.Location = new System.Drawing.Point(316, 148);
            this.lblSil.Name = "lblSil";
            this.lblSil.Size = new System.Drawing.Size(19, 13);
            this.lblSil.TabIndex = 75;
            this.lblSil.Text = "sil:";
            // 
            // lblTransectSpacing
            // 
            this.lblTransectSpacing.AutoSize = true;
            this.lblTransectSpacing.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTransectSpacing.Location = new System.Drawing.Point(157, 148);
            this.lblTransectSpacing.Name = "lblTransectSpacing";
            this.lblTransectSpacing.Size = new System.Drawing.Size(46, 26);
            this.lblTransectSpacing.TabIndex = 74;
            this.lblTransectSpacing.Text = "transect\r\nspacing:";
            // 
            // txtAreaUnits
            // 
            this.txtAreaUnits.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.batchReportBindingSource, "areaunits", true));
            this.txtAreaUnits.Location = new System.Drawing.Point(51, 172);
            this.txtAreaUnits.Name = "txtAreaUnits";
            this.txtAreaUnits.Size = new System.Drawing.Size(100, 20);
            this.txtAreaUnits.TabIndex = 9;
            // 
            // txtReportArea
            // 
            this.txtReportArea.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.batchReportBindingSource, "reportarea", true));
            this.txtReportArea.Location = new System.Drawing.Point(51, 150);
            this.txtReportArea.Name = "txtReportArea";
            this.txtReportArea.Size = new System.Drawing.Size(100, 20);
            this.txtReportArea.TabIndex = 8;
            // 
            // txtPDF
            // 
            this.txtPDF.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.batchReportBindingSource, "pdf", true));
            this.txtPDF.Location = new System.Drawing.Point(69, 236);
            this.txtPDF.Name = "txtPDF";
            this.txtPDF.Size = new System.Drawing.Size(481, 20);
            this.txtPDF.TabIndex = 17;
            // 
            // txtLinearWidth
            // 
            this.txtLinearWidth.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.batchReportBindingSource, "linearwidth", true));
            this.txtLinearWidth.Location = new System.Drawing.Point(357, 172);
            this.txtLinearWidth.Name = "txtLinearWidth";
            this.txtLinearWidth.Size = new System.Drawing.Size(100, 20);
            this.txtLinearWidth.TabIndex = 13;
            // 
            // txtLinearBlock
            // 
            this.txtLinearBlock.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.batchReportBindingSource, "linearblock", true));
            this.txtLinearBlock.Location = new System.Drawing.Point(208, 172);
            this.txtLinearBlock.Name = "txtLinearBlock";
            this.txtLinearBlock.Size = new System.Drawing.Size(100, 20);
            this.txtLinearBlock.TabIndex = 11;
            // 
            // txtPreparedFor
            // 
            this.txtPreparedFor.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtPreparedFor.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.txtPreparedFor.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.batchReportBindingSource, "prepared_for", true));
            this.txtPreparedFor.Items.AddRange(new object[] {
            "PG&E"});
            this.txtPreparedFor.Location = new System.Drawing.Point(421, 95);
            this.txtPreparedFor.Name = "txtPreparedFor";
            this.txtPreparedFor.Size = new System.Drawing.Size(234, 21);
            this.txtPreparedFor.TabIndex = 6;
            // 
            // txtAuthor
            // 
            this.txtAuthor.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.batchReportBindingSource, "author", true));
            this.txtAuthor.Location = new System.Drawing.Point(67, 73);
            this.txtAuthor.Name = "txtAuthor";
            this.txtAuthor.Size = new System.Drawing.Size(481, 20);
            this.txtAuthor.TabIndex = 4;
            // 
            // txtReportLabel
            // 
            this.txtReportLabel.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.batchReportBindingSource, "label", true));
            this.txtReportLabel.Location = new System.Drawing.Point(67, 13);
            this.txtReportLabel.Name = "txtReportLabel";
            this.txtReportLabel.Size = new System.Drawing.Size(100, 20);
            this.txtReportLabel.TabIndex = 0;
            // 
            // txtAgencyCompany
            // 
            this.txtAgencyCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtAgencyCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.txtAgencyCompany.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.batchReportBindingSource, "agencycompany", true));
            this.txtAgencyCompany.DataSource = this.lookupagencyBindingSource;
            this.txtAgencyCompany.DisplayMember = "agencygrouped";
            this.txtAgencyCompany.Location = new System.Drawing.Point(67, 95);
            this.txtAgencyCompany.Name = "txtAgencyCompany";
            this.txtAgencyCompany.Size = new System.Drawing.Size(241, 21);
            this.txtAgencyCompany.TabIndex = 5;
            // 
            // txtYear
            // 
            this.txtYear.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.batchReportBindingSource, "year", true));
            this.txtYear.Location = new System.Drawing.Point(582, 73);
            this.txtYear.Name = "txtYear";
            this.txtYear.Size = new System.Drawing.Size(73, 20);
            this.txtYear.TabIndex = 2;
            // 
            // txtReportTitle
            // 
            this.txtReportTitle.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.batchReportBindingSource, "reporttitle", true));
            this.txtReportTitle.Location = new System.Drawing.Point(67, 36);
            this.txtReportTitle.Multiline = true;
            this.txtReportTitle.Name = "txtReportTitle";
            this.txtReportTitle.Size = new System.Drawing.Size(588, 35);
            this.txtReportTitle.TabIndex = 3;
            // 
            // txtReportNumber
            // 
            this.txtReportNumber.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.batchReportBindingSource, "reportnumber", true));
            this.txtReportNumber.Location = new System.Drawing.Point(243, 13);
            this.txtReportNumber.Name = "txtReportNumber";
            this.txtReportNumber.Size = new System.Drawing.Size(100, 20);
            this.txtReportNumber.TabIndex = 1;
            // 
            // txtReportID
            // 
            this.txtReportID.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtReportID.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.batchReportBindingSource, "reportid", true));
            this.txtReportID.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtReportID.Location = new System.Drawing.Point(555, 22);
            this.txtReportID.Name = "txtReportID";
            this.txtReportID.ReadOnly = true;
            this.txtReportID.Size = new System.Drawing.Size(100, 11);
            this.txtReportID.TabIndex = 30;
            // 
            // bindingNavigator2
            // 
            this.bindingNavigator2.AddNewItem = this.bindingNavigatorAddNewItem1;
            this.bindingNavigator2.BindingSource = this.batchReportBindingSource;
            this.bindingNavigator2.CountItem = this.bindingNavigatorCountItem1;
            this.bindingNavigator2.DeleteItem = this.bindingNavigatorDeleteItem1;
            this.bindingNavigator2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bindingNavigator2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem1,
            this.bindingNavigatorMovePreviousItem1,
            this.bindingNavigatorSeparator3,
            this.bindingNavigatorPositionItem1,
            this.bindingNavigatorCountItem1,
            this.bindingNavigatorSeparator4,
            this.bindingNavigatorMoveNextItem1,
            this.bindingNavigatorMoveLastItem1,
            this.bindingNavigatorSeparator5,
            this.bindingNavigatorAddNewItem1,
            this.bindingNavigatorDeleteItem1});
            this.bindingNavigator2.Location = new System.Drawing.Point(3, 484);
            this.bindingNavigator2.MoveFirstItem = this.bindingNavigatorMoveFirstItem1;
            this.bindingNavigator2.MoveLastItem = this.bindingNavigatorMoveLastItem1;
            this.bindingNavigator2.MoveNextItem = this.bindingNavigatorMoveNextItem1;
            this.bindingNavigator2.MovePreviousItem = this.bindingNavigatorMovePreviousItem1;
            this.bindingNavigator2.Name = "bindingNavigator2";
            this.bindingNavigator2.PositionItem = this.bindingNavigatorPositionItem1;
            this.bindingNavigator2.Size = new System.Drawing.Size(742, 25);
            this.bindingNavigator2.TabIndex = 2;
            this.bindingNavigator2.Text = "bindingNavigator2";
            // 
            // bindingNavigatorAddNewItem1
            // 
            this.bindingNavigatorAddNewItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem1.Image")));
            this.bindingNavigatorAddNewItem1.Name = "bindingNavigatorAddNewItem1";
            this.bindingNavigatorAddNewItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem1.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem1.Text = "Add new";
            this.bindingNavigatorAddNewItem1.Click += new System.EventHandler(this.bindingNavigatorAddNewItem1_Click);
            // 
            // bindingNavigatorCountItem1
            // 
            this.bindingNavigatorCountItem1.Name = "bindingNavigatorCountItem1";
            this.bindingNavigatorCountItem1.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem1.Text = "of {0}";
            this.bindingNavigatorCountItem1.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem1
            // 
            this.bindingNavigatorDeleteItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem1.Image")));
            this.bindingNavigatorDeleteItem1.Name = "bindingNavigatorDeleteItem1";
            this.bindingNavigatorDeleteItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem1.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem1.Text = "Delete";
            this.bindingNavigatorDeleteItem1.Click += new System.EventHandler(this.bindingNavigatorDeleteItem1_Click);
            // 
            // bindingNavigatorMoveFirstItem1
            // 
            this.bindingNavigatorMoveFirstItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem1.Image")));
            this.bindingNavigatorMoveFirstItem1.Name = "bindingNavigatorMoveFirstItem1";
            this.bindingNavigatorMoveFirstItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem1.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem1.Text = "Move first";
            this.bindingNavigatorMoveFirstItem1.Click += new System.EventHandler(this.bindingNavigatorMoveFirstItem1_Click_1);
            // 
            // bindingNavigatorMovePreviousItem1
            // 
            this.bindingNavigatorMovePreviousItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem1.Image")));
            this.bindingNavigatorMovePreviousItem1.Name = "bindingNavigatorMovePreviousItem1";
            this.bindingNavigatorMovePreviousItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem1.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem1.Text = "Move previous";
            this.bindingNavigatorMovePreviousItem1.Click += new System.EventHandler(this.bindingNavigatorMovePreviousItem1_Click_1);
            // 
            // bindingNavigatorSeparator3
            // 
            this.bindingNavigatorSeparator3.Name = "bindingNavigatorSeparator3";
            this.bindingNavigatorSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem1
            // 
            this.bindingNavigatorPositionItem1.AccessibleName = "Position";
            this.bindingNavigatorPositionItem1.AutoSize = false;
            this.bindingNavigatorPositionItem1.Name = "bindingNavigatorPositionItem1";
            this.bindingNavigatorPositionItem1.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem1.Text = "0";
            this.bindingNavigatorPositionItem1.ToolTipText = "Current position";
            this.bindingNavigatorPositionItem1.Click += new System.EventHandler(this.bindingNavigatorPositionItem1_Click_1);
            // 
            // bindingNavigatorSeparator4
            // 
            this.bindingNavigatorSeparator4.Name = "bindingNavigatorSeparator4";
            this.bindingNavigatorSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem1
            // 
            this.bindingNavigatorMoveNextItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem1.Image")));
            this.bindingNavigatorMoveNextItem1.Name = "bindingNavigatorMoveNextItem1";
            this.bindingNavigatorMoveNextItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem1.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem1.Text = "Move next";
            this.bindingNavigatorMoveNextItem1.Click += new System.EventHandler(this.bindingNavigatorMoveNextItem1_Click_1);
            // 
            // bindingNavigatorMoveLastItem1
            // 
            this.bindingNavigatorMoveLastItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem1.Image")));
            this.bindingNavigatorMoveLastItem1.Name = "bindingNavigatorMoveLastItem1";
            this.bindingNavigatorMoveLastItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem1.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem1.Text = "Move last";
            this.bindingNavigatorMoveLastItem1.Click += new System.EventHandler(this.bindingNavigatorMoveLastItem1_Click_1);
            // 
            // bindingNavigatorSeparator5
            // 
            this.bindingNavigatorSeparator5.Name = "bindingNavigatorSeparator5";
            this.bindingNavigatorSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // tabReportMPE
            // 
            this.tabReportMPE.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabReportMPE.Controls.Add(this.btnDeleteMPERecord);
            this.tabReportMPE.Controls.Add(this.btnAddMPERecord);
            this.tabReportMPE.Controls.Add(this.label30);
            this.tabReportMPE.Controls.Add(this.label29);
            this.tabReportMPE.Controls.Add(this.txtNonintensiveAcres);
            this.tabReportMPE.Controls.Add(this.txtIntensiveAcres);
            this.tabReportMPE.Controls.Add(this.label28);
            this.tabReportMPE.Controls.Add(this.cboForest);
            this.tabReportMPE.Controls.Add(this.label27);
            this.tabReportMPE.Controls.Add(this.label26);
            this.tabReportMPE.Controls.Add(this.txtNotificationID);
            this.tabReportMPE.Controls.Add(this.label25);
            this.tabReportMPE.Controls.Add(this.notificationDate);
            this.tabReportMPE.Controls.Add(label24);
            this.tabReportMPE.Controls.Add(this.txtReportIDMPE);
            this.tabReportMPE.Controls.Add(this.btnAddMPEResource);
            this.tabReportMPE.Controls.Add(this.dgvPersonnel);
            this.tabReportMPE.Controls.Add(this.dgvMPEResource);
            this.tabReportMPE.Controls.Add(this.btnAddMPEPerson);
            this.tabReportMPE.Controls.Add(this.label23);
            this.tabReportMPE.Controls.Add(this.label22);
            this.tabReportMPE.Controls.Add(this.label21);
            this.tabReportMPE.Controls.Add(this.label20);
            this.tabReportMPE.Controls.Add(this.label19);
            this.tabReportMPE.Controls.Add(this.label18);
            this.tabReportMPE.Controls.Add(this.label17);
            this.tabReportMPE.Controls.Add(this.txtNumSites);
            this.tabReportMPE.Controls.Add(this.cboClass);
            this.tabReportMPE.Controls.Add(this.cboActivity);
            this.tabReportMPE.Controls.Add(this.chkActMonitor);
            this.tabReportMPE.Controls.Add(this.surveyStart);
            this.tabReportMPE.Controls.Add(this.surveyEnd);
            this.tabReportMPE.Controls.Add(this.txtContraints);
            this.tabReportMPE.Controls.Add(this.chkActTest);
            this.tabReportMPE.Controls.Add(this.chkActSurvey);
            this.tabReportMPE.Controls.Add(this.chkActDesktop);
            this.tabReportMPE.Location = new System.Drawing.Point(4, 22);
            this.tabReportMPE.Name = "tabReportMPE";
            this.tabReportMPE.Padding = new System.Windows.Forms.Padding(3);
            this.tabReportMPE.Size = new System.Drawing.Size(748, 512);
            this.tabReportMPE.TabIndex = 2;
            this.tabReportMPE.Text = "MPE";
            // 
            // btnDeleteMPERecord
            // 
            this.btnDeleteMPERecord.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteMPERecord.Location = new System.Drawing.Point(99, 489);
            this.btnDeleteMPERecord.Name = "btnDeleteMPERecord";
            this.btnDeleteMPERecord.Size = new System.Drawing.Size(90, 17);
            this.btnDeleteMPERecord.TabIndex = 49;
            this.btnDeleteMPERecord.Text = "Delete MPE Record";
            this.btnDeleteMPERecord.UseVisualStyleBackColor = true;
            this.btnDeleteMPERecord.Click += new System.EventHandler(this.btnDeleteMPERecord_Click);
            // 
            // btnAddMPERecord
            // 
            this.btnAddMPERecord.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddMPERecord.Location = new System.Drawing.Point(7, 489);
            this.btnAddMPERecord.Name = "btnAddMPERecord";
            this.btnAddMPERecord.Size = new System.Drawing.Size(86, 17);
            this.btnAddMPERecord.TabIndex = 48;
            this.btnAddMPERecord.Text = "Add MPE Record";
            this.btnAddMPERecord.UseVisualStyleBackColor = true;
            this.btnAddMPERecord.Click += new System.EventHandler(this.btnAddMPERecord_Click);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(296, 76);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(97, 13);
            this.label30.TabIndex = 47;
            this.label30.Text = "Nonintensive Acres";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(186, 76);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(79, 13);
            this.label29.TabIndex = 46;
            this.label29.Text = "Intensive Acres";
            // 
            // txtNonintensiveAcres
            // 
            this.txtNonintensiveAcres.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.mpereportBindingSource, "nonintensivearea", true));
            this.txtNonintensiveAcres.Location = new System.Drawing.Point(295, 92);
            this.txtNonintensiveAcres.Name = "txtNonintensiveAcres";
            this.txtNonintensiveAcres.Size = new System.Drawing.Size(100, 20);
            this.txtNonintensiveAcres.TabIndex = 45;
            // 
            // mpereportBindingSource
            // 
            this.mpereportBindingSource.DataMember = "mpereport";
            this.mpereportBindingSource.DataSource = this.batchDataset1;
            // 
            // txtIntensiveAcres
            // 
            this.txtIntensiveAcres.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.mpereportBindingSource, "intensivearea", true));
            this.txtIntensiveAcres.Location = new System.Drawing.Point(189, 92);
            this.txtIntensiveAcres.Name = "txtIntensiveAcres";
            this.txtIntensiveAcres.Size = new System.Drawing.Size(100, 20);
            this.txtIntensiveAcres.TabIndex = 44;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(499, 10);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(36, 13);
            this.label28.TabIndex = 43;
            this.label28.Text = "Forest";
            // 
            // cboForest
            // 
            this.cboForest.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.mpereportBindingSource, "forest", true));
            this.cboForest.FormattingEnabled = true;
            this.cboForest.Items.AddRange(new object[] {
            "Unknown",
            "Eldorado",
            "Lassen",
            "Los Padres",
            "Plumas",
            "Sequoia",
            "Shasta-Trinity",
            "Sierra",
            "Six Rivers",
            "Stanislaus",
            "Tahoe"});
            this.cboForest.Location = new System.Drawing.Point(501, 25);
            this.cboForest.Name = "cboForest";
            this.cboForest.Size = new System.Drawing.Size(230, 21);
            this.cboForest.TabIndex = 42;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(3, 306);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(56, 13);
            this.label27.TabIndex = 41;
            this.label27.Text = "Resources";
            this.label27.Click += new System.EventHandler(this.label27_Click);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(301, 8);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(67, 13);
            this.label26.TabIndex = 40;
            this.label26.Text = "Notication ID";
            // 
            // txtNotificationID
            // 
            this.txtNotificationID.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.mpereportBindingSource, "notificationid", true));
            this.txtNotificationID.Location = new System.Drawing.Point(303, 24);
            this.txtNotificationID.Name = "txtNotificationID";
            this.txtNotificationID.Size = new System.Drawing.Size(100, 20);
            this.txtNotificationID.TabIndex = 39;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(406, 9);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(84, 13);
            this.label25.TabIndex = 38;
            this.label25.Text = "Notification Date";
            // 
            // notificationDate
            // 
            this.notificationDate.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.mpereportBindingSource, "notificationdate", true));
            this.notificationDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.notificationDate.Location = new System.Drawing.Point(409, 25);
            this.notificationDate.Name = "notificationDate";
            this.notificationDate.Size = new System.Drawing.Size(86, 20);
            this.notificationDate.TabIndex = 37;
            // 
            // txtReportIDMPE
            // 
            this.txtReportIDMPE.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtReportIDMPE.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.batchReportBindingSource, "reportid", true));
            this.txtReportIDMPE.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtReportIDMPE.Location = new System.Drawing.Point(634, 6);
            this.txtReportIDMPE.Name = "txtReportIDMPE";
            this.txtReportIDMPE.ReadOnly = true;
            this.txtReportIDMPE.Size = new System.Drawing.Size(100, 11);
            this.txtReportIDMPE.TabIndex = 36;
            // 
            // btnAddMPEResource
            // 
            this.btnAddMPEResource.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddMPEResource.Location = new System.Drawing.Point(672, 440);
            this.btnAddMPEResource.Name = "btnAddMPEResource";
            this.btnAddMPEResource.Size = new System.Drawing.Size(61, 17);
            this.btnAddMPEResource.TabIndex = 34;
            this.btnAddMPEResource.Text = "Add";
            this.btnAddMPEResource.UseVisualStyleBackColor = true;
            this.btnAddMPEResource.Click += new System.EventHandler(this.btnAddMPEResource_Click);
            // 
            // dgvPersonnel
            // 
            this.dgvPersonnel.AllowUserToAddRows = false;
            this.dgvPersonnel.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPersonnel.Location = new System.Drawing.Point(6, 192);
            this.dgvPersonnel.Name = "dgvPersonnel";
            this.dgvPersonnel.Size = new System.Drawing.Size(727, 106);
            this.dgvPersonnel.TabIndex = 33;
            // 
            // dgvMPEResource
            // 
            this.dgvMPEResource.AllowUserToAddRows = false;
            this.dgvMPEResource.AllowUserToOrderColumns = true;
            this.dgvMPEResource.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMPEResource.Location = new System.Drawing.Point(6, 322);
            this.dgvMPEResource.Name = "dgvMPEResource";
            this.dgvMPEResource.Size = new System.Drawing.Size(727, 112);
            this.dgvMPEResource.TabIndex = 31;
            this.dgvMPEResource.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvMPEResource_CellContentDoubleClick);
            // 
            // btnAddMPEPerson
            // 
            this.btnAddMPEPerson.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddMPEPerson.Location = new System.Drawing.Point(672, 301);
            this.btnAddMPEPerson.Name = "btnAddMPEPerson";
            this.btnAddMPEPerson.Size = new System.Drawing.Size(61, 17);
            this.btnAddMPEPerson.TabIndex = 30;
            this.btnAddMPEPerson.Text = "Add";
            this.btnAddMPEPerson.UseVisualStyleBackColor = true;
            this.btnAddMPEPerson.Click += new System.EventHandler(this.btnAddMPERPerson_Click);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(631, 76);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(39, 13);
            this.label23.TabIndex = 26;
            this.label23.Text = "# Sites";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(6, 176);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(54, 13);
            this.label22.TabIndex = 25;
            this.label22.Text = "Personnel";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(6, 113);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(59, 13);
            this.label21.TabIndex = 24;
            this.label21.Text = "Constraints";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(94, 76);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(62, 13);
            this.label20.TabIndex = 23;
            this.label20.Text = "Survey End";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(4, 76);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(65, 13);
            this.label19.TabIndex = 22;
            this.label19.Text = "Survey Start";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(134, 8);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(65, 13);
            this.label18.TabIndex = 21;
            this.label18.Text = "PG&E Activity";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(6, 8);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(32, 13);
            this.label17.TabIndex = 20;
            this.label17.Text = "Class";
            // 
            // txtNumSites
            // 
            this.txtNumSites.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.mpereportBindingSource, "surveynumberofsites", true));
            this.txtNumSites.Location = new System.Drawing.Point(633, 92);
            this.txtNumSites.Name = "txtNumSites";
            this.txtNumSites.Size = new System.Drawing.Size(100, 20);
            this.txtNumSites.TabIndex = 19;
            // 
            // cboClass
            // 
            this.cboClass.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.mpereportBindingSource, "classtype", true));
            this.cboClass.FormattingEnabled = true;
            this.cboClass.Items.AddRange(new object[] {
            "Class I",
            "Class II",
            "Class IIa",
            "Class IIb",
            "Class IIc",
            "Class III",
            "Class IV"});
            this.cboClass.Location = new System.Drawing.Point(6, 24);
            this.cboClass.Name = "cboClass";
            this.cboClass.Size = new System.Drawing.Size(113, 21);
            this.cboClass.TabIndex = 12;
            // 
            // cboActivity
            // 
            this.cboActivity.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.mpereportBindingSource, "pgeactivity", true));
            this.cboActivity.DataSource = this.bindingSourceActivity;
            this.cboActivity.DisplayMember = "activity";
            this.cboActivity.FormattingEnabled = true;
            this.cboActivity.Location = new System.Drawing.Point(125, 24);
            this.cboActivity.Name = "cboActivity";
            this.cboActivity.Size = new System.Drawing.Size(172, 21);
            this.cboActivity.TabIndex = 13;
            // 
            // bindingSourceActivity
            // 
            this.bindingSourceActivity.DataMember = "lookupmpeactivity";
            this.bindingSourceActivity.DataSource = this.pgeresourcesDataSet;
            this.bindingSourceActivity.Sort = "activity";
            // 
            // chkActMonitor
            // 
            this.chkActMonitor.AutoSize = true;
            this.chkActMonitor.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.mpereportBindingSource, "archactivitymonitoring", true));
            this.chkActMonitor.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkActMonitor.Location = new System.Drawing.Point(189, 51);
            this.chkActMonitor.Name = "chkActMonitor";
            this.chkActMonitor.Size = new System.Drawing.Size(75, 17);
            this.chkActMonitor.TabIndex = 14;
            this.chkActMonitor.Text = "Monitoring";
            this.chkActMonitor.UseVisualStyleBackColor = true;
            // 
            // surveyStart
            // 
            this.surveyStart.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.mpereportBindingSource, "surveystartdate", true));
            this.surveyStart.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.surveyStart.Location = new System.Drawing.Point(7, 92);
            this.surveyStart.Name = "surveyStart";
            this.surveyStart.Size = new System.Drawing.Size(86, 20);
            this.surveyStart.TabIndex = 15;
            // 
            // surveyEnd
            // 
            this.surveyEnd.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.mpereportBindingSource, "surveyenddate", true));
            this.surveyEnd.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.surveyEnd.Location = new System.Drawing.Point(97, 92);
            this.surveyEnd.Name = "surveyEnd";
            this.surveyEnd.Size = new System.Drawing.Size(82, 20);
            this.surveyEnd.TabIndex = 16;
            // 
            // txtContraints
            // 
            this.txtContraints.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.mpereportBindingSource, "surveyconstraints", true));
            this.txtContraints.Location = new System.Drawing.Point(6, 129);
            this.txtContraints.Multiline = true;
            this.txtContraints.Name = "txtContraints";
            this.txtContraints.Size = new System.Drawing.Size(727, 45);
            this.txtContraints.TabIndex = 17;
            // 
            // chkActTest
            // 
            this.chkActTest.AutoSize = true;
            this.chkActTest.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.mpereportBindingSource, "archactivitytesting", true));
            this.chkActTest.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkActTest.Location = new System.Drawing.Point(130, 51);
            this.chkActTest.Name = "chkActTest";
            this.chkActTest.Size = new System.Drawing.Size(59, 17);
            this.chkActTest.TabIndex = 9;
            this.chkActTest.Text = "Testing";
            this.chkActTest.UseVisualStyleBackColor = true;
            // 
            // chkActSurvey
            // 
            this.chkActSurvey.AutoSize = true;
            this.chkActSurvey.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.mpereportBindingSource, "archactivitysurvey", true));
            this.chkActSurvey.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkActSurvey.Location = new System.Drawing.Point(73, 51);
            this.chkActSurvey.Name = "chkActSurvey";
            this.chkActSurvey.Size = new System.Drawing.Size(59, 17);
            this.chkActSurvey.TabIndex = 11;
            this.chkActSurvey.Text = "Survey";
            this.chkActSurvey.UseVisualStyleBackColor = true;
            // 
            // chkActDesktop
            // 
            this.chkActDesktop.AutoSize = true;
            this.chkActDesktop.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.mpereportBindingSource, "archactivitydesktop", true));
            this.chkActDesktop.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkActDesktop.Location = new System.Drawing.Point(10, 51);
            this.chkActDesktop.Name = "chkActDesktop";
            this.chkActDesktop.Size = new System.Drawing.Size(65, 17);
            this.chkActDesktop.TabIndex = 10;
            this.chkActDesktop.Text = "Desktop";
            this.chkActDesktop.UseVisualStyleBackColor = true;
            // 
            // tabPage16
            // 
            this.tabPage16.Controls.Add(this.tableLayoutPanel13);
            this.tabPage16.Location = new System.Drawing.Point(4, 22);
            this.tabPage16.Name = "tabPage16";
            this.tabPage16.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage16.Size = new System.Drawing.Size(756, 538);
            this.tabPage16.TabIndex = 3;
            this.tabPage16.Text = "GIS";
            this.tabPage16.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.ColumnCount = 1;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 197F));
            this.tableLayoutPanel13.Controls.Add(this.splitContainer1, 0, 1);
            this.tableLayoutPanel13.Controls.Add(this.tableLayoutPanel14, 0, 0);
            this.tableLayoutPanel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel13.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 3;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(750, 532);
            this.tableLayoutPanel13.TabIndex = 1;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(3, 43);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.dgvMatchTable);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dgvGISTable);
            this.splitContainer1.Size = new System.Drawing.Size(744, 466);
            this.splitContainer1.SplitterDistance = 406;
            this.splitContainer1.TabIndex = 4;
            // 
            // dgvMatchTable
            // 
            this.dgvMatchTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMatchTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvMatchTable.Location = new System.Drawing.Point(0, 0);
            this.dgvMatchTable.Name = "dgvMatchTable";
            this.dgvMatchTable.Size = new System.Drawing.Size(406, 466);
            this.dgvMatchTable.TabIndex = 0;
            this.dgvMatchTable.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dgvMatchTable_MouseDown);
            this.dgvMatchTable.MouseMove += new System.Windows.Forms.MouseEventHandler(this.dgvMatchTable_MouseMove);
            // 
            // dgvGISTable
            // 
            this.dgvGISTable.AllowDrop = true;
            this.dgvGISTable.AllowUserToAddRows = false;
            this.dgvGISTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvGISTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvGISTable.Location = new System.Drawing.Point(0, 0);
            this.dgvGISTable.Name = "dgvGISTable";
            this.dgvGISTable.Size = new System.Drawing.Size(334, 466);
            this.dgvGISTable.TabIndex = 0;
            this.dgvGISTable.DragDrop += new System.Windows.Forms.DragEventHandler(this.dgvGISTable_DragDrop);
            this.dgvGISTable.DragEnter += new System.Windows.Forms.DragEventHandler(this.dgvGISTable_DragEnter);
            this.dgvGISTable.DragOver += new System.Windows.Forms.DragEventHandler(this.dgvGISTable_DragOver);
            // 
            // tableLayoutPanel14
            // 
            this.tableLayoutPanel14.ColumnCount = 5;
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel14.Controls.Add(this.cboMatchType, 2, 0);
            this.tableLayoutPanel14.Controls.Add(this.btnLoadGIS, 1, 0);
            this.tableLayoutPanel14.Controls.Add(this.cboGISLayer, 0, 0);
            this.tableLayoutPanel14.Controls.Add(this.label16, 3, 0);
            this.tableLayoutPanel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel14.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel14.Name = "tableLayoutPanel14";
            this.tableLayoutPanel14.RowCount = 1;
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel14.Size = new System.Drawing.Size(744, 34);
            this.tableLayoutPanel14.TabIndex = 5;
            // 
            // cboMatchType
            // 
            this.cboMatchType.Enabled = false;
            this.cboMatchType.FormattingEnabled = true;
            this.cboMatchType.Location = new System.Drawing.Point(283, 3);
            this.cboMatchType.Name = "cboMatchType";
            this.cboMatchType.Size = new System.Drawing.Size(180, 21);
            this.cboMatchType.TabIndex = 3;
            this.cboMatchType.SelectedIndexChanged += new System.EventHandler(this.cboMatchType_SelectedIndexChanged);
            // 
            // btnLoadGIS
            // 
            this.btnLoadGIS.Enabled = false;
            this.btnLoadGIS.Location = new System.Drawing.Point(203, 3);
            this.btnLoadGIS.Name = "btnLoadGIS";
            this.btnLoadGIS.Size = new System.Drawing.Size(46, 19);
            this.btnLoadGIS.TabIndex = 1;
            this.btnLoadGIS.Text = "Load";
            this.btnLoadGIS.UseVisualStyleBackColor = true;
            this.btnLoadGIS.Click += new System.EventHandler(this.btnLoadGIS_Click);
            // 
            // cboGISLayer
            // 
            this.cboGISLayer.FormattingEnabled = true;
            this.cboGISLayer.Location = new System.Drawing.Point(3, 3);
            this.cboGISLayer.Name = "cboGISLayer";
            this.cboGISLayer.Size = new System.Drawing.Size(180, 21);
            this.cboGISLayer.TabIndex = 2;
            this.cboGISLayer.SelectedIndexChanged += new System.EventHandler(this.cboGISLayer_SelectedIndexChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(483, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(74, 26);
            this.label16.TabIndex = 4;
            this.label16.Text = "Select a field to try to match";
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "filter_delete.png");
            this.imageList1.Images.SetKeyName(1, "filter_add.png");
            // 
            // lookuppersonnelBindingSource
            // 
            this.lookuppersonnelBindingSource.DataMember = "lookuppersonnel";
            this.lookuppersonnelBindingSource.DataSource = this.pgeresourcesDataSet;
            // 
            // mperesourceBindingSource
            // 
            this.mperesourceBindingSource.DataMember = "mperesource";
            this.mperesourceBindingSource.DataSource = this.batchDataset1;
            // 
            // mpepersonnelBindingSource
            // 
            this.mpepersonnelBindingSource.DataMember = "mpepersonnel";
            this.mpepersonnelBindingSource.DataSource = this.batchDataset1;
            // 
            // bindingSource2
            // 
            this.bindingSource2.DataMember = "lookupreporttype";
            this.bindingSource2.DataSource = this.pgeresourcesDataSet;
            // 
            // lookupreporttypeTableAdapter
            // 
            this.lookupreporttypeTableAdapter.ClearBeforeFill = true;
            // 
            // mpereportTableAdapter1
            // 
            this.mpereportTableAdapter1.ClearBeforeFill = true;
            // 
            // mpepersonnelTableAdapter
            // 
            this.mpepersonnelTableAdapter.ClearBeforeFill = true;
            // 
            // lookuppersonnelTableAdapter
            // 
            this.lookuppersonnelTableAdapter.ClearBeforeFill = true;
            // 
            // ConfigDS
            // 
            this.ConfigDS.DataSetName = "dsConfigs";
            this.ConfigDS.Tables.AddRange(new System.Data.DataTable[] {
            this.MPEResource,
            this.MPEResourceColumns});
            // 
            // MPEResource
            // 
            this.MPEResource.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn3,
            this.dataColumn4});
            this.MPEResource.TableName = "MPEResource";
            // 
            // dataColumn1
            // 
            this.dataColumn1.ColumnName = "colKey";
            this.dataColumn1.DataType = typeof(int);
            // 
            // dataColumn2
            // 
            this.dataColumn2.Caption = "Label 1";
            this.dataColumn2.ColumnName = "colLabel1";
            // 
            // dataColumn3
            // 
            this.dataColumn3.Caption = "Label 2";
            this.dataColumn3.ColumnName = "colLabel2";
            // 
            // dataColumn4
            // 
            this.dataColumn4.Caption = "Label 3";
            this.dataColumn4.ColumnName = "colLabel3";
            // 
            // MPEResourceColumns
            // 
            this.MPEResourceColumns.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn5,
            this.dataColumn6,
            this.dataColumn7});
            this.MPEResourceColumns.TableName = "MPEResourceColumns";
            // 
            // dataColumn5
            // 
            this.dataColumn5.Caption = "Label";
            this.dataColumn5.ColumnName = "colLabel";
            // 
            // dataColumn6
            // 
            this.dataColumn6.Caption = "Visible";
            this.dataColumn6.ColumnName = "colVisible";
            // 
            // dataColumn7
            // 
            this.dataColumn7.Caption = "Name";
            this.dataColumn7.ColumnName = "colName";
            // 
            // lookupmpeactivityTableAdapter
            // 
            this.lookupmpeactivityTableAdapter.ClearBeforeFill = true;
            // 
            // lookupSILTableAdapter
            // 
            this.lookupSILTableAdapter.ClearBeforeFill = true;
            // 
            // DockPGEData
            // 
            this.Controls.Add(this.tabControl1);
            this.Name = "DockPGEData";
            this.Size = new System.Drawing.Size(764, 564);
            ((System.ComponentModel.ISupportInitialize)(this.batchResourceBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.batchDataset1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupcountycodesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pgeresourcesDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupeligibilityBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.batchReportBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupagencyBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupReportTypebindingSource)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel11.ResumeLayout(false);
            this.tableLayoutPanel12.ResumeLayout(false);
            this.tableLayoutPanel12.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel10.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.resourceTabCntl.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sitesDataGridView)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.tabControl2.ResumeLayout(false);
            this.tabPage8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.tabPage9.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvResourceReport)).EndInit();
            this.tabPage12.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvResourceXref)).EndInit();
            this.tabControl3.ResumeLayout(false);
            this.tabPage10.ResumeLayout(false);
            this.tabPage10.PerformLayout();
            this.tabPage11.ResumeLayout(false);
            this.tabPage11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).EndInit();
            this.bindingNavigator1.ResumeLayout(false);
            this.bindingNavigator1.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.reportList.ResumeLayout(false);
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.reportsDataGridView)).EndInit();
            this.tabReportDetail.ResumeLayout(false);
            this.tabReportDetail.PerformLayout();
            this.tabControl4.ResumeLayout(false);
            this.tabPage13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.tabPage14.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvReportResource)).EndInit();
            this.tabPage15.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvReportXref)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupSILBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator2)).EndInit();
            this.bindingNavigator2.ResumeLayout(false);
            this.bindingNavigator2.PerformLayout();
            this.tabReportMPE.ResumeLayout(false);
            this.tabReportMPE.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mpereportBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPersonnel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMPEResource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceActivity)).EndInit();
            this.tabPage16.ResumeLayout(false);
            this.tableLayoutPanel13.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMatchTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGISTable)).EndInit();
            this.tableLayoutPanel14.ResumeLayout(false);
            this.tableLayoutPanel14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookuppersonnelBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mperesourceBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mpepersonnelBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gisTableBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConfigDS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MPEResource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MPEResourceColumns)).EndInit();
            this.ResumeLayout(false);

		}



        private void reportsDataGridView_UserAddedRow(object sender, DataGridViewRowEventArgs e)
        {

            //reportidDataGridViewTextBoxColumn
            
            MessageBox.Show(Convert.ToString(reportsDataGridView.Rows[0].Cells[0].Value));
            //_bd.AppendReport((int)e.Row.Cells["reportidDataGridViewTextBoxColumn"].Value);

        }



        private void sitesDataGridView_UserAddedRow(object sender, DataGridViewRowEventArgs e)
        {
            _bd.AppendResource((int)e.Row.Cells["resourceid"].Value);
        }

        //private void SitesDataGridView_NewRowNeeded(object sender, DataGridViewRowEventArgs e)
        //{
        //    _bd.AppendResource();
        //}



        private void bindingNavigatorPositionItem1_Click_1(object sender, EventArgs e)
        {
            GetReports();
        }

        private void BindingNavigatorPositionItem_Click(object sender, EventArgs e)
        {
            GetResources();
        }

        private void dataGridView3_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

                if (!string.IsNullOrEmpty(txtOrigPath.Text))
                {
                    System.Diagnostics.Process.Start(txtOrigPath.Text);
                }
                _bd.ReplaceReportRecord(Convert.ToInt32(txtReportID.Text), Convert.ToInt32(dataGridView3.Rows[e.RowIndex].Cells["id"].Value));
                RefreshBatchData(true);
        }

        private void dgvReportXref_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            _bd.DeleteReportXRef((int)dgvReportXref.CurrentRow.Cells["reportid"].Value, (int)dgvReportXref.CurrentRow.Cells["xrefid"].Value, (int)dgvReportXref.CurrentRow.Cells["xrefcode"].Value);
        }

        private void dgvReportResource_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            _bd.DeleteResourceReportEvent((int)dgvResourceReport.CurrentRow.Cells["resourceid"].Value, Convert.ToInt32(this.txtReportID.Text));

        }

        private void dgvResourceXref_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            _bd.DeleteResourceXRef((int)dgvResourceXref.CurrentRow.Cells["resourceid"].Value, (int)dgvResourceXref.CurrentRow.Cells["xrefid"].Value, (int)dgvResourceXref.CurrentRow.Cells["xrefcode"].Value);


        }

        private void dgvResourceReport_UserDeletingRow(object sender, EventArgs e)
        {
            _bd.DeleteResourceReportEvent(Convert.ToInt32(siteidTextBox.Text),(int)dgvResourceReport.CurrentRow.Cells["reportid"].Value);

                
        }

        private void dataGridView2__CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //run match

            //store order in tuple list
           //int position = batchReportBindingSource.Position;
           int batchID = Convert.ToInt32(siteidTextBox.Text);
           int newID = Convert.ToInt32(dataGridView2.Rows[e.RowIndex].Cells["id"].Value);

           if (!string.IsNullOrEmpty(textOrigPDF.Text)){
                System.Diagnostics.Process.Start(textOrigPDF.Text);
            }
           

            _bd.ReplaceResourceRecord(batchID, newID);
            RefreshBatchData(true);
            //var matchedItem = batchReportBindingSource[batchReportBindingSource.Find("reportid", newID)];
            //batchReportBindingSource.RemoveAt(batchReportBindingSource.Find("reportid", newID));
            //batchReportBindingSource.Insert(position, matchedItem);
        }

        private void textPGENumber_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
			{
				e.Handled = true;
			}
		}

		private void textFWJobNumber_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
			{
				e.Handled = true;
			}

		}
		#endregion
		private System.Windows.Forms.BindingSource batchResourceBindingSource;
		private System.Windows.Forms.BindingSource batchReportBindingSource;
		private BatchDataset batchDataset1;
		private BatchDatasetTableAdapters.TableAdapterManager tableAdapterManager;
        private BatchDatasetTableAdapters.TableAdapterManager x;
        
		private BatchDatasetTableAdapters.resourcesTableAdapter resourcesTableAdapter;
		private BatchDatasetTableAdapters.TableAdapterManager tableAdapterManager1;
		private BatchDatasetTableAdapters.reportsTableAdapter reportsTableAdapter;
       // private BatchDatasetTableAdapters.mperesourceTableAdapter mperesourceTableAdapter;
        
		private DataGridViewTextBoxColumn siteidDataGridViewTextBoxColumn;
		private DataGridViewTextBoxColumn resourcelabelDataGridViewTextBoxColumn;
		private DataGridViewTextBoxColumn primarylabelDataGridViewTextBoxColumn;
		private DataGridViewTextBoxColumn trinomiallabelDataGridViewTextBoxColumn;
		private DataGridViewTextBoxColumn fsnumberlabelDataGridViewTextBoxColumn;
		private DataGridViewTextBoxColumn countynameDataGridViewTextBoxColumn;
		private DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
		private DataGridViewTextBoxColumn dataGridViewTextBoxColumn27;
		private Npgsql.NpgsqlCommand npgsqlCommand1;
        private BindingSource lookupagencyBindingSource;
        private pgeresourcesDataSet pgeresourcesDataSet;
        private pgeresourcesDataSetTableAdapters.lookupagencyTableAdapter lookupagencyTableAdapter;
        private BindingSource lookupeligibilityBindingSource;
        private pgeresourcesDataSetTableAdapters.lookupeligibilityTableAdapter lookupeligibilityTableAdapter;
        private OpenFileDialog ofdPDF;
        private pgeresourcesDataSetTableAdapters.lookupcountycodesTableAdapter lookupcountycodesTableAdapter1;
        private BindingSource lookupReportTypebindingSource;
        private BindingSource lookupcountycodesBindingSource;
        private pgeresourcesDataSetTableAdapters.TableAdapterManager tableAdapterManager2;
        
        private TabControl tabControl1;
        private TabPage tabPage1;
        private TableLayoutPanel tableLayoutPanel5;
        private TableLayoutPanel tableLayoutPanel11;
        private Button buttonImportSitePDFs;
        private Button button3;
        private TableLayoutPanel tableLayoutPanel12;
        private RadioButton optReportNumber;
        private RadioButton optReportTitle;
        private Button button2;
        private Button button4;
        private TableLayoutPanel tableLayoutPanel6;
        private CheckBox checkExistingVersion;
        private Button buttonSetVersion;
        private TableLayoutPanel tableLayoutPanel7;
        private ComboBox comboBatchNames;
        private Label label4;
        private Button buttonReload;
        private TableLayoutPanel tableLayoutPanel8;
        private CheckBox checkNewVersion;
        private TableLayoutPanel tableLayoutPanel9;
        private Label label1;
        private TextBox textNewBatchName;
        private TableLayoutPanel tableLayoutPanel10;
        private Label label2;
        private Label label3;
        private TextBox textFWJobNumber;
        private TextBox textPGENumber;
        private TextBox textComment;
        private Label label5;
        private Button buttonCreateVersion;
        private TabPage tabPage2;
        private TabPage tabPage3;
        private TabControl reportList;
        private TabPage tabPage6;
        private Button btnReportAddFilter;
        private Button btnReportClearFilter;
        private Button btnReportFilter;
        private TextBox txtReportSelectValue;
        private ComboBox drpReportSelect;
        private Label lblReportFilter;
        private DataGridView reportsDataGridView;
        private DataGridViewTextBoxColumn reportidDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn labelDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn reportnumberDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn reporttitleDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn yearDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn authorDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn agencycompanyDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn preparedforDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn linearblockDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn linearwidthDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn reportareaDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn areaunitsDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn transectspacingDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn silDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn commentsDataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn temptitleDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn reporttypeDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn pdfDataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn origpathDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn reportdatastatusDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn sitevisitDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn createdbyDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn createddateDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn editedbyDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn editeddateDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn origreportidDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn createduserDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn createddateDataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn lastediteduserDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn lastediteddateDataGridViewTextBoxColumn;
        private TabPage tabReportDetail;
        private Button btnOpenOrigReportPDF;
        private Button btnSelectReportPDF;
        private Button btnOpenReportPDF;
        private TabControl tabControl4;
        private TabPage tabPage13;
        private DataGridView dataGridView3;
        private TabPage tabPage14;
        private TableLayoutPanel tableLayoutPanel4;
        private Button tnReportAddResource;
        private DataGridView dgvReportResource;
        private TabPage tabPage15;
        private TableLayoutPanel tableLayoutPanel3;
        private Button btnReportAddXref;
        private DataGridView dgvReportXref;
        private TextBox txtComment;
        private TextBox txtOrigPath;
        private ComboBox txtSil;
        private TextBox txtTransectSpacing;
        private Label lblAreaUnits;
        private Label lblReportType;
        private Label lblSil;
        private Label lblTransectSpacing;
        private TextBox txtAreaUnits;
        private TextBox txtReportArea;
        private TextBox txtPDF;
        private TextBox txtLinearWidth;
        private TextBox txtLinearBlock;
        private ComboBox txtPreparedFor;
        private TextBox txtAuthor;
        private TextBox txtReportLabel;
        private ComboBox txtAgencyCompany;
        private TextBox txtYear;
        private TextBox txtReportTitle;
        private TextBox txtReportNumber;
        private TextBox txtReportID;
        private BindingNavigator bindingNavigator2;
        private ToolStripButton bindingNavigatorAddNewItem1;
        private ToolStripLabel bindingNavigatorCountItem1;
        private ToolStripButton bindingNavigatorDeleteItem1;
        private ToolStripButton bindingNavigatorMoveFirstItem1;
        private ToolStripButton bindingNavigatorMovePreviousItem1;
        private ToolStripSeparator bindingNavigatorSeparator3;
        private ToolStripTextBox bindingNavigatorPositionItem1;
        private ToolStripSeparator bindingNavigatorSeparator4;
        private ToolStripButton bindingNavigatorMoveNextItem1;
        private ToolStripButton bindingNavigatorMoveLastItem1;
        private ToolStripSeparator bindingNavigatorSeparator5;
        private Button btnComplete;
        private TextBox txtBatchPath;
        private Label label11;
        private Button btnReportsUpdateMatch;
        private CheckBox chkSiteVisit;
        private Button buttonRefresh2;
        private ComboBox cboReportType;
        private BindingSource bindingSource2;
        private pgeresourcesDataSetTableAdapters.lookupreporttypeTableAdapter lookupreporttypeTableAdapter;
        private ComboBox comboReportGroup;
        private ComboBox comboReportStatus;
        private Label label12;
        private TextBox textBox4;
        private CheckBox chkMissingReportGIS;
        private TabControl resourceTabCntl;
        private TabPage tabPage4;
        private Button buttonRefresh1;
        private Button btnAddFilter;
        private Button btnClear;
        private ComboBox drpSelection;
        private DataGridView sitesDataGridView;
        private Button btnFilter;
        private TextBox txtSearch;
        private Label lblFilter;
        private CheckBox chkMissingSiteGIS;
        private TabPage tabPage5;
        private Label label15;
        private ComboBox comboBox3;
        private Button btnSitesRefreshMatch;
        private Button btnOpenOrigPDf;
        private TextBox textOrigPDF;
        private TextBox areaTextBox;
        private TextBox pdfTextBox;
        private TextBox btnAddResourceReportRef;
        private TextBox descriptionTextBox;
        private TextBox othernameTextBox;
        private TextBox siteidTextBox;
        private Button btnSelectSitePDF;
        private Button btnOpenSitePDF;
        private ComboBox ageTextBox;
        private TabControl tabControl2;
        private TabPage tabPage8;
        private DataGridView dataGridView2;
        private TabPage tabPage9;
        private TableLayoutPanel tableLayoutPanel1;
        private DataGridView dgvResourceReport;
        private Button btnResourceAddReport;
        private TabPage tabPage12;
        private TableLayoutPanel tableLayoutPanel2;
        private Button button1;
        private DataGridView dgvResourceXref;
        private ComboBox land_ownerTextBox;
        private ComboBox nrhp_eligibilityTextBox;
        private TabControl tabControl3;
        private TabPage tabPage10;
        private TextBox hrinumberTextBox;
        private TextBox textBox1;
        private TextBox textBox2;
        private TextBox textBox3;
        private TabPage tabPage11;
        private Label label10;
        private ComboBox comboBox1;
        private ComboBox trinhTextBox;
        private TextBox txtfsnump2;
        private TextBox txtfsnump1;
        private TextBox txtfsforest;
        private TextBox txtfsregion;
        private TextBox txttrinno;
        private TextBox txtprimno;
        private Label primcoLabel;
        private Label fsnump2Label;
        private Label fsnump1Label;
        private Label forestLabel;
        private Label fsregionLabel;
        private Label trinnoLabel;
        private Label primnoLabel;
        private ComboBox txtprimco;
        private BindingNavigator bindingNavigator1;
        private ToolStripButton bindingNavigatorAddNewItem;
        private ToolStripLabel bindingNavigatorCountItem;
        private ToolStripButton bindingNavigatorDeleteItem;
        private ToolStripButton bindingNavigatorMoveFirstItem;
        private ToolStripButton bindingNavigatorMovePreviousItem;
        private ToolStripSeparator bindingNavigatorSeparator;
        private ToolStripTextBox bindingNavigatorPositionItem;
        private ToolStripSeparator bindingNavigatorSeparator1;
        private ToolStripButton bindingNavigatorMoveNextItem;
        private ToolStripButton bindingNavigatorMoveLastItem;
        private ToolStripSeparator bindingNavigatorSeparator2;
        private TabPage tabPage16;
        private TableLayoutPanel tableLayoutPanel13;
        private DataGridView dgvGISTable;
        private ComboBox cboMatchType;
        private ComboBox cboGISLayer;
        private Button btnLoadGIS;
        private BindingSource gisTableBindingSource;
        private CheckBox chkReportMatchDisabled;
        private CheckBox chkSitesMatchingDisabled;
        private SplitContainer splitContainer1;
        private DataGridView dgvMatchTable;
        private TableLayoutPanel tableLayoutPanel14;
        private Label label16;
        private TabPage tabPage17;
        private TabPage tabReportMPE;
        private BatchDatasetTableAdapters.mpereportTableAdapter mpereportTableAdapter1;
        
        private Label label17;
        private TextBox txtNumSites;
        private BindingSource mpereportBindingSource;
        private CheckBox chkActDesktop;
        private CheckBox chkActSurvey;
        private ComboBox cboClass;
        private CheckBox chkActTest;
        private ComboBox cboActivity;
        private CheckBox chkActMonitor;
        private DateTimePicker surveyStart;
        private DateTimePicker surveyEnd;
        private TextBox txtContraints;
        private Label label18;
        private BindingSource mpepersonnelBindingSource;
        private Label label23;
        private Label label22;
        private Label label21;
        private Label label20;
        private Label label19;
        private BatchDatasetTableAdapters.mpepersonnelTableAdapter mpepersonnelTableAdapter;
        private BindingSource lookuppersonnelBindingSource;
        private pgeresourcesDataSetTableAdapters.lookuppersonnelTableAdapter lookuppersonnelTableAdapter;
        private ImageList imageList1;
        private Button btnAddMPEPerson;
        private DataGridView dgvMPEResource;
        private BindingSource mperesourceBindingSource;
        private System.Data.DataSet ConfigDS;
        private System.Data.DataTable MPEResource;
        private System.Data.DataColumn dataColumn1;
        private System.Data.DataColumn dataColumn2;
        private System.Data.DataColumn dataColumn3;
        private System.Data.DataColumn dataColumn4;
        private System.Data.DataTable MPEResourceColumns;
        private System.Data.DataColumn dataColumn5;
        private System.Data.DataColumn dataColumn6;
        private System.Data.DataColumn dataColumn7;
        private DataGridView dgvPersonnel;
        private Button btnAddMPEResource;
        private TextBox txtReportIDMPE;
        private Label label25;
        private DateTimePicker notificationDate;
        private Label label26;
        private TextBox txtNotificationID;
        private BindingSource bindingSourceActivity;
        private pgeresourcesDataSetTableAdapters.lookupmpeactivityTableAdapter lookupmpeactivityTableAdapter;
        private BindingSource lookupSILBindingSource;
        private pgeresourcesDataSetTableAdapters.lookupSILTableAdapter lookupSILTableAdapter;
        private DataGridViewTextBoxColumn resourceidDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn primcoDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn primnoDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn hrinumberDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn trinnoDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn trinno2DataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn trinhDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn fsregionDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn fsforestDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn fsnump1DataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn fsnump2DataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn othernameDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn pdfDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn ageDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn descriptionDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn nrhpeligibilityDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn resareaDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn landownerDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn commentsDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn preeligDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn histeligDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn sitedatastatusDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn origfilepathDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn origsiteidDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn createduserDataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn createddateDataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn lastediteduserDataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn lastediteddateDataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn fslabelDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn reportgroupDataGridViewTextBoxColumn;
        private Label label27;
        private Label label28;
        private ComboBox cboForest;
        private Label label30;
        private Label label29;
        private TextBox txtNonintensiveAcres;
        private TextBox txtIntensiveAcres;
        private Button btnDeleteMPERecord;
        private Button btnAddMPERecord;
    }
}
