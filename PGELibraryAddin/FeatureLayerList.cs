﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Geodatabase;
using System.ComponentModel;

namespace PGELibraryAddin
{
    class FeatureLayerList: List<IFeatureLayer>
    {
        
            private BindingList<string> _nameList = new BindingList<string>();

            private int _lastIndex;
            public new void Add(IFeatureLayer item)
            {
                base.Add(item);
                

                AddNamedLayer(item.Name, 0);
            }
            private void AddNamedLayer(string sFLName, int i)
            {
                if (_nameList.Contains(sFLName))
                {
                if (i > 0)
                    // if we added the (#) then we need to remove it 
                    //sFLName = Strings.Replace(sFLName, " (" + i + ")", "");
                    sFLName = sFLName.Replace(" (" + i + ")", "");

                    i = i + 1;
                    sFLName = sFLName + " (" + i + ")";
                    AddNamedLayer(sFLName, i);
                }
                else
                {
                    _nameList.Add(sFLName);
                    _lastIndex = _nameList.IndexOf(sFLName);
                }
            }
            public new void clear()
            {
                base.Clear();
                _nameList.Clear();
            }
            public new bool Remove(ESRI.ArcGIS.Carto.IFeatureLayer item)
            {
                int i;
                i = base.IndexOf(item);
                base.Remove(item);
                _nameList.RemoveAt(i);
            return true;
            }
            public int IndexOfName(string name)
            {
                int indx;
                indx = _nameList.IndexOf(name);
            return indx;
            }
            public bool ContainsName(string item)
            {
                return _nameList.Contains(item);
            }
            public virtual new void insert(int index, ESRI.ArcGIS.Carto.IFeatureLayer item)
            {
                base.Insert(index, item);
                _nameList.Insert(index, item.Name);
            }
            public int IndexOfLast
            {
                get
                {
                    return _lastIndex;
                }
            }
            public IList<string> NameList
            {
                get
                {
                    return _nameList;
                }
            }
            public IFeatureLayer FeatureLayerByName(string name)
            {
                return base[_nameList.IndexOf(name)];
        
            }
            public IList<string> FieldsByFeaturelayerName(string name, esriFieldType? fldType = null/* TODO Change to default(_) if this is not a reference type */)
            {
                // get fields by feature layer name 
                // optional field filter 
                // default is to return all
                IList<string> fldNames = new List<string>();
                // new additions to get unqualified field names
                // Dim worksp As IWorkspace
                IFeatureLayer fl;
                // Dim sqlSyntx As ISQLSyntax
                fl = this.FeatureLayerByName(name);
                if (fl == null)
                    fldNames.Add("(Error Reading Fields)");
                ILayerFields flds;
                ESRI.ArcGIS.Geodatabase.IField fld;
                flds = (ILayerFields) fl;
                int i;
                string sFldName;
                for (i = 0; i <= flds.FieldCount - 1; i++)
                {
                    fld = flds.Field[i];

                    if (fldType == null/* TODO Change to default(_) if this is not a reference type */ )
                        fldNames.Add(fld.AliasName);
                    else if (fld.Type == fldType)
                        fldNames.Add(fld.AliasName);
                }
                return fldNames;
            }
            public new ESRI.ArcGIS.Carto.IFeatureLayer this[int index]
            {
                get
                {
                    return base[index];
                }
                set
                {
                    base[index] = value;
                    // this may be problematic
                    // check to see if this then calls add item
                    _nameList[index] = value.Name;
                }
            }
            public new void RemoveAt(int index)
            {
                _nameList.RemoveAt(index);
                base.RemoveAt(index);
            }
        
    }
}
