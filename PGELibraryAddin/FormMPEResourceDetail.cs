﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PGELibraryAddin
{
    public partial class FormMPEResourceDetail : Form
    {
        int _MPEReportID;
        int _MPEResourceID;
        BatchData _bd;
        string _resourceLabel;


        public FormMPEResourceDetail()
        {
            InitializeComponent();
        }
        public string ResourceLabel
        {
            set { _resourceLabel = value; }
        }
        public int reportid
        {
            set { _MPEReportID = value; }
        }
        public int resourceid
        {
            set { _MPEResourceID = value; }
        }
        public BatchData batchData
        {
            set { _bd = value; }
        }
        private void FormMPEResourceDetail_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'batchDataset.mperesource' table. You can move, or remove it, as needed.
            this.mperesourceTableAdapter.Fill(this.batchDataset.mpeResource);
            this.mperesourceBindingSource.Filter="reportid =" + _MPEReportID + " and resourceid =" + _MPEResourceID;
            // TODO: This line of code loads data into the 'pgeresourcesDataSet.lookukpmperpm' table. You can move, or remove it, as needed.
            this.lookukpmperpmTableAdapter.Fill(this.pgeresourcesDataSet.lookukpmperpm);

        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            //_bd.AppendMPEResource(_MPEReportID, _MPEResourceID);
            this.Close();
        }

        private void chkInadvertent_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
