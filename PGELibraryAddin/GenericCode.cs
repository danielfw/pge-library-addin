﻿using System;
using System.Collections.Generic;
using ESRI.ArcGIS.ArcMapUI;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Geometry;
using System.Linq;

namespace PGELibraryAddin
{
	class GenericCode
    {
        private static NLog.Logger logger = FWLogManager.Instance.GetCurrentClassLogger();

        public FeatureLayerList GetLayers(IList<ESRI.ArcGIS.Geometry.esriGeometryType> GeomTypes = null, IWorkspace2 Worksp = null, string FeatDatasetNameFilter = null)
        {
            // returns a list of layers in the map
            // optionally define a geomoetry type
            // optionally define a workspace where the layers must reside
            ESRI.ArcGIS.esriSystem.IUID uid;
            uid = new ESRI.ArcGIS.esriSystem.UID();
            uid.Value = "{E156D7E5-22AF-11D3-9F99-00C04F6BC78E}"; // IGeoFeatureLayer
            FeatureLayerList myLayers = new FeatureLayerList();

            try
            {
                if (GeomTypes == null)
                {
                    GeomTypes = new List<esriGeometryType>();
                    GeomTypes.Add(esriGeometryType.esriGeometryAny);
                }
                IEnumLayer enumLayer = ArcMap.Document.FocusMap.Layers[(ESRI.ArcGIS.esriSystem.UID)uid, true];
                IFeatureLayer pFlay;
                enumLayer.Reset();
                ESRI.ArcGIS.Carto.ILayer pLay = enumLayer.Next();
                while (!(pLay == null))
                {
                    pFlay = (IFeatureLayer)pLay;
                    if (pLay.Valid)
                    {
                        esriGeometryType shapetype = pFlay.FeatureClass.ShapeType;
                        if (GeomTypes.Contains(shapetype) | GeomTypes.Contains(esriGeometryType.esriGeometryAny))
                        {
                            if (FeatDatasetNameFilter is null && pFlay.FeatureClass.FeatureDataset is null)
                            {

                            } else if (FeatDatasetNameFilter is null || pFlay.FeatureClass.FeatureDataset.BrowseName == FeatDatasetNameFilter)
                            {
                                if (Worksp is null) {
                                    myLayers.Add(pFlay);
                                } else
                                {
                                    IDataLayer2 dl2;
                                    dl2 = (IDataLayer2)pLay;
                                    if (dl2.InWorkspace((IWorkspace)Worksp)) {
                                        myLayers.Add(pFlay);
                                    }
                                }
                            }
                        }
                    }

                    pLay = enumLayer.Next();
                }
                return myLayers;
            }
            catch (Exception e)
            {
                logger.Error(e, System.Reflection.MethodBase.GetCurrentMethod().Name);

                //logger.ErrorException(System.Reflection.MethodBase.GetCurrentMethod().Name, ex);
                return null/* TODO Change to default(_) if this is not a reference type */;
            }
        }

        public IWorkspace2 GetSettingsDefinedWorkspace(string sWorkspaceType, string sWorkspaceFile, string versionname = "sde.DEFAULT")
        {
            // this will return the default resource workspace
            // it could be used generally if you pass the appropriate information
            // if you pass a shapefile workspace type the workspace file should be the directory
            if (versionname =="")
            {
                versionname = "sde.DEFAULT";
            }
            IWorkspaceFactory2 WkspFact;
            Type wksptype = Type.GetTypeFromProgID(sWorkspaceType);
            WkspFact = (IWorkspaceFactory2)Activator.CreateInstance(wksptype);
            IWorkspace2 Wksp;
            if (sWorkspaceType == "esriDataSourcesOleDB.OLEDBWorkspaceFactory")
            {
                // ArcObjects is failing when passing a workspacefile that points to an SDE file. this is a  work around 
                IPropertySet pConProps = new PropertySetClass();

                // ----------------------------------------------
                // this would work for a SQLWorkspaceFactory
                // pConProps.SetProperty("DBCLIENT", "sqlserver")
                // pConProps.SetProperty("SERVERINSTANCE", "fwsqlsvr\SQLExpress")
                // pConProps.SetProperty("DATABASE", "MasterResourceList")
                // pConProps.SetProperty("AUTHENTICATION_MODE", "OSA")
                // ----------------------------------------------
                pConProps.SetProperty("CONNECTSTRING", sWorkspaceFile);
                Wksp = (IWorkspace2)WkspFact.Open(pConProps, ArcMap.Application.hWnd);
                // Wksp = WkspFact.OpenFromString(sWorkspaceFile, My.ThisApplication.hwnd)
                return Wksp;
            }
            else if (versionname != "sde.DEFAULT")
            {
                IPropertySet pConProps = new PropertySetClass();


                pConProps.SetProperty("DBCLIENT", "posgtresql");
                pConProps.SetProperty("SERVERINSTANCE", "192.168.0.213");
                pConProps.SetProperty("DATABASE", "pgelibrary");
                pConProps.SetProperty("AUTHENTICATION_MODE", "OSA");
                pConProps.SetProperty("VERSION", versionname);
                Wksp = (IWorkspace2)WkspFact.Open(pConProps, ArcMap.Application.hWnd);
                return Wksp;
            }
            else
            {
                Wksp = (IWorkspace2)WkspFact.OpenFromFile(sWorkspaceFile, ArcMap.Application.hWnd);
                return Wksp;
            }
        }
        public Boolean IsVersionWorkspace(IStandaloneTable standaloneTable,string versionname = "")
        {
            IWorkspace workspace;
            workspace = ((IDataset)standaloneTable).Workspace;
            IPropertySet2 propertySet;
            propertySet = (IPropertySet2)workspace.ConnectionProperties;
            object obj = propertySet.GetProperty("VERSION");
            
            if (obj.ToString()==versionname)
            {
                return true;
            } else
            {
                IVersionedWorkspace4 Vwksp = (IVersionedWorkspace4)workspace;
                IVersion VFrom = (IVersion3)Vwksp;
                IVersion VTo = Vwksp.FindVersion(versionname);
                IChangeDatabaseVersion VChange = new ChangeDatabaseVersion();
                VChange.Execute(VFrom, VTo, (IBasicMap)ArcMap.Document.FocusMap);
                return false;
            }
        }
        public IStandaloneTable GetStandAloneQueryTable(string sTableName, string sqlDefinition, string queryOIDFields, IWorkspace2 batchWorkspace)
        {
            IStandaloneTable pStTab;
            IStandaloneTableCollection pStTabColl;
            IMxDocument pMxDoc;
            pMxDoc = ArcMap.Application.Document as IMxDocument;
            pStTabColl = pMxDoc.FocusMap as IStandaloneTableCollection;
            IStandaloneTable pStbl;
            short i;

            IWorkspace2 pWorkspace = null/* TODO Change to default(_) if this is not a reference type */;
            
            ISqlWorkspace2 pSQLWkSp;
            // loop thru standalone tables to find the table (go backwards in case count changes)
            //for (i = pStTabColl.StandaloneTableCount() - 1; i >= 0; i += -1)
            int count;
            if (pStTabColl != null)
            {
                count = pStTabColl.StandaloneTableCount;
                for (i = 0; i < count; ++i)
                {
                    pStbl = pStTabColl.StandaloneTable[i];
                    if (pStbl.Name == sTableName)
                    {
                        if (pStbl.Valid)
                        {
                            return pStbl;
                        }
                        else
                            pStTabColl.RemoveStandaloneTable(pStbl);// found, but invalid -- remove
                    }
                }
            }


            // never found the table so we need to add it
            pStTab = new StandaloneTable();
            //pWorkspace = GetSettingsDefinedWorkspace("esriDataSourcesGDB.SdeWorkspaceFactory", PGELibraryAddin.Properties.Settings.Default.UserSDEConnectionFile, versionname);
            pSQLWkSp = (ISqlWorkspace2)batchWorkspace;
            IQueryDescription queryDescription = pSQLWkSp.GetQueryDescription2(sqlDefinition, false);
            queryDescription.OIDFields = queryOIDFields;

            string outQueryName;
            pSQLWkSp.CheckDatasetName(sTableName, queryDescription, out outQueryName);
            ITable table = pSQLWkSp.OpenQueryClass(outQueryName, queryDescription);
            
            pStTab.Table = table;
            pStTab.Name = sTableName;
            
            pStTabColl.AddStandaloneTable(pStTab);
            
            // Refresh the TOC

            pMxDoc.UpdateContents();
            return pStTab;
        }
        public IStandaloneTable GetStandAloneTable(string sTable, string versionname = "")
        //
        //public IStandaloneTable GetStandAloneTable(string sTable)
        {

            IStandaloneTable pStTab;
            IStandaloneTableCollection pStTabColl;
            IMxDocument pMxDoc;
            pMxDoc = ArcMap.Application.Document as IMxDocument;

            pStTabColl = pMxDoc.FocusMap as IStandaloneTableCollection;

            IStandaloneTable pStbl;
            short i;

            // loop thru standalone tables to find the table (go backwards in case count changes)
            //for (i = pStTabColl.StandaloneTableCount() - 1; i >= 0; i += -1)
            int count;
            if (pStTabColl != null)
            {
                count = pStTabColl.StandaloneTableCount;
                for (i = 0; i < count; ++i)
                {
                    pStbl = pStTabColl.StandaloneTable[i];
                    if (pStbl.Name == sTable)
                    {
                        if (pStbl.Valid)
                        {
                            if (versionname == "")
                            {
                                return pStbl;
                            } else if (IsVersionWorkspace(pStbl, versionname))
                            {
                                return pStbl;
                            }
                        }
                        else
                            pStTabColl.RemoveStandaloneTable(pStbl);// found, but invalid -- remove
                    }
                }
            }
            

            // never found the table so we need to add it
            pStTab = new StandaloneTable();

            pStTab.Table = AddTable(sTable,versionname);
            pStTabColl.AddStandaloneTable(pStTab);

            // Refresh the TOC

            pMxDoc.UpdateContents();
            return pStTab;
        }

        private ITable AddTable(string sTable, string versionName)
        {

            // --look up feature class names
            IWorkspace2 pWorkspace = null/* TODO Change to default(_) if this is not a reference type */;
            IFeatureWorkspace pFeatws;
            ISqlWorkspace pSQLWkSp;
            ISQLSyntax pSQLSyntax;

            ITable pTable;
            try
            {
                //if (sTable == My.Settings.MasterDBResourceList | sTable == My.Settings.MasterDBProjectList | sTable == My.Settings.MasterDBProjectElementList | sTable == My.Settings.MasterDBProjectEventList | sTable == My.Settings.MasterDBPriorDocumentList)
                //{
                pWorkspace = GetSettingsDefinedWorkspace("esriDataSourcesGDB.SdeWorkspaceFactory", PGELibraryAddin.Properties.Settings.Default.UserSDEConnectionFile,versionName);
                if (pWorkspace.NameExists[esriDatasetType.esriDTTable, sTable])
                {
                    pFeatws = (IFeatureWorkspace)pWorkspace;
                    pTable = pFeatws.OpenTable(sTable);
                    // add the table
                    return pTable;
                }
                else
                    throw new Exception(sTable + " does not exist in workspace.");
                //}
                //else
                //    throw new Exception(sTable + " needs to have a defined location");
            }
            catch (Exception ex)
            {
                logger.Log(NLog.LogLevel.Error, string.Format("Cannot locate source for {0}", sTable));
                return null;
            }
        }

        public void RemoveJoin(IDisplayTable displayTable)
        {
            IDisplayRelationshipClass displayRelationshipClass;
            //IRelQueryTable relQueryTable;
            ITable table = displayTable.DisplayTable;
            if (table.GetType() is IRelQueryTable) {
                //relQueryTable = table;
                displayRelationshipClass = (IDisplayRelationshipClass)displayTable;
                displayRelationshipClass.DisplayRelationshipClass(null, esriJoinType.esriLeftInnerJoin);
            }
        }

        public IRelationshipClass JoinTable(IDisplayTable displayTableFrom,IDisplayTable displayTableTo, string joinFieldFrom, string joinFieldTo, esriRelCardinality cardinality)
        {
            // jointype: 0 outer,  1 inner
            try
            {
               
                //provide a display table similar to 
               // pDispTable = (IDisplayTable)fl;
               //or
               //pdisptable = (idisplaytable)standalonetable

                ITable toTable;
                ITable fromTable;

                
                toTable = displayTableTo.DisplayTable;
                fromTable = displayTableFrom.DisplayTable;

                // Create virtual relate
                IMemoryRelationshipClassFactory pMemRelFact;
                IRelationshipClass pRelClass;
                pMemRelFact = new MemoryRelationshipClassFactory();
                pRelClass = pMemRelFact.Open("TabletoLayer",(IObjectClass)fromTable, joinFieldFrom, (IObjectClass)toTable, joinFieldTo, "forward", "backward", cardinality);

                // use Relate to perform a join
               // IDisplayRelationshipClass pDispRC;
                //pDispRC = fl;

                //pDispRC.DisplayRelationshipClass(pRelClass, jt);
                return pRelClass;
            }
            catch (Exception ex)
            {
                //logger.Log(NLog.LogLevel.Error, string.Format("Exception in {0} reads {1}", System.Reflection.Assembly.GetExecutingAssembly().GetModules()(0).Name, ex.Message));
                
                return null/* TODO Change to default(_) if this is not a reference type */;
            }
        }

            public System.Collections.ArrayList GetUniqueValuesForLayer(IFeatureLayer fl, string fld, string WhereFilter = "")
            {
            
                IFeatureSelection featSel;
                ISelectionSet selSet;
             ESRI.ArcGIS.Geodatabase.esriFieldType fldType;
                try
                {

                //    var returnItem = MakeUniqueListForField(fl, fld);
                // Type t = fld.GetType();
                //ESRI.ArcGIS.Geodatabase.IField flField;
                //flField = fl.FeatureClass.Fields.Field[fl.FeatureClass.Fields.FindField(fld)];
                //fldType = fl.FeatureClass.Fields.Field[fl.FeatureClass.Fields.FindField(fld)].Type;
                System.Collections.ArrayList arrayList = new System.Collections.ArrayList();

                //var list = (System.Collections.IList)Activator.CreateInstance(fldType);
                //list < fldType > = new List;
                //List list<fldType> = new List;
                //List<flField.Type> templist = new List<flField.Type>() { };
                featSel = (IFeatureSelection)fl;
                    //selSet = featSel.SelectionSet;
                    QueryFilter pQF = new QueryFilter();
                    pQF.WhereClause = WhereFilter;
                    IFeatureCursor pFCur;
                    ESRI.ArcGIS.Geodatabase.IDataStatistics pDataStatistics = new  ESRI.ArcGIS.Geodatabase.DataStatistics();
                // verify that a qQF without a where works the same as a nothing
                //selSet.Search(pQF, false,out pCur);
                pFCur = fl.Search(pQF, false);
                //selSet.Search(pQF, false, out pCur);
                pDataStatistics.Cursor = pFCur as ESRI.ArcGIS.Geodatabase.FeatureCursor;
                    pDataStatistics.Field = fld;
                    System.Collections.IEnumerator pEnumerator = pDataStatistics.UniqueValues;

                while (pEnumerator.MoveNext())
                {
                    //list.Add(pEnumerator.Current);
                    arrayList.Add(pEnumerator.Current);
                }

                //return list;
                return arrayList;
                }
                catch (Exception ex)
                {
                    logger.Log(NLog.LogLevel.Error, string.Format("Exception in {0} reads {1}", "GetUniqueValuesForLayer", ex.Message));
                    return null;
                }
            }
        //    private object MakeUniqueListForField(IFeatureLayer fl, string fld)
        //    {
        //        ESRI.ArcGIS.Geodatabase.IField MyField;


        //        try
        //        {
        //            MyField = fl.FeatureClass.Fields.Field[fl.FeatureClass.Fields.FindField(fld)];
        //            MyField = fl.FeatureClass.Fields.Field[fl.FeatureClass.Fields.FindField(fld)];

        //                switch (MyField.Type)
        //            {
        //                case  esriFieldType.esriFieldTypeDate:
        //                    {
        //                    return new List<DateTime>();
        //                        break;
        //                    }

        //                case  esriFieldType.esriFieldTypeDouble:
        //                    {
        //                    return new List<double>();
        //                        break;
        //                    }

        //                case  esriFieldType.esriFieldTypeInteger:
        //                    {
        //                    return new List<int>();
        //                        break;
        //                    }

        //                case  esriFieldType.esriFieldTypeSmallInteger:
        //                    {
        //                    return new List<int>();
        //                        break;
        //                    }

        //                case esriFieldType.esriFieldTypeString:
        //                    {
        //                    return new List<string>();
        //                        break;
        //                    }

        //                default:
        //                    {
        //                        throw new Exception("Unsupported Field Type");
        //                        break;
        //                    }
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            logger.Log(NLog.LogLevel.Error, string.Format("Cannot make unique list for field {0} in {1}", fld, fl.Name));
        //            return null/* TODO Change to default(_) if this is not a reference type */;
        //        }
           
        //}
    }
}



