﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;
//using ArcDataBinding;
using FWARGTableWrapper;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Catalog;
using ESRI.ArcGIS.CatalogUI;
using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Editor;
using ESRI.ArcGIS.Geodatabase;
using Npgsql;
using System.ComponentModel;

namespace PGELibraryAddin
{

	public class BatchData
	{
        private static readonly NLog.Logger logger = FWLogManager.Instance.GetCurrentClassLogger();
        public enum ImportType { ResourcePDF, ResourceGIS, ReportPDF, ReportGIS };
        public enum FilterType { New, Add, Clear, GISAdd, GISRemove}
		private IWorkspace2 _BatchWorkspace;

		private FWTableWrapper _VersionedResourceRecords; //Versioned batch resources
		private FWTableWrapper _VersionedReportRecords; //Versioned batch reports
		private FWTableWrapper _VersionedReportLog; //Versioned batch reports
		private FWTableWrapper _VersionedResourceLog; //Versioned batch resources
		private FWTableWrapper _BatchLog; //log of batches
		private FWTableWrapper _VersionedSiteReportEvents;
        
		private FWTableWrapper _VersionedXrefReport;
		private FWTableWrapper _VersionedXrefResource;
        //mpe
        private FWTableWrapper _VersionedMPEReports;
        private FWTableWrapper _VersionedViewMPEreportpersonnel;
        private FWTableWrapper _VersionedViewMPEResource;
        private FWTableWrapper _VersionedMPEreportpersonnel;
        private FWTableWrapper _VersionedMPEResource;


        private string _VersionName;
        private string _ResourceFilter;
        private string _ReportFilter;
        private string _ResourceGISFilter;
        private string _ReportGISFilter;
        private string _BatchPath="";

        private string _VersionedResourceGISWhere;
        private string _VersionedReportGISWhere;


        private IWorkspace2 _TempWorkspace;
		private string _BatchGUID;
		//private IMultiuserWorkspaceEdit _MUEditor;
        private IEditor3 _Editor;
        private BatchDataset _bds;

        GenericCode _workerBee;

		//need to wrap a versioned table to make edits
		//need a view of wrapped table for formating of labels
		//public BatchData(String dbclient, String dbConnProp, String user, String password, String database, String version, String authentication)
		public BatchData()
		{
            try
            {
                _bds = new BatchDataset();

                //BatchDatasetTableAdapters.lookupcountycodesTableAdapter lookupcountycodesTableAdapter = new BatchDatasetTableAdapters.lookupcountycodesTableAdapter();
                BatchDatasetTableAdapters.lookupcountycodesTableAdapter lookupcountycodesTableAdapter = new BatchDatasetTableAdapters.lookupcountycodesTableAdapter();
                BatchDatasetTableAdapters.lookupagencyTableAdapter lookupagencyTableAdapter = new BatchDatasetTableAdapters.lookupagencyTableAdapter();
                BatchDatasetTableAdapters.lookupreporttypeTableAdapter lookupreporttypeTableAdapter = new BatchDatasetTableAdapters.lookupreporttypeTableAdapter();
                BatchDatasetTableAdapters.lookupmperpmTableAdapter lookupmperpmTableAdapter = new BatchDatasetTableAdapters.lookupmperpmTableAdapter();
                lookupreporttypeTableAdapter.Fill(_bds.lookupreporttype);
                lookupcountycodesTableAdapter.Fill(_bds.lookupcountycodes);
                lookupagencyTableAdapter.Fill(_bds.lookupagency);
                lookupmperpmTableAdapter.Fill(_bds.lookupmperpm);


                _workerBee = new GenericCode();
                _BatchWorkspace = _workerBee.GetSettingsDefinedWorkspace("esriDataSourcesGDB.SdeWorkspaceFactory", PGELibraryAddin.Properties.Settings.Default.UserSDEConnectionFile, GetLoadedMapVersion());
               
            }
            catch (Exception e)
            {

                _BatchWorkspace = null;
                //throw;
            }
           
		}

 

        public string GetLoadedMapVersion()
        {
            FeatureLayerList flList = new FeatureLayerList();
            flList = _workerBee.GetLayers(null, null, null);
            IFeatureLayer fl = flList.FeatureLayerByName(PGELibraryAddin.Properties.Settings.Default.BatchResourcePoly);
            IWorkspace workspace;
            workspace = ((IDataset)fl).Workspace;
            IPropertySet2 propertySet;
            propertySet = (IPropertySet2)workspace.ConnectionProperties;
            object obj = propertySet.GetProperty("VERSION");
            SetBatchGUID(obj.ToString());
            return obj.ToString();

        }

		//workspace for version
		public IWorkspace2 BatchWorkspace
		{
			get => _BatchWorkspace;
		}
        public bool Enabled
        {
            get
            {
                bool b;
                b = _BatchWorkspace == null ? false : true;
                return b;
            }   
        }
		//load all approprite tables from the workspace to the class
		public IList<string> GetVersions()
		{
            if (this.Enabled)
            {
                IVersionedWorkspace4 Vwksp = (IVersionedWorkspace4)_BatchWorkspace;
			    IEnumVersionInfo VEinfo = Vwksp.Versions;
			    IVersionInfo Vinfo = VEinfo.Next();
			    IList<string> vnames = new List<string>();
			    while (!(Vinfo == null))
			    {
				    vnames.Add(Vinfo.VersionName);
				    Vinfo = VEinfo.Next();
			    }
			    return vnames;
            } else
            {
                return null;
            }

		}

		public void ChangeVersion(string versionName)
		{
			//try
			//{
                //before we change database version we need to be sure all our
                //creates new version then runs create version
                //need a better way to check that data in map is pointing to correct version
                //here we're swiching the default
                //loading all the tables
                //then changing versions
                //addtable and addstandalonetable should be version aware

                //method on a Map, you may wish to call IMapAdmin::FireChangeVersion
                SaveEdits(false);
                IVersionedWorkspace4 Vwksp = (IVersionedWorkspace4)_BatchWorkspace;
				IVersion VFrom = (IVersion3)Vwksp;
				IVersion VTo = Vwksp.FindVersion(versionName);
				IChangeDatabaseVersion VChange = new ChangeDatabaseVersion();
				VChange.Execute(VFrom, VTo, (IBasicMap)ArcMap.Document.FocusMap);
				ArcMap.Document.ActiveView.Refresh();
				_BatchWorkspace = (IWorkspace2)VTo;
                SetBatchGUID(versionName);
                _VersionName = versionName;

           if (versionName != "Default" | versionName !="sde.Default")
                {
                    LoadVersioned();


                } else
                {

                }
                
			//}
			//catch (Exception e)
			//{
			//	MessageBox.Show(e.Message, "Change Version");
			//}
		}
        private Boolean SetBatchGUID(string versionName)
        {
            int c = versionName.IndexOf(".") + 1;
            versionName = versionName.Substring(c, versionName.Length - c);

            //this will return for the default version. if the version hasn't been posted then this batch does not exist.
            IQueryFilter qf = new QueryFilterClass();
            qf.WhereClause = "batchname ='" + versionName + "'";

            IStandaloneTable batchStandAloneTble = _workerBee.GetStandAloneTable("pgelibrary.geo.batch");
            ITable tbl = batchStandAloneTble.Table;

            ICursor cur;
            cur = tbl.Search(qf, false);
            IRow r;
            r = cur.NextRow();
            if (r != null)
            {
                
                _BatchGUID = r.get_Value(r.Fields.FindField("globalid")) as string;
                _BatchPath = r.get_Value(r.Fields.FindField("batchpath")) as string;
                return true;
            }
            return false;
        }
		private string GetUniqueValueList(IDisplayTable displayTable, string searchField, string qfWhere)
		{
			string sQuote;
			if (displayTable.DisplayTable.FindField(searchField) == -1)
			{
				return null;
			}
			else
			{
				if (displayTable.DisplayTable.Fields.Field[displayTable.DisplayTable.FindField(searchField)].Type == esriFieldType.esriFieldTypeString)
				{
					sQuote = "'";
				}
				IQueryFilter qf = new QueryFilterClass();
				qf.WhereClause = qfWhere;
				DataStatistics dataStatistics = new DataStatistics();
				dataStatistics.Field = searchField;
				dataStatistics.Cursor = displayTable.DisplayTable.Search(qf, false);

				IEnumerator uniqueValues = dataStatistics.UniqueValues;
				string outList = "";
				while (uniqueValues.MoveNext())
				{
					object current = uniqueValues.Current;
					if (outList == "")
					{
						outList = current.ToString();
					}
					else
					{
						outList = outList + "," + current.ToString();
					}
				}
				return outList;
			}
		}

		public void CreateVersion(string versionName, int fwjobnumber, int pgenumber, string comment, string batchpath)
		{
			try
			{
				//creates new version then runs create version
				IVersionedWorkspace4 Vwksp = (IVersionedWorkspace4)_BatchWorkspace;
				IVersion VFrom = (IVersion3)Vwksp;
				if (VFrom != Vwksp.DefaultVersion)
				{
					IChangeDatabaseVersion VChange = new ChangeDatabaseVersion();
					VChange.Execute(VFrom, Vwksp.DefaultVersion, (IBasicMap)ArcMap.Document.FocusMap);
					VFrom = Vwksp.DefaultVersion;
				}

                //need to add batch name to batch log before create version
                //should check that version name does not exist first
                StopEditing();
				StartEditing(esriMultiuserEditSessionMode.esriMESMVersioned);
                //fetch objects to edit after start edit
                _BatchLog = new FWTableWrapper(_workerBee.GetStandAloneTable("pgelibrary.geo.batch").Table, null);
                AppendBatch(versionName, fwjobnumber, pgenumber, comment, batchpath);
				StopEditing(false);
				VFrom.CreateVersion(versionName);

				//batch edits are non-versioned so batch name and id is available to all users immeidately
				ChangeVersion(WorkspaceUser() + "." + versionName);
			}
			catch (Exception e)
			{
				MessageBox.Show(e.Message, "Create Version");
			}
		}
        public void SaveEdits(bool restartEditng )
        {
            if (_Editor != null)
            {
                _Editor.StopEditing(true);
                if (restartEditng)
                {
                    _Editor.StartEditing((IWorkspace)_BatchWorkspace);
                }
                //save edits
                //show editor toolbar in edit mode
            }
        }
        private bool StartEditing(ESRI.ArcGIS.Geodatabase.esriMultiuserEditSessionMode editMode = esriMultiuserEditSessionMode.esriMESMNonVersioned)
		{

            //_MUEditor = (IMultiuserWorkspaceEdit)_BatchWorkspace;
			//IWorkspaceEdit2 wkspedit = (IWorkspaceEdit2)_MUEditor;
            UID editUid = new UIDClass();
            editUid.Value = "esriEditor.Editor";
            //You can get app from ICommand :: OnCreate() hook parameter   
            _Editor = (IEditor3)ArcMap.Application.FindExtensionByCLSID(editUid);
            //check to see if a workspace is already being edited.
            if(_Editor.EditState==esriEditState.esriStateNotEditing)
            {
                _Editor.StartEditing((IWorkspace)_BatchWorkspace);
                return true;
            } else
            {
                return false;
            }
   //         if (wkspedit.IsBeingEdited() == false)
			//{
			//	_MUEditor.StartMultiuserEditing(editMode);
                
			//	return true;
			//}
			//else
			//{
			//	return false;
			//}
		}

		private bool StopEditing(Boolean asktosave = true)
		{
            //if (_MUEditor != null){
			//    IWorkspaceEdit wkspedit = (IWorkspaceEdit)_MUEditor;
                if (_Editor != null)
                {
                   // IWorkspaceEdit wkspedit = (IWorkspaceEdit)_Editor;
                   // bool wkspedits = false;

			   // wkspedit.HasEdits(ref wkspedits);
               
			   // if (wkspedits)
			    //{
				    if (asktosave == false || (asktosave && MessageBox.Show("Do you want to save your edits?", "Save Edits", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes))
				    {
                    _Editor.StopEditing(true);
					    return true;
				    }
				    else
				    {
                    _Editor.StopEditing(false);
					    return true;
				    }
			    //}
            }
			 return false;

		}

		public string WorkspaceUser()
		{
			//returns the workspace username since this could be windows or generic
			IWorkspace ws = (IWorkspace)_BatchWorkspace;
			IPropertySet wps = ws.ConnectionProperties;
			object n;
			object v;
			wps.GetAllProperties(out n, out v);
			if (wps.GetProperty("AUTHENTICATION_MODE").ToString() == "OSA")
			{
				return System.Security.Principal.WindowsIdentity.GetCurrent().Name.Split('\\').Last();
			}
			else
			{
				return wps.GetProperty("user").ToString();
			}
		}

		public void Load()
		{
			//FeatureLayerList flList = new FeatureLayerList();

			//this check is needed so we don't end up with an empty dataset when first loading the document
			if (ArcMap.Document.FocusMap != null)
			{
				//flList = _workerBee.GetLayers(null, _BatchWorkspace, null);
				_BatchLog = new FWTableWrapper(_workerBee.GetStandAloneTable("pgelibrary.geo.batch").Table, null);
			}
		}
        #region "LoadWrappedTables"
        private void LoadResourceVersion()
        {
            IQueryFilter qf = new QueryFilterClass();
            string whereclause = "batchid = '" + _BatchGUID + "'";
            

            qf.WhereClause = whereclause;
            
            _VersionedResourceLog = new FWTableWrapper(_workerBee.GetStandAloneTable("pgelibrary.geo.batchresource").Table, (IQueryFilter2)qf);

            qf = new QueryFilterClass();
            string idlist = GetUniqueValueList((IDisplayTable)_workerBee.GetStandAloneTable("pgelibrary.geo.batchresource"), "resourceid", "batchid = '" + _BatchGUID + "'");
            if (!String.IsNullOrEmpty(_ResourceGISFilter))
            {
                string temp = "";
                //List<string> filterList = _ReportGISFilter.ToList<string>();
                idlist.Split(',').ToList().ForEach(k => { if (!_ResourceGISFilter.Split(',').ToList().Contains(k)) { temp += k + ","; } });
                idlist = temp.Trim(',');
            }
            qf.WhereClause = idlist == "" ? "resourceid in (-9)" : "resourceid in (" + idlist + ")";
            if (!String.IsNullOrEmpty(_ResourceFilter) )
            {
                qf.WhereClause = qf.WhereClause + " AND (" + _ResourceFilter +")";
            }

            _VersionedResourceGISWhere = idlist;
            _VersionedResourceRecords = new FWTableWrapper(_workerBee.GetStandAloneTable("pgelibrary.geo.resources").Table, (IQueryFilter2)qf);
            _VersionedXrefResource = new FWTableWrapper(_workerBee.GetStandAloneTable("pgelibrary.geo.xrefsresource").Table, (IQueryFilter2)qf);

        }
        private void LoadReportVersion()
        {
            IQueryFilter qf = new QueryFilterClass();
            string whereclause = "batchid = '" + _BatchGUID + "'";
            qf.WhereClause = whereclause;
            _VersionedReportLog = new FWTableWrapper(_workerBee.GetStandAloneTable("pgelibrary.geo.batchreport").Table, (IQueryFilter2)qf);

            qf = new QueryFilterClass();
            string idlist = GetUniqueValueList((IDisplayTable)_workerBee.GetStandAloneTable("pgelibrary.geo.batchreport"), "reportid", "batchid = '" + _BatchGUID + "'");
            //remove from list those with GIS features
            if (!String.IsNullOrEmpty(_ReportGISFilter))
            {
                string temp = "";
                //List<string> filterList = _ReportGISFilter.ToList<string>();
                idlist.Split(',').ToList().ForEach(k => { if (!_ReportGISFilter.Split(',').ToList().Contains(k)) { temp += k + ","; } });
                idlist = temp.Trim(',');
            }
            qf.WhereClause = idlist == "" ? "reportid in (-9)" : "reportid in (" + idlist + ")";
           if (!String.IsNullOrEmpty(_ReportFilter))
            {
                //whereclause = whereclause + " AND (" + _ReportFilter +")";
                qf.WhereClause = qf.WhereClause + " AND (" + _ReportFilter + ")";
            }
            _VersionedReportRecords = new FWTableWrapper(_workerBee.GetStandAloneTable("pgelibrary.geo.reports").Table, (IQueryFilter2)qf);
            _VersionedXrefReport = new FWTableWrapper(_workerBee.GetStandAloneTable("pgelibrary.geo.xrefreport").Table, (IQueryFilter2)qf);
            _VersionedReportGISWhere = idlist;


            //_VersionedMPEreportpersonnel = new FWTableWrapper(_workerBee.GetStandAloneTable("pgelibrary.geo.mpereportpersonnel").Table, (IQueryFilter2)qf);
           // whereclause = qf.WhereClause;
           // _VersionedMPEResource = new FWTableWrapper(_workerBee.GetStandAloneTable("pgelibrary.geo.mperesource").Table, (IQueryFilter2)qf);
           // _VersionedMPEreportpersonnel = new FWTableWrapper(_workerBee.GetStandAloneTable("pgelibrary.geo.mpereportpersonnel").Table, (IQueryFilter2)qf);

            //IStandaloneTable mperptpersonnel = _workerBee.GetStandAloneTable("pgelibrary.geo.mpereportpersonnel");
            //IDisplayTable dspmperptpersonnel = (IDisplayTable)mperptpersonnel;
            //IStandaloneTable versionedpersonnel = _workerBee.GetStandAloneTable("pgelibrary.geo.mpepersonnel");
            //IDisplayTable dspversionedpersonnel = (IDisplayTable)versionedpersonnel;

            //_workerBee.RemoveJoin(dspmperptpersonnel);

            //IDisplayRelationshipClass dispRelClass2;
            //IRelationshipClass relationshipClass2 = _workerBee.JoinTable(dspmperptpersonnel, dspversionedpersonnel, "personnelid", "objectid", ESRI.ArcGIS.Geodatabase.esriRelCardinality.esriRelCardinalityManyToMany);
            //dispRelClass2 = (IDisplayRelationshipClass)dspmperptpersonnel;
            //dispRelClass2.DisplayRelationshipClass(relationshipClass2, esriJoinType.esriLeftInnerJoin);
            //qf.WhereClause = whereclause.Replace("reportid", "pgelibrary.geo.mpereportpersonnel.reportid");
            //qf.SubFields = "pgelibrary.geo.mpepersonnel.firstname, pgelibrary.geo.mpepersonnel.lastname, pgelibrary.geo.mpepersonnel.company, pgelibrary.geo.mpepersonnel.title,pgelibrary.geo.mpepersonnel.status";
            //_VersionedMPEreportpersonnel = new FWTableWrapper(_workerBee.GetStandAloneTable("pgelibrary.geo.viewmpereportpersonnel").Table, (IQueryFilter2)qf);
            //       IQueryFilter qf = new QueryFilter();
            //qf.WhereClause = where;
            //_VersionedMPEResource = new FWTableWrapper(_workerBee.GetStandAloneTable("pgelibrary.geo.viewmperesources").Table, (IQueryFilter2)qf);

            ////alternative 1 for displaying labels with mperesource
            ///this would work if we filter the results to correct reportid after load
            //IStandaloneTable mperes = _workerBee.GetStandAloneTable("pgelibrary.geo.mperesource");
            //IDisplayTable dspmperes = (IDisplayTable)mperes;
            //IStandaloneTable versionedres = _workerBee.GetStandAloneTable("pgelibrary.geo.resources");
            //IDisplayTable dspversionedres = (IDisplayTable)versionedres;
            //_workerBee.RemoveJoin(dspmperes);

            //IDisplayRelationshipClass dispRelClass;
            //IRelationshipClass relationshipClass = _workerBee.JoinTable(dspmperes, dspversionedres, "resourceid", "resourceid", ESRI.ArcGIS.Geodatabase.esriRelCardinality.esriRelCardinalityOneToOne);
            //dispRelClass = (IDisplayRelationshipClass)dspmperes;
            //dispRelClass.DisplayRelationshipClass(relationshipClass, esriJoinType.esriLeftInnerJoin);
            //qf.WhereClause = whereclause.Replace("reportid", "pgelibrary.geo.mperesource.reportid");
            //qf.SubFields = "pgelibrary.geo.resources.primarylabel, pgelibrary.geo.resources.trinomiallabel, pgelibrary.geo.resources.fslabel, pgelibrary.geo.resources.othername,pgelibrary.geo.mperesource.*";
            //_VersionedMPEResource = new FWTableWrapper(dspmperes.DisplayTable, (IQueryFilter2)qf);
            //IQueryFilter qf2 = new QueryFilter();
            //qf2.WhereClause = whereclause;
            //test to see where the subfields are coming from
            //_VersionedMPEResource = new FWTableWrapper(_workerBee.GetStandAloneTable("pgelibrary.geo.viewmperesources").Table,(IQueryFilter2)qf2);

        }
        public void LoadVersioned()
		{
			//FeatureLayerList flList = new FeatureLayerList();

			//this check is needed so we don't end up with an empty dataset when first loading the document
			if (ArcMap.Document.FocusMap != null)
			{
				//if we have no rows qf is returning all rows
                LoadResourceVersion();
                LoadReportVersion();
                IQueryFilter qf = new QueryFilterClass();
                string whereclause;
               // if (_VersionedReportGISWhere == string.Empty) { _}
               if (String.IsNullOrEmpty(_VersionedReportGISWhere))
                {
                    _VersionedReportGISWhere = "-9";
                }
                if (String.IsNullOrEmpty(_VersionedResourceGISWhere))
                {
                    _VersionedResourceGISWhere = "-9";
                }
                //String.IsNullOrEmpty(_VersionedReportGISWhere) ? "-9" : _VersionedReportGISWhere +
                whereclause = "reportid in (" +
                   _VersionedReportGISWhere + 
                    ") OR resourceid in (" + 
                    _VersionedResourceGISWhere + ")";
                qf.WhereClause = whereclause;

                _VersionedSiteReportEvents = new FWTableWrapper(_workerBee.GetStandAloneTable("pgelibrary.geo.resourcereportevents").Table,(IQueryFilter2)qf);

               // _VersionedXrefReport = new FWTableWrapper(_workerBee.GetStandAloneTable("pgelibrary.geo.xrefreport").Table);
				//IDisplayTable d = ((IDisplayTable)_workerBee.GetStandAloneTable("pgelibrary.geo.xrefsresource"));
				//_VersionedXrefResource = new FWTableWrapper(d.DisplayTable);


               
       }
		}

		public Boolean VersionLoaded()
		{
			IVersionedWorkspace4 Vwksp = (IVersionedWorkspace4)_BatchWorkspace;
			IVersion VFrom = (IVersion3)Vwksp;
			if (VFrom != Vwksp.DefaultVersion)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


#endregion
  //      private IWorkspace2 GetTempWorkspace()
		//{
		//	IWorkspaceFactory2 WkspFact;
		//	Type wksptype = Type.GetTypeFromProgID("esriDataSourcesGDB.FileGDBWorkspaceFactory");
		//	WkspFact = (IWorkspaceFactory2)Activator.CreateInstance(wksptype);
		//	_TempWorkspace = (IWorkspace2)WkspFact.OpenFromFile(PGELibraryAddin.Properties.Settings.Default.UserScratchWorkspace, ArcMap.Application.hWnd);
		//	return _TempWorkspace;
		//}

		//public string SetTempWorkspace(string path = "")
		//{
		//	try
		//	{
		//		IGxDialog gxdlg = new GxDialogClass();
		//		IGxObjectFilterCollection gxObjFilterCollection;
		//		IEnumGxObject rsltEnumObj;
		//		gxObjFilterCollection = (IGxObjectFilterCollection)gxdlg;
		//		gxObjFilterCollection.AddFilter(new GxFilterFileGeodatabases(), true);
		//		gxdlg.Title = "Choose a scratch workspace";
		//		gxdlg.AllowMultiSelect = false;
		//		gxdlg.DoModalOpen(ArcMap.Application.hWnd, out rsltEnumObj);
		//		IGxObject gxObj;
		//		gxObj = rsltEnumObj.Next();
		//		if (gxObj != null)
		//		{
		//			PGELibraryAddin.Properties.Settings.Default.UserScratchWorkspace = gxObj.FullName;
		//		}
		//	}
		//	catch (Exception e)
		//	{

		//		MessageBox.Show(e.Message, "SetTempWorkspace Error", MessageBoxButtons.OK);
		//	}

		//	return PGELibraryAddin.Properties.Settings.Default.UserScratchWorkspace;
		//}



        public Boolean ReplaceResourceRecord (int existingID,int importID)
        {
            //ask if we should replace
            //delete importid from batch table
            //delete record for importid
            //add existingID to batch table
            //update tablewrapper - done at 
            if (MessageBox.Show("Replace the record?","Match replace",MessageBoxButtons.YesNo) == DialogResult.Yes)
            {

                //this will save entries such as labels that cause the match to occur
                StopEditing(false);
                StartEditing(esriMultiuserEditSessionMode.esriMESMVersioned);
                //IWorkspaceEdit2 wkspedit = (IWorkspaceEdit2)_MUEditor;
                //wkspedit.StartEditOperation();
                _Editor.StartOperation();
                
                string sourcePDFPath = DeleteResourceLog(existingID);
                AppendResourceLog(importID, sourcePDFPath);
                UpdateResourcePDF(importID, sourcePDFPath);
                DeleteResource(existingID);
                //_Editor.HasEdits(true);
                if (_Editor.HasEdits())
                {
                    _Editor.StopOperation("Replace resource record");
                }
                StopEditing(false);
                LoadResourceVersion();
                return true;
            } else
            {
                return false;
            }
        }



        public Boolean ReplaceReportRecord(int existingID, int importID)
        {
            //ask if we should replace
            //delete importid from batch table
            //delete record for importid
            //add existingID to batch table
            //update tablewrapper - done at 
            if (MessageBox.Show("Replace the record?", "Match replace", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                //this will save entries such as labels that cause the match to occur
                StopEditing(false);
                StartEditing(esriMultiuserEditSessionMode.esriMESMVersioned);
                _Editor.StartOperation();
                //wkspedit.StartEditOperation();
                string sourcepdf = DeleteReportLog(existingID);
                AppendReportLog(importID, sourcepdf);
                UpdateReportPDF(importID, sourcepdf);
                DeleteReport(existingID);
                if (_Editor.HasEdits())
                {
                    _Editor.StopOperation("Replace report record");
                }
                StopEditing(false);
                LoadReportVersion();
                return true;
            }
            else
            {
                return false;
            }
        }
        #region "FilterWrappedTables"
        public void FilterResourceRecords(string FieldSelection, string FieldValue, FilterType filterType)
        {

            if (filterType == FilterType.Clear)
            {
                _ResourceFilter = "";
            } else if (filterType == FilterType.New)
            {
                _ResourceFilter = FieldSelection + " LIKE '%" + FieldValue + "%'";
            } else if (filterType == FilterType.Add)
            {
                _ResourceFilter = _ResourceFilter + " AND " + FieldSelection + " LIKE '%" + FieldValue + "%'";
            }
            else if (filterType == FilterType.GISAdd)
            {
                _ResourceGISFilter =  FieldValue;

            }
            else if (filterType == FilterType.GISRemove)
            {
                _ResourceGISFilter = "";
            }
            LoadResourceVersion();
        }
        public void FilterReportRecords(string FieldSelection, string FieldValue, FilterType filterType)
        {
            if (filterType == FilterType.Clear)
            {
                _ReportFilter = "";
            }
            else if (filterType == FilterType.New)
            {
                _ReportFilter = FieldSelection + " Like '%" + FieldValue + "%'";

            }
            else if (filterType == FilterType.Add)
            {
                _ReportFilter = _ReportFilter + " AND " + FieldSelection + " LIKE '%" + FieldValue + "%'";
            }
            else if (filterType == FilterType.GISAdd)
            {

                _ReportGISFilter = FieldValue;

            }
            else if (filterType == FilterType.GISRemove)
            {
                _ReportGISFilter = "";
            }
            LoadReportVersion();
        }
        #endregion
        #region BatchTableWrappers
        //temporary dataset of batch report records
        public FWTableWrapper BatchReportRecords()
        {
            return _VersionedReportRecords;
        }
        public FWTableWrapper BatchResourceRecords()
        {
            return _VersionedResourceRecords;
        }
        public FWTableWrapper BatchViewMPEResourceRecords(string where)
        {
            IQueryFilter qf = new QueryFilter();
            qf.WhereClause = where;
            _VersionedViewMPEResource = new FWTableWrapper(_workerBee.GetStandAloneTable("pgelibrary.geo.viewmperesources").Table, (IQueryFilter2)qf);
            return _VersionedViewMPEResource;

        }
        public FWTableWrapper BatchMPEResourceRecords(string where)
        {

            IQueryFilter qf = new QueryFilter();
            qf.WhereClause = where;
            _VersionedMPEResource = new FWTableWrapper(_workerBee.GetStandAloneTable("pgelibrary.geo.mperesource").Table, (IQueryFilter2)qf);
           return _VersionedMPEResource;

        }
        public FWTableWrapper BatchMPEReportRecords(string where)
        {

            IQueryFilter qf = new QueryFilter();
            qf.WhereClause = where;
            _VersionedMPEReports = new FWTableWrapper(_workerBee.GetStandAloneTable("pgelibrary.geo.mpereport").Table, (IQueryFilter2)qf);
            return _VersionedMPEReports;
        }
        //public FWTableWrapper BatchViewMPEPersonnelRecords(string where)
        //{
        //    IQueryFilter qf = new QueryFilter();
        //    qf.WhereClause = where;
        //    _VersionedViewMPEreportpersonnel = new FWTableWrapper(_workerBee.GetStandAloneTable("pgelibrary.geo.viewmpereportpersonnel").Table, (IQueryFilter2)qf);
        //    //string sqlString;
        //    //sqlString = "SELECT (pgelibrary.geo.mpepersonnel_evw.lastname::text || ', '::text) || pgelibrary.geo.mpepersonnel_evw.firstname::text AS person, " +
        //    //"pgelibrary.geo.mpepersonnel_evw.company, pgelibrary.geo.mpepersonnel_evw.firmlookup,  " +
        //    //"pgelibrary.geo.mpereportpersonnel_evw.reportid, pgelibrary.geo.mpereportpersonnel_evw.personnelid,  " +
        //    //"pgelibrary.geo.mpereportpersonnel_evw.objectid, " +
        //    //"(SELECT sde.sde_set_current_version('" + _VersionName.Replace(this.WorkspaceUser()+".", string.Empty) +"') as v) " +
        //    //"FROM pgelibrary.geo.mpepersonnel_evw, pgelibrary.geo.mpereportpersonnel_evw " +
        //    //"WHERE pgelibrary.geo.mpepersonnel_evw.objectid = pgelibrary.geo.mpereportpersonnel_evw.personnelid";


        //    //_VersionedViewMPEreportpersonnel = new FWTableWrapper(_workerBee.GetStandAloneQueryTable("tempMPEReportPersonnel",sqlString, "objectid",this.BatchWorkspace).Table);
        //    //return _VersionedViewMPEreportpersonnel;
        //}
        public FWTableWrapper BatchMPEPersonnelRecords(string where)
        {
            IQueryFilter qf = new QueryFilter();
            qf.WhereClause = where;
            _VersionedMPEreportpersonnel=            new FWTableWrapper(_workerBee.GetStandAloneTable("pgelibrary.geo.mpereportpersonnel").Table, (IQueryFilter2)qf);
            return _VersionedMPEreportpersonnel;
        }
        //public FWTableWrapper BatchReportXref()
        //{
        //    return _VersionedXrefReport;
        //}
        //public FWTableWrapper BatchResourceXref()
        //{
        //    return _VersionedXrefResource;
        //}
        public FWTableWrapper BatchResourceReport()
        {
            return _VersionedSiteReportEvents;
        }
        public string BatchResourceGISWhere()
        {
            return _VersionedResourceGISWhere;
        }
        public string BatchReportGISWhere()
        {
            return _VersionedReportGISWhere;
        }
        #endregion
        public string VersionName()
        {
            return _VersionName;
        }
        public bool CompleteBatch()
        {
            //copy pdfs
            StartEditing(esriMultiuserEditSessionMode.esriMESMVersioned);
            _Editor.StartOperation();
            CopyPDFs(_VersionedResourceRecords, ImportType.ResourcePDF,"origfilepath", "pdf","primco");
            CopyPDFs(_VersionedReportRecords, ImportType.ReportPDF, "origpath", "pdf");
            _Editor.StopOperation("Copied PDFs");
            StopEditing(false);
            //reconcile and post
            return ReconcileandPost();


        }
        
        private bool ReconcileandPost()
        {
            IVersionedWorkspace4 Vwksp = (IVersionedWorkspace4)_BatchWorkspace;
            IVersion Vchild = (IVersion3)Vwksp;
            IVersion VDefault = Vwksp.DefaultVersion;
            


            IMultiuserWorkspaceEdit muWorkspaceEdit = (IMultiuserWorkspaceEdit)Vchild;
            IWorkspaceEdit workspaceEdit = (IWorkspaceEdit2)Vchild;
            IVersionEdit4 versionEdit = (IVersionEdit4)workspaceEdit;
            bool returnResult = false;
            if (muWorkspaceEdit.SupportsMultiuserEditSessionMode
                (esriMultiuserEditSessionMode.esriMESMVersioned))
            {
                muWorkspaceEdit.StartMultiuserEditing
                    (esriMultiuserEditSessionMode.esriMESMVersioned);
                //Reconcile with the target version.
                bool conflicts = versionEdit.Reconcile4(VDefault.VersionName, true,
                    false, false, false);
                if (conflicts)
                {
                    MessageBox.Show(" Conflicts Detected. Please review and manually reconcile and post.");
                }
                else
                {
                    DialogResult dialogResult;
                    dialogResult = MessageBox.Show(" No Conflicts Detected. Do you want to delete the version after post? ", "Delete Version?", MessageBoxButtons.YesNo);
                    workspaceEdit.StartEditOperation();
                    //Post to the target version.
                    if (versionEdit.CanPost())
                        versionEdit.Post(VDefault.VersionName);
                    workspaceEdit.StopEditOperation();
                    workspaceEdit.StopEditing(true);
                    if (dialogResult==DialogResult.Yes)
                    {
                        ChangeVersion("sde.DEFAULT");

                        returnResult = true;

                    } 
                }

            }
            return returnResult;
        }
        public void TestProcessing()
        {
            IRow r;
            int id;
            IEnumerator<IRow> rows = _VersionedReportRecords.GetEnumerator();
            rows.MoveNext();
            r = rows.Current;
            while (r != null)
            {
                string reportlabel = GenerateReportLabel(r.get_Value(r.Fields.FindField("agencycompany")).ToString(),
                    r.get_Value(r.Fields.FindField("year")).ToString(),
                    r.get_Value(r.Fields.FindField("author")).ToString(),
                    r.get_Value(r.Fields.FindField("temptitle")).ToString(),
                    r.get_Value(r.Fields.FindField("reporttype")).ToString(),
                    r.get_Value(r.Fields.FindField("reportnumber")).ToString());
                logger.Info(reportlabel + "|" + r.get_Value(r.Fields.FindField("agencycompany")).ToString() + "," +
                    r.get_Value(r.Fields.FindField("year")).ToString() + "," +
                    r.get_Value(r.Fields.FindField("author")).ToString() + "," +
                    r.get_Value(r.Fields.FindField("temptitle")).ToString() + "," +
                    r.get_Value(r.Fields.FindField("reporttype")).ToString() + "," +
                    r.get_Value(r.Fields.FindField("reportnumber")).ToString());
                rows.MoveNext();
                r = rows.Current;
            }
        }
        private void CopyPDFs(FWARGTableWrapper.FWTableWrapper wrappedTable, ImportType importType,string origPathField, string permPathField, string countyfield = "")
        {
            IRow r;
            string origpath;
            string permpath;
            int county = 0;
            int id;
            IEnumerator<IRow> rows = wrappedTable.GetEnumerator();
            
            rows.MoveNext();
            r = rows.Current;
            while (r != null)
            {
                //r.get_Value(r.Fields.FindField(origPathField))
               
                object origpathresult = r.get_Value(r.Fields.FindField(origPathField));
                object permpathresult = r.get_Value(r.Fields.FindField(permPathField));
                if (String.IsNullOrEmpty(Convert.ToString(origpathresult)) |  !String.IsNullOrEmpty(Convert.ToString(permpathresult))){
                    //if the orignal path is null (no pdf) there is nothing to copy
                    //if the permpath is filled then edits should have occurred to this pdf and there is nothing to copy
                    logger.Debug(string.Format("Origpath is empty or permpath is not empty"));
                
                }
                else {
                    if (countyfield!="" )
                        {
                        object o = r.get_Value(r.Fields.FindField(countyfield));
                        if (o.Equals(DBNull.Value)==false){
                            county = (int)o;
                        }
                        }
                    permpath = Convert.ToString(permpathresult);
                    logger.Debug(string.Format("CopyPDFs permanent pdf is {0}", permpath));

                    origpath = Convert.ToString(origpathresult);
                    logger.Debug(string.Format("CopyPDFs original pdf is {0}", origpath));

                    if (string.IsNullOrEmpty(permpath))
                    {

                    if (System.IO.File.Exists(origpath))
                    {
                        string pathbase = Properties.Settings.Default.PDFSource;
                        string path2;
                        string pathcounty;
                        string filename;
                        string newfilename;
                        int i=1;
                        //filename = System.IO.Path.GetFileName(origpath);
                        if (importType == ImportType.ResourcePDF)
                        {
                            filename = Coalesce(Convert.ToString(r.get_Value(r.Fields.FindField("primarylabel"))),
                                 Convert.ToString(r.get_Value(r.Fields.FindField("trinomiallabel"))),
                                 Convert.ToString(r.get_Value(r.Fields.FindField("fslabel"))),
                                 Convert.ToString(r.get_Value(r.Fields.FindField("hrinumber"))),
                                 Convert.ToString(r.get_Value(r.Fields.FindField("othername"))),
                                Convert.ToString(r.get_Value(r.Fields.FindField("resourceid")))
                                );
                            filename = MakeValidFileName(filename) + ".pdf";
                                

                            pathcounty = (string)_bds.lookupcountycodes.Select("primco = " + county)[0]["countyname"];

                                logger.Debug(string.Format("CopyPDF -path county is {0}",pathcounty));
                            pathcounty = pathcounty == "" ? "_Misc" : pathcounty;
                                logger.Debug(string.Format("CopyPDF -path county is {0}", pathcounty));
                                path2 = "Site Records";
                            permpath = System.IO.Path.Combine(pathbase, path2, pathcounty,filename);
                            while (System.IO.File.Exists(permpath))
                            {
                                    if (i == 1)
                                    {
                                        newfilename = filename.Replace(".pdf", "");
                                    }
                                    else
                                    {
                                        newfilename = filename.Replace("_" + (i - 1) + ".pdf", "");
                                    }
                                    filename = newfilename + "_" + i + ".pdf";
                                permpath = System.IO.Path.Combine(pathbase, path2, pathcounty, filename);
                                i++;
                            }
                        } else
                        {
                            path2 = "Reports";
                                string reportlabel = GenerateReportLabel(r.get_Value(r.Fields.FindField("agencycompany")).ToString(),  
                                    r.get_Value(r.Fields.FindField("year")).ToString(),
                                    r.get_Value(r.Fields.FindField("author")).ToString(),
                                    r.get_Value(r.Fields.FindField("temptitle")).ToString(),
                                    r.get_Value(r.Fields.FindField("reporttype")).ToString(),
                                    r.get_Value(r.Fields.FindField("reportnumber")).ToString());
                            filename = Coalesce(Convert.ToString(r.get_Value(r.Fields.FindField("label"))),
                            Convert.ToString(r.get_Value(r.Fields.FindField("reportnumber"))),
                            Convert.ToString(r.get_Value(r.Fields.FindField("author"))) + " " +
                            Convert.ToString(r.get_Value(r.Fields.FindField("year")))
                            );
                            filename = MakeValidFileName(filename) + ".pdf";

                            permpath = System.IO.Path.Combine(pathbase, path2, filename);
                            while (System.IO.File.Exists(permpath))
                            {
                                    if (i == 1)
                                    {
                                        newfilename = filename.Replace(".pdf", "");
                                    } else
                                    {
                                        newfilename = filename.Replace("_" + (i - 1) + ".pdf", "");
                                    }
                                    
                                
                                filename = newfilename + "_" + i + ".pdf";
                                permpath = System.IO.Path.Combine(pathbase, path2,  filename);
                                i++;
                            }
                        }
                            //copy the files
                            logger.Debug(string.Format("CopyPDFs copying {0} to {1}", origpath,permpath));
                            System.IO.File.Copy(origpath, permpath);
                            //now store the result
                            r.set_Value(r.Fields.FindField(permPathField), permpath);
                            r.Store();
                    } else
                    {
                            //log file not found
                            logger.Error(string.Format("CopyPDFs failed to find {0}", origpath));
                            throw new FileNotFoundException("Could not find " + origpath);
                    }
                    }

                }
                rows.MoveNext();
                r = rows.Current;
            }
        }

        private string GenerateReportLabel(string agencycovalue = "", string yearvalue = "", string authorvalue="", string temptitlevalue = "", string reporttypevalue = "",string reportnumbervalue = "")
        {
            //company or author + desc + reptype + repnum +Year
            string companyclean = "";
            string outLabel = "";
            string outWho="";
            string outType = "";
            string outWhen = "";

            try
            {
                if (agencycovalue.Length > 0)
                {
                    
                    System.Data.DataRow[] r = _bds.lookupagency.Select("agencygrouped = '" + agencycovalue + "'");
                    if (r.Count() != 0)
                    {
                        companyclean = (string)r[0]["agencyabbr"];
                    }
                    else
                    {
                        companyclean = agencycovalue;
                    }
                    //string invalidChars = System.Text.RegularExpressions.Regex.Escape(new string(System.IO.Path.GetInvalidFileNameChars()));
                    System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(@"[',;\.+$)|()<>#^\\/]");

                        companyclean = regex.Replace(companyclean,  "");
                        companyclean = companyclean.Replace("Associates", "Assoc").Replace("Company", "Co").Replace("Corporation", "Corp").Replace("Environmental", "Enviro").Replace("Services", "Serv").Replace("FarWesternAnthropologicalResearchGroup", "FarWestern");
                    companyclean = companyclean.Replace(" ", "");
                    companyclean = companyclean.Length > 25 ? companyclean.Substring(0, 25) : companyclean;
                    outWho = companyclean;
                    

                } else if (authorvalue.Length > 0)
                {
                    outWho = CropString(authorvalue,",");
                    outWho = CropString(outWho, " ");
                    outWho = CropString(outWho, ";");
                    outWho = CropString(outWho, "/");
                    outWho = CropString(outWho, ".");
                } else if (temptitlevalue.Length > 0)
                {
                    outWho = temptitlevalue;
                }
                if (reportnumbervalue != "")
                {
                    outLabel = outWho + "_" + reportnumbervalue;

                } else
                {
                    outLabel = outWho;
                }

                if (reporttypevalue!= "")
                {
                    System.Data.DataRow[] r = _bds.lookupreporttype.Select("reporttype = '" + reporttypevalue + "'");
                    if (r.Count() != 0)
                    {
                        outType = "_" + (string)r[0]["reporttypeabbrev"];
                    } else
                    {
                        outType = "_" + reporttypevalue;
                    }
                }
                outLabel = outLabel + outType;

                if (yearvalue != "")
                {
                    outWhen = "_" + yearvalue;
                }
                outLabel = outLabel + outWhen;
                return MakeValidFileName(outLabel);

            }
            catch (Exception ex)
            {

                logger.Error(ex.Message,string.Format("Error Processing ReportLabel"));
                return string.Empty;
            }
        }
private string CropString(string txtString, string delim)
{
    int x;
    x = txtString.IndexOf(delim);
    txtString = x == -1 ? txtString : txtString.Substring(0, x );
    return txtString;
}

#region BatchEditing
private static string MakeValidFileName(string name)
        {
            string invalidChars = System.Text.RegularExpressions.Regex.Escape(new string(System.IO.Path.GetInvalidFileNameChars()));
            string invalidRegStr = string.Format(@"([{0}]*\.+$)|([{0}]+)", invalidChars);

            return System.Text.RegularExpressions.Regex.Replace(name, invalidRegStr, "_");
        }
        private static string Coalesce(params string[] strings)
        {
            return strings.FirstOrDefault(s => !string.IsNullOrEmpty(s));
        }
        public void ImportToBatch(ImportType importType, bool labelTitle = true)
		{
			IGxDialog gxDialog = new GxDialogClass();
			IEnumGxObject gxEnumObj;
			gxDialog.ObjectFilter = new GxFilterFileFolderClass();
            if (!string.IsNullOrEmpty(_BatchPath))
            {
                gxDialog.set_StartingLocation(_BatchPath);
            }
			if (gxDialog.DoModalOpen(ArcMap.Application.hWnd, out gxEnumObj)) {

			    IGxObject gxObj;
			    gxObj = gxEnumObj.Next();
			    if (gxObj != null)
			    {
                    //start an edit session on our new batch
                    StartEditing(esriMultiuserEditSessionMode.esriMESMVersioned);

                    _Editor.StartOperation();
                    string[] files = Directory.GetFiles(gxObj.FullName, "*.pdf");
				    foreach (var filePath in files)
				    {
					    switch (importType)
					    {
						    case ImportType.ResourcePDF:
                                AppendResourceToBatchFromPath(filePath);
							    break;
						    case ImportType.ResourceGIS:
							    break;
						    case ImportType.ReportPDF:
                                AppendReportToBatchFromPath(filePath,labelTitle);
							    break;
						    case ImportType.ReportGIS:
							    break;
						    default:
							    break;
					    }
				    }
                    _Editor.StopOperation("Importing " + importType.ToString());
			    }
            }

		}

		private void AppendBatch(string versionName, int fwjobnumber, int pgenumber, string comment, string batchpath)
		{
			IWorkspaceEdit2 wkspedit = (IWorkspaceEdit2)_BatchWorkspace;
			wkspedit.StartEditOperation();
			IRow row = _BatchLog.AddNew();
			row.set_Value(row.Fields.FindField("batchname"), versionName);
			row.set_Value(row.Fields.FindField("fwjobnumber"), fwjobnumber);
			row.set_Value(row.Fields.FindField("pgeordernumber"), pgenumber);
			row.set_Value(row.Fields.FindField("comment"), comment);
            row.set_Value(row.Fields.FindField("batchpath"), batchpath);
			row.Store();
			_BatchGUID = (String)row.get_Value(row.Fields.FindField("globalid"));
			wkspedit.StopEditOperation();
		}
        private void AppendReportToBatchFromPath(string fullpath, bool labelTitle)
        {
            try
            {
                IRow row = _VersionedReportRecords.AddNew();
                string fileLabel = Path.GetFileNameWithoutExtension(fullpath);
                if (labelTitle)
                {
                    row.set_Value(row.Fields.FindField("reporttitle"), fileLabel);
                } else
                {
                    if (fileLabel.Length > 50)
                    {
                        row.set_Value(row.Fields.FindField("reporttitle"), fileLabel);
                    } else
                    {
                        row.set_Value(row.Fields.FindField("reportnumber"), fileLabel);
                    }
                    
                }
                row.set_Value(row.Fields.FindField("origpath"), fullpath);
                row.Store();
                //_VersionedReportRecords.Add(row);
                AppendReportLog((int)row.get_Value(row.Fields.FindField("reportid")),fullpath);
            } catch (Exception e)
            {
                MessageBox.Show(e.Message, "AppendReportToBatchFromPath");
            }
        }
		private void AppendResourceToBatchFromPath(string fullpath)
		{
			try
			{
				IRow row = _VersionedResourceRecords.AddNew();
				string fileLabel = Path.GetFileNameWithoutExtension(fullpath);
				if (fileLabel.Substring(0, 2) == "P-")
				{
					//primary
					string[] primary = fileLabel.Split('-');
					if (primary.Length > 0)
					{
						row.set_Value(row.Fields.FindField("primco"), primary[1]);
						row.set_Value(row.Fields.FindField("primno"), primary[2]);
					}
					else
					{
						row.set_Value(row.Fields.FindField("othername"), fileLabel);
					}
				}
				else if (fileLabel.Substring(0, 3) == "CA-")
				{
					//trinomial
					string[] trinomial = fileLabel.Split('-');
					if (trinomial.Length > 0)
					{
                        if (fileLabel.Substring(fileLabel.Length - 2, 2) == "/H")
                        {
                            row.set_Value(row.Fields.FindField("trinh"), "/H");
                            trinomial[2] = trinomial[2].Replace("/H", "");
                        }
                         else if (fileLabel.Substring(fileLabel.Length - 1, 1) == "H")
						{
							row.set_Value(row.Fields.FindField("trinh"), "H");
							trinomial[2] = trinomial[2].Replace("H", "");
						}
						row.set_Value(row.Fields.FindField("trinno"), trinomial[2]);
						if (trinomial.Length > 3)
						{
							if (trinomial[3] != "")
							{
								row.set_Value(row.Fields.FindField("trinh"), trinomial[3]);
							}

						}
						row.set_Value(row.Fields.FindField("primco"), _bds.lookupcountycodes.Select("trinco = '" + trinomial[1] + "'")[0]["primco"]);
					}

				}
				else
				{
					row.set_Value(row.Fields.FindField("othername"), fileLabel);
				}

				row.set_Value(row.Fields.FindField("origfilepath"), fullpath);
				row.Store();
				//_VersionedResourceRecords.Add(row);
				AppendResourceLog((int)row.get_Value(row.Fields.FindField("resourceid")), fullpath);
			}
			catch (Exception e)
			{
				MessageBox.Show(e.Message, "AppendResourceToBatchError");
			}
		}

		private void AppendResourceLog(int rowid,string sourcepdf)
		{
			IRow reslogrow = _VersionedResourceLog.AddNew();
			reslogrow.set_Value(reslogrow.Fields.FindField("batchid"), _BatchGUID);
			reslogrow.set_Value(reslogrow.Fields.FindField("resourceid"), rowid);
            reslogrow.set_Value(reslogrow.Fields.FindField("sourcepdf"), sourcepdf);
			reslogrow.Store();
			_VersionedResourceLog.Add(reslogrow);
		}
        private string DeleteResourceLog(int resourceid)
        {
            //IWorkspaceEdit2 wkspedit = (IWorkspaceEdit2)_MUEditor;

            ITable t = _workerBee.GetStandAloneTable("pgelibrary.geo.batchresource").Table;
            IRow r;
            string filepath = "";
            IQueryFilter2 qf = new QueryFilterClass();
            qf.WhereClause = "resourceid = " + resourceid + " and batchid = '" + _BatchGUID + "'";
            qf.SubFields = "*";
            ICursor c = t.Search(qf, false);
            r = c.NextRow();
            int objid = 0;
            while (r != null)
            {
                objid = (int)r.get_Value(r.Fields.FindField("objectid"));
                filepath = (string)r.get_Value(r.Fields.FindField("sourcepdf"));
                r = c.NextRow();
            }
           // _VersionedResourceLog.Remove(t.GetRow(objid));

            t.DeleteSearchedRows(qf);
            return filepath;
        }
        private void DeleteResource(int resourceid)

        {
            //IWorkspaceEdit2 wkspedit = (IWorkspaceEdit2)_MUEditor;

            ITable t = _workerBee.GetStandAloneTable("pgelibrary.geo.resources").Table;
            //IRow r;
            //string filepath = "";
            IQueryFilter2 qf = new QueryFilterClass();
            qf.WhereClause = "resourceid = " + resourceid;
            qf.SubFields = "*";
            //ICursor c = t.Search(qf, false);
            //r = c.NextRow();
            //int objid = 0;
            //while (r != null)
            //{
            //    objid = (int)r.get_Value(r.Fields.FindField("resourceid"));
            //    r = c.NextRow();
            //}
            //_VersionedResourceRecords.Remove(t.GetRow(resourceid));
            //_Editor.StartOperation();
            t.DeleteSearchedRows(qf);
           // _Editor.StopOperation("Delete Resource Record");
            //return filepath;
        }
        private void UpdateResourcePDF(int importID, string sourcePDFPath)
        {
            ITable t = _workerBee.GetStandAloneTable("pgelibrary.geo.resources").Table;
            IRow r;
            IQueryFilter2 qf = new QueryFilterClass();
            qf.WhereClause = "resourceid = " + importID;
            qf.SubFields = "*";
            ICursor c = t.Search(qf, false);
            r = c.NextRow();
            while (r != null)
            {
                r.set_Value(r.Fields.FindField("origfilepath"), sourcePDFPath);
                r.Store();
                r = c.NextRow();
            }
        }
        private void UpdateReportPDF(int importID, string sourcePDFPath)
        {
            ITable t = _workerBee.GetStandAloneTable("pgelibrary.geo.reports").Table;
            IRow r;
            IQueryFilter2 qf = new QueryFilterClass();
            qf.WhereClause = "reportid = " + importID;
            qf.SubFields = "*";
            ICursor c = t.Search(qf, false);
            r = c.NextRow();
            while (r != null)
            {
                r.set_Value(r.Fields.FindField("origpath"), sourcePDFPath);
                r.Store();
                r = c.NextRow();
            }
        }
        private void AppendReportLog(int rowid, string sourcepdf)
        {
            IRow reslogrow = _VersionedReportLog.AddNew();
            reslogrow.set_Value(reslogrow.Fields.FindField("batchid"), _BatchGUID);
            reslogrow.set_Value(reslogrow.Fields.FindField("reportid"), rowid);
            reslogrow.set_Value(reslogrow.Fields.FindField("sourcepdf"), sourcepdf);
            reslogrow.Store();
            _VersionedReportLog.Add(reslogrow);
        }
        private string DeleteReportLog(int reportid)
        {

            ITable t = _workerBee.GetStandAloneTable("pgelibrary.geo.batchreport").Table;
            IRow r;
            string filepath = "";
            IQueryFilter2 qf = new QueryFilterClass();
            qf.WhereClause = "reportid = " + reportid + " and batchid = '" + _BatchGUID + "'";
            qf.SubFields = "*";
            ICursor c = t.Search(qf, false);
            r = c.NextRow();
            int objid = 0;
            while (r != null)
            {
                objid = (int)r.get_Value(r.Fields.FindField("objectid"));
                filepath = (string)r.get_Value(r.Fields.FindField("sourcepdf"));
                r = c.NextRow();
            }
            //_VersionedSiteReportEvents.Remove(t.GetRow(objid));
           // _VersionedReportLog.Remove(t.GetRow(objid));
            t.DeleteSearchedRows(qf);

            return filepath;
        }
        private void DeleteReport(int reportid)
        {

            ITable t = _workerBee.GetStandAloneTable("pgelibrary.geo.reports").Table;
            //IRow r;
            //string filepath = "";
            IQueryFilter2 qf = new QueryFilterClass();
            qf.WhereClause = "reportid = " + reportid ;
            qf.SubFields = "*";
            //ICursor c = t.Search(qf, false);
            //r = c.NextRow();
            //int objid = 0;
            //while (r != null)
            //{
            //    objid = (int)r.get_Value(r.Fields.FindField("reportid"));
            //    r = c.NextRow();
            //}
            //_VersionedSiteReportEvents.Remove(t.GetRow(objid));
            // _VersionedReportRecords.Remove(t.GetRow(reportid));
            //_Editor.StartOperation();
            t.DeleteSearchedRows(qf);
           // _Editor.StopOperation("Delete Report Records");
            // return filepath;
        }
        public void DeleteDuplicateResource(int DeleteID, int KeepID, bool deleteGIS, bool AppendBatch)
        {
            //check the data exists first
            ITable t = _workerBee.GetStandAloneTable("pgelibrary.geo.resources").Table;
            if (t.GetRow(KeepID) == null)
            {
                MessageBox.Show("ID to keep (" + KeepID + ") does not exist");
                return;
            }
            if (t.GetRow(DeleteID) == null)
            {
                MessageBox.Show("ID to delete (" + DeleteID + ") does not exist");
                return;
            }
            StartEditing(esriMultiuserEditSessionMode.esriMESMVersioned);
            //update related
            UpdateResourceXRef(DeleteID, KeepID);
            UpdateResourceReportEvent(DeleteID, KeepID,true);
            UpdateResourceLogDeleted(DeleteID);
            //_Editor.StartOperation();
            //string sourcePDFPath = DeleteResourceLog(DeleteID);
            if (AppendBatch)
            {
                AppendResourceLog(KeepID, "");
            }
            DeleteResource(DeleteID);
            if (deleteGIS)
            {
                DeleteResourceGIS(DeleteID);
            }

            StopEditing(false);
            LoadResourceVersion();


        }
        public void DeleteDuplicateReport(int DeleteID, int KeepID, bool deleteGIS, bool AppendBatch)
        {
            //check the data exists first
            ITable t = _workerBee.GetStandAloneTable("pgelibrary.geo.reports").Table;
            if (t.GetRow(KeepID) == null)
            {
                MessageBox.Show("ID to keep (" + KeepID + ") does not exist");
                return;
            }
            if (t.GetRow(DeleteID) == null)
            {
                MessageBox.Show("ID to delete (" + DeleteID + ") does not exist");
                return;
            }
            StartEditing(esriMultiuserEditSessionMode.esriMESMVersioned);
            
            //update related
            UpdateReportXRef(DeleteID, KeepID);
            UpdateResourceReportEvent(DeleteID, KeepID, false);
            UpdateReportLogDeleted(DeleteID);
           // _Editor.StartOperation();
            //string sourcePDFPath = DeleteResourceLog(DeleteID);
            if (AppendBatch)
            {
                AppendReportLog(KeepID, "");
            }
            DeleteReport(DeleteID);
            if (deleteGIS)
            {
                DeleteReportGIS(DeleteID);
            }

            StopEditing(false);
            LoadReportVersion();


        }
        public void DeleteMPEReport(int id)
        {
            ITable t = _workerBee.GetStandAloneTable("pgelibrary.geo.mpereport").Table;
            IQueryFilter2 qf = new QueryFilterClass();
            qf.WhereClause = "reportid = " + id;
            qf.SubFields = "*";
            t.DeleteSearchedRows(qf);

            ITable t2 = _workerBee.GetStandAloneTable("pgelibrary.geo.mpereportpersonnel").Table;
            t2.DeleteSearchedRows(qf);

            ITable t3 = _workerBee.GetStandAloneTable("pgelibrary.geo.mperesource").Table;
            t3.DeleteSearchedRows(qf);
        }
        public void AppendResource(int id)
        {
            
            //for manual additions
            StartEditing(esriMultiuserEditSessionMode.esriMESMVersioned);
            //IRow row = _VersionedResourceRecords.AddNew();
            //row.Store();
            //_VersionedResourceRecords.Add(row);

            AppendResourceLog(id, "");
            

        }
        public void AppendReport(int id)
        {
            //for manual additions
            StartEditing(esriMultiuserEditSessionMode.esriMESMVersioned);
            //IRow row = _VersionedReportRecords.AddNew();
            //row.Store();
            //_VersionedReportRecords.Add(row);
            AppendReportLog(id, "");

        }
        public void AppendMPEReport(int id)
        {
            StartEditing(esriMultiuserEditSessionMode.esriMESMVersioned);
            IRow r = _VersionedMPEReports.AddNew();
            r.set_Value(r.Fields.FindField("reportid"), id);
            r.Store();
            _VersionedMPEReports.Add(r);
            StopEditing();

        }
        public int AppendMPEResource(int reportid, int resourceid, string primarylabel, string trinomiallabel, string fslabel, string othername)
        {
            StartEditing(esriMultiuserEditSessionMode.esriMESMNonVersioned);
            IQueryFilter qf = new QueryFilter();
            FWTableWrapper wrappedTable = new FWTableWrapper(_workerBee.GetStandAloneTable("pgelibrary.geo.mperesource").Table, (IQueryFilter2)qf);
            IRow row = wrappedTable.AddNew();
            row.set_Value(row.Fields.FindField("reportid"), reportid);
            row.set_Value(row.Fields.FindField("resourceid"), resourceid);
            row.set_Value(row.Fields.FindField("primarylabel"), primarylabel);
            row.set_Value(row.Fields.FindField("trinomiallabel"), trinomiallabel);
            row.set_Value(row.Fields.FindField("fsnumlabel"), fslabel);
            row.set_Value(row.Fields.FindField("othername"), othername);

            row.Store();
            wrappedTable.Add(row);

            return Convert.ToInt32(row.get_Value(row.Fields.FindField("objectid")));
        }
        public int AppendMPEPersonnel(int reportid, int personid, string person, string company)
        {
            StartEditing(esriMultiuserEditSessionMode.esriMESMNonVersioned);
            IQueryFilter qf = new QueryFilter();
            FWTableWrapper wrappedTable =new FWTableWrapper(_workerBee.GetStandAloneTable("pgelibrary.geo.mpereportpersonnel").Table, (IQueryFilter2)qf);
            IRow row = wrappedTable.AddNew();
            row.set_Value(row.Fields.FindField("reportid") , reportid);
            row.set_Value(row.Fields.FindField("personnelid"), personid);
            row.set_Value(row.Fields.FindField("person"), person);
            row.set_Value(row.Fields.FindField("company"), company);
            row.Store();
           
            wrappedTable.Add(row);
            return Convert.ToInt32(row.get_Value(row.Fields.FindField("objectid")));
        }
        public void ResourcePDFChanged(int resid, string pdf)
        {
            StartEditing(esriMultiuserEditSessionMode.esriMESMVersioned);
            //IWorkspaceEdit2 wkspedit = (IWorkspaceEdit2)_MUEditor;
            //wkspedit.StartEditOperation();
            _Editor.StartOperation();
            ITable t = _workerBee.GetStandAloneTable("pgelibrary.geo.batchresource").Table;
            IRow r;
            string filepath = "";
            IQueryFilter2 qf = new QueryFilterClass();
            qf.WhereClause = "resourceid = " + resid + " and batchid = '" + _BatchGUID + "'";
            qf.SubFields = "*";
            ICursor c = t.Search(qf, false);
            r = c.NextRow();
            int objid = 0;
            while (r != null)
            {
                r.set_Value(r.Fields.FindField("sourcepdf"), pdf);
                r.Store();
                r = c.NextRow();
            }
            if (_Editor.HasEdits())
            {
                _Editor.StopOperation("Update Resource Log PDF");
            }

            StopEditing(false);
            
        }
        public void ReportPDFChanged(int repid, string pdf)
        {
            StartEditing(esriMultiuserEditSessionMode.esriMESMVersioned);
            //IWorkspaceEdit2 wkspedit = (IWorkspaceEdit2)_MUEditor;
            //wkspedit.StartEditOperation();
            _Editor.StartOperation();
            ITable t = _workerBee.GetStandAloneTable("pgelibrary.geo.batchreport").Table;
            IRow r;
            string filepath = "";
            IQueryFilter2 qf = new QueryFilterClass();
            qf.WhereClause = "reportid = " + repid + " and batchid = '" + _BatchGUID + "'";
            qf.SubFields = "*";
            ICursor c = t.Search(qf, false);
            r = c.NextRow();
            int objid = 0;
            while (r != null)
            {
                r.set_Value(r.Fields.FindField("sourcepdf"),pdf);
                r.Store();
                r = c.NextRow();
            }
            if (_Editor.HasEdits())
            {
                _Editor.StopOperation("Update Report Log PDF");
            }
            //wkspedit.HasEdits(true);
            //wkspedit.StopEditOperation();
            StopEditing(false);
        }
        public void AppendResourceReportEvent(int reportid, int resourceid)
        {
            StartEditing(esriMultiuserEditSessionMode.esriMESMVersioned);
            IRow resrepevent = _VersionedSiteReportEvents.AddNew();
            resrepevent.set_Value(resrepevent.Fields.FindField("reportid"), reportid);
            resrepevent.set_Value(resrepevent.Fields.FindField("resourceid"), resourceid);
            resrepevent.Store();
            _VersionedSiteReportEvents.Add(resrepevent);
            StopEditing(false);
        }
        public void AppendResourceXRef(int resourceid, int xrefid, int relationvalue)
        {
            StartEditing(esriMultiuserEditSessionMode.esriMESMVersioned);
            IRow resxref = _VersionedXrefResource.AddNew();
            resxref.set_Value(resxref.Fields.FindField("resourceid"), resourceid);
            resxref.set_Value(resxref.Fields.FindField("xrefid"), xrefid);
            resxref.set_Value(resxref.Fields.FindField("xrefcode"), relationvalue);
            resxref.Store();
            _VersionedXrefResource.Add(resxref);
            StopEditing(false);
        }
        public void AppendReportXRef(int reportid, int xrefid, int relationvalue)
        {
            StartEditing(esriMultiuserEditSessionMode.esriMESMVersioned);
            IRow repxref = _VersionedXrefReport.AddNew();
            repxref.set_Value(repxref.Fields.FindField("reportid"), reportid);
            repxref.set_Value(repxref.Fields.FindField("xrefid"), xrefid);
            repxref.set_Value(repxref.Fields.FindField("xrefcode"), relationvalue);
            repxref.Store();
            _VersionedXrefReport.Add(repxref);
            StopEditing(false);
        }
        public void DeleteResourceReportEvent(int resourceid, int reportid)
        {
            StartEditing(esriMultiuserEditSessionMode.esriMESMVersioned);
            // IWorkspaceEdit2 wkspedit = (IWorkspaceEdit2)_MUEditor;
            //wkspedit.StartEditOperation();
            _Editor.StartOperation();
            ITable t =  _workerBee.GetStandAloneTable("pgelibrary.geo.resourcereportevents").Table;
            IRow r;
           
            IQueryFilter2 qf = new QueryFilterClass();
            qf.WhereClause = "reportid = " + reportid + " AND resourceid =" + resourceid;
            qf.SubFields = "*";
            ICursor c = t.Search(qf, false);
            r = c.NextRow();
            int objid = 0;
            while (r!= null)
            {
                objid = (int)r.get_Value(r.Fields.FindField("objectid"));
                r = c.NextRow();
            }
             _VersionedSiteReportEvents.Remove(t.GetRow(objid));
           
           
            t.DeleteSearchedRows(qf);
            if (_Editor.HasEdits())
            {
                _Editor.StopOperation("Delete Resource Report Event PDF");
            }
            StopEditing(false);
        }
        public void DeleteResourceXRef(int resourceid, int xrefid, int relationvalue)
        {
            StartEditing(esriMultiuserEditSessionMode.esriMESMVersioned);
            //IWorkspaceEdit2 wkspedit = (IWorkspaceEdit2)_MUEditor;
            //wkspedit.StartEditOperation();
            _Editor.StartOperation();
            ITable t = _workerBee.GetStandAloneTable("pgelibrary.geo.xrefsresource").Table;
            IQueryFilter2 qf = new QueryFilterClass();
            qf.WhereClause = "xrefid = " + xrefid + " AND resourceid =" + resourceid + " AND xrefcode = " + relationvalue;
            qf.SubFields = "*";
            IRow r;
            ICursor c = t.Search(qf, false);
            r = c.NextRow();
            int objid = 0;
            while (r != null)
            {
                objid = (int)r.get_Value(r.Fields.FindField("objectid"));
                r = c.NextRow();
            }
            _VersionedXrefResource.Remove(t.GetRow(objid));
            
            t.DeleteSearchedRows(qf);
            if (_Editor.HasEdits())
            {
                _Editor.StopOperation("Delete Resource XRef");
            }
            StopEditing(false);
       
        }
        public void DeleteReportXRef(int reportid, int xrefid, int relationvalue)
        {
            StartEditing(esriMultiuserEditSessionMode.esriMESMVersioned);
            _Editor.StartOperation();
            ITable t = _workerBee.GetStandAloneTable("pgelibrary.geo.xrefsreport").Table;
            IQueryFilter2 qf = new QueryFilterClass();
            qf.WhereClause = "xrefid = " + xrefid + " AND reportid =" + reportid + " AND xrefcode = " + relationvalue;
            qf.SubFields = "*";
            IRow r;
            ICursor c = t.Search(qf, false);
            r = c.NextRow();
            int objid = 0;
            while (r != null)
            {
                objid = (int)r.get_Value(r.Fields.FindField("objectid"));
                r = c.NextRow();
            }
            _VersionedXrefReport.Remove(t.GetRow(objid));
            if (_Editor.HasEdits())
            {
                _Editor.StopOperation("Delete Report XRef");
            }
            t.DeleteSearchedRows(qf);
            StopEditing(false);
        }
        public void UpdateResourceReportEvent(int id, int updateid, bool resource)
        {
            _Editor.StartOperation();
            ITable t = _workerBee.GetStandAloneTable("pgelibrary.geo.resourcereportevents").Table;
            IRow r;

            IQueryFilter2 qf = new QueryFilterClass();
            if (resource)
            {
                qf.WhereClause = " resourceid =" + id;
            } else
            {
                qf.WhereClause = " reportid =" + id;
            }
            
            qf.SubFields = "*";
            ICursor c = t.Search(qf, false);
            r = c.NextRow();
            int objid = 0;
            while (r != null)
            {
                if (resource)
                {
                    r.set_Value(r.Fields.FindField("resourceid"), updateid);
                } else
                {
                    r.set_Value(r.Fields.FindField("reportid"), updateid);
                }
                r.Store();
                r = c.NextRow();
            }
            if (_Editor.HasEdits())
            {
                _Editor.StopOperation("Update Resource Report Event");
            }
        }
        public void UpdateResourceXRef(int resourceid, int updateid)
        {
            _Editor.StartOperation();
            ITable t = _workerBee.GetStandAloneTable("pgelibrary.geo.xrefsresource").Table;
            IQueryFilter2 qf = new QueryFilterClass();
            qf.WhereClause = "resourceid =" + resourceid ;
            qf.SubFields = "*";
            IRow r;
            ICursor c = t.Search(qf, false);
            r = c.NextRow();
            int objid = 0;
            while (r != null)
            {
                r.set_Value(r.Fields.FindField("resourceid"), updateid);
                    r.Store();
                r = c.NextRow();
            }
            if (_Editor.HasEdits())
            {
                _Editor.StopOperation("Update Resource XRef");
            }
            

        }
        public void UpdateReportXRef(int reportid, int updateid)
        {   _Editor.StartOperation();
            ITable t = _workerBee.GetStandAloneTable("pgelibrary.geo.xrefreport").Table;
            IQueryFilter2 qf = new QueryFilterClass();
            qf.WhereClause = "reportid =" + reportid;
            qf.SubFields = "*";
            IRow r;
            ICursor c = t.Search(qf, false);
            r = c.NextRow();
            int objid = 0;
            while (r != null)
            {
                r.set_Value(r.Fields.FindField("reportid"),updateid);
                r.Store();
                r = c.NextRow();
            }
            if (_Editor.HasEdits())
            {
                _Editor.StopOperation("Update Report XRef");
            }
        }
        private void UpdateResourceLogDeleted(int resourceid)
        {
            _Editor.StartOperation();
            ITable t = _workerBee.GetStandAloneTable("pgelibrary.geo.batchresource").Table;
            IRow r;
            string filepath = "";
            IQueryFilter2 qf = new QueryFilterClass();
            qf.WhereClause = "resourceid = " + resourceid ;
            qf.SubFields = "*";
            ICursor c = t.Search(qf, false);
            r = c.NextRow();
            int objid = 0;
            while (r != null)
            {
                r.set_Value(r.Fields.FindField("deleted"),true);
                r.set_Value(r.Fields.FindField("deletedate"),DateTime.Now);
                r.Store();
                r = c.NextRow();
            }
            _Editor.StopOperation("Update Resource Log as Deleted");

        }
        private void UpdateReportLogDeleted(int reportid)
        {
            _Editor.StartOperation();
            ITable t = _workerBee.GetStandAloneTable("pgelibrary.geo.batchreport").Table;
            IRow r;
            string filepath = "";
            IQueryFilter2 qf = new QueryFilterClass();
            qf.WhereClause = "reportid = " + reportid;
            qf.SubFields = "*";
            ICursor c = t.Search(qf, false);
            r = c.NextRow();
            int objid = 0;
            while (r != null)
            {
                r.set_Value(r.Fields.FindField("deleted"), true);
                r.set_Value(r.Fields.FindField("deletedate"), DateTime.Now);
                r.Store();
                r = c.NextRow();
            }
            _Editor.StopOperation("Update Report Log as Deleted");

        }
        private void DeleteResourceGIS(int resourceid)
        {
            //StartEditing(esriMultiuserEditSessionMode.esriMESMVersioned);
            // IWorkspaceEdit2 wkspedit = (IWorkspaceEdit2)_MUEditor;
            //wkspedit.StartEditOperation();
            _Editor.StartOperation();
            FeatureLayerList featureLayers = _workerBee.GetLayers(null, _BatchWorkspace, null);
            IFeatureLayer fl;
            IFeature feature;
            IFeatureCursor featureCursor;
            IQueryFilter2 qf = new QueryFilterClass();
            qf.WhereClause = "resourceid =" + resourceid;
            qf.SubFields = "*";
            if (featureLayers.IndexOfName(PGELibraryAddin.Properties.Settings.Default.BatchResourcePoly) != -1)
            {
                fl = featureLayers.FeatureLayerByName(PGELibraryAddin.Properties.Settings.Default.BatchResourcePoly);
                featureCursor = fl.FeatureClass.Search(qf, false);
                while ((feature = featureCursor.NextFeature()) != null)
                {
                    feature.Delete();
                }
            }
            if (featureLayers.IndexOfName(PGELibraryAddin.Properties.Settings.Default.BatchResourceLinear) != -1)
            {
                fl = featureLayers.FeatureLayerByName(PGELibraryAddin.Properties.Settings.Default.BatchResourcePoly);
                featureCursor = fl.FeatureClass.Search(qf, false);
                while ((feature = featureCursor.NextFeature()) != null)
                {
                    feature.Delete();
                }
            }
            if (featureLayers.IndexOfName(PGELibraryAddin.Properties.Settings.Default.BatchResourcePoint) != -1)
            {
                fl = featureLayers.FeatureLayerByName(PGELibraryAddin.Properties.Settings.Default.BatchResourcePoly);
                featureCursor = fl.FeatureClass.Search(qf, false);
                while ((feature = featureCursor.NextFeature()) != null)
                {
                    feature.Delete();
                }
            }
            _Editor.StopOperation("Delete Resource GIS");
        }
        private void DeleteReportGIS(int reportid)
        {
            //StartEditing(esriMultiuserEditSessionMode.esriMESMVersioned);
            // IWorkspaceEdit2 wkspedit = (IWorkspaceEdit2)_MUEditor;
            //wkspedit.StartEditOperation();
            _Editor.StartOperation();
            FeatureLayerList featureLayers = _workerBee.GetLayers(null, _BatchWorkspace, null);
            IFeatureLayer fl;
            IFeature feature;
            IFeatureCursor featureCursor;
            IQueryFilter2 qf = new QueryFilterClass();
            qf.WhereClause = "reportid =" + reportid;
            qf.SubFields = "*";
            if (featureLayers.IndexOfName(PGELibraryAddin.Properties.Settings.Default.BatchReportPoly) != -1)
            {
                fl = featureLayers.FeatureLayerByName(PGELibraryAddin.Properties.Settings.Default.BatchReportPoly);
                featureCursor = fl.FeatureClass.Search(qf, false);
                while ((feature = featureCursor.NextFeature()) != null)
                {
                    feature.Delete();
                }
            }
            if (featureLayers.IndexOfName(PGELibraryAddin.Properties.Settings.Default.BatchReportLinear) != -1)
            {
                fl = featureLayers.FeatureLayerByName(PGELibraryAddin.Properties.Settings.Default.BatchReportLinear);
                featureCursor = fl.FeatureClass.Search(qf, false);
                while ((feature = featureCursor.NextFeature()) != null)
                {
                    feature.Delete();
                }
            }
            if (featureLayers.IndexOfName(PGELibraryAddin.Properties.Settings.Default.BatchReportPoint) != -1)
            {
                fl = featureLayers.FeatureLayerByName(PGELibraryAddin.Properties.Settings.Default.BatchReportPoint);
                featureCursor = fl.FeatureClass.Search(qf, false);
                while ((feature = featureCursor.NextFeature()) != null)
                {
                    feature.Delete();
                }
            }
            _Editor.StopOperation("Delete Report GIS");
        }

        #endregion
        //      public ITable CreateFetchTempTable(String FeatureClassName, string inTemplateTable)
        //{
        //	//duplicates table to temporary table
        //	IFeatureWorkspace fwksp = (IFeatureWorkspace)GetTempWorkspace();
        //	ITable tbl;
        //	if (_TempWorkspace.get_NameExists(ESRI.ArcGIS.Geodatabase.esriDatasetType.esriDTTable, FeatureClassName))
        //	{
        //		//table with that name already exists return that table 
        //		tbl = fwksp.OpenTable(FeatureClassName);

        //	}
        //	else
        //	{
        //		//does not exist so we'll create
        //		IObjectClassDescription ocDesc = new ObjectClassDescriptionClass();
        //		IFieldChecker fldChk = new FieldCheckerClass();
        //		IEnumFieldError enumFieldError = null;
        //		IFields validatedFields = null;
        //		fldChk.ValidateWorkspace = (IWorkspace)_TempWorkspace;
        //		fldChk.Validate(_workerBee.GetStandAloneTable(inTemplateTable).Table.Fields, out enumFieldError, out validatedFields);
        //		tbl = fwksp.CreateTable(FeatureClassName, validatedFields, ocDesc.InstanceCLSID, null, "");
        //		AddGlobalID(tbl, "tempid");
        //	}

        //	IStandaloneTable pStTab;
        //	IStandaloneTableCollection pStTabColl = (IStandaloneTableCollection)ArcMap.Document.FocusMap;

        //	pStTab = new StandaloneTable();
        //	pStTab.Table = tbl;
        //	pStTabColl.AddStandaloneTable(pStTab);

        //	//refresh the TOC
        //	ArcMap.Document.UpdateContents();
        //	return tbl;
        //}

        //public void AddGlobalID(ITable table, String globalIdFieldName)
        //{
        //	//try to acquire an exclusive schema lock.
        //	ISchemaLock schemaLock = (ISchemaLock)table;
        //	try
        //	{
        //		schemaLock.ChangeSchemaLock(esriSchemaLock.esriExclusiveSchemaLock);

        //		//add the GlobalID field.
        //		IClassSchemaEdit3 classSchemaEdit3 = (IClassSchemaEdit3)table;
        //		classSchemaEdit3.AddGlobalID(globalIdFieldName);
        //	}
        //	catch (COMException comExc)
        //	{
        //		//handle the exception in a way appropriate to the application.
        //	}
        //	finally
        //	{
        //		//demote the exclusive lock to a shared lock.
        //		schemaLock.ChangeSchemaLock(esriSchemaLock.esriSharedSchemaLock);
        //	}
        //}

        //private static string ValidTableName(string name)
        //{
        //	string invalidChars = System.Text.RegularExpressions.Regex.Escape(new string(System.IO.Path.GetInvalidFileNameChars()));
        //	string invalidRegStr = string.Format(@"([{0}]*\.+$)|([{0}]+)", invalidChars);
        //	string valid1 = System.Text.RegularExpressions.Regex.Replace(name, invalidRegStr, "_");

        //	return valid1.Replace(" ", "_");
        //}
    }
}