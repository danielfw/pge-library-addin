﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PGELibraryAddin
{
    public partial class LookupForm : Form
    {

        string _sqlString;
        int _relationKey;

        public enum LookupType { ReportForResource, ResourceForReport, ResourceXRef, ReportXRef, PersonnelForMPE, ResourceForMPE};
        LookupType _formPurpose;
        BatchData _bd;

        public LookupForm()
        {
            InitializeComponent();
        }
        public BatchData bd
        {
            set { _bd = value; }
        }


        public int RelationKey
        {
            set { _relationKey = value; }
        }
        public string SQLSource
        {
            get { return _sqlString; }
            set { _sqlString = value; }
        }
        public LookupType FormPurpose
        {
            get { return _formPurpose; }
            set { _formPurpose = value; }
        }
        public string InstructionText
        {
            set { label1.Text = value; }
        }
        public bool RelationshipVisible
        {
            set
            {
                labelRelationship.Visible = value;
                cboRelationship.Visible = value;
            }
        }
        public string RelationshipSource
        {
            set
            {
                //cboRelationship.d
            }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            DataSet dsReports = new DataSet();
            DataTable dtReports = new DataTable();
            NpgsqlConnection conn = new NpgsqlConnection(PGELibraryAddin.Properties.Settings.Default.pgsqlConnectionString);
            conn.Open();
            NpgsqlCommand cmdV = new NpgsqlCommand("SELECT sde.sde_set_current_version('" + _bd.VersionName().Replace(_bd.WorkspaceUser(), string.Empty) + "');", conn);
            cmdV.ExecuteNonQuery();
            NpgsqlCommand cmd = new NpgsqlCommand(_sqlString,conn);
            AddCommandParameters(cmd);
            NpgsqlDataAdapter da = new NpgsqlDataAdapter(cmd);
            dsReports.Reset();
            da.Fill(dsReports);
            dtReports.Reset();
            dtReports = dsReports.Tables[0];
            dgvList.DataSource = dtReports;
            conn.Close();
        }
        private void AddCommandParameters(NpgsqlCommand cmd)
        {
           switch (_formPurpose)
            {
                case LookupType.ResourceForReport:
                    cmd.Parameters.AddWithValue("@p1", "%" + SearchBox.Text + "%");
                    cmd.Parameters.AddWithValue("@p2", "%" + SearchBox.Text + "%");
                    cmd.Parameters.AddWithValue("@p3", "%" + SearchBox.Text + "%");
                    cmd.Parameters.AddWithValue("@p4", "%" + SearchBox.Text + "%");
                    cmd.Parameters.AddWithValue("@p5", "%" + SearchBox.Text + "%");
                    break;
                case LookupType.ResourceXRef:
                    cmd.Parameters.AddWithValue("@p1", "%" + SearchBox.Text + "%");
                    cmd.Parameters.AddWithValue("@p2", "%" + SearchBox.Text + "%");
                    cmd.Parameters.AddWithValue("@p3", "%" + SearchBox.Text + "%");
                    cmd.Parameters.AddWithValue("@p4", "%" + SearchBox.Text + "%");
                    cmd.Parameters.AddWithValue("@p5", "%" + SearchBox.Text + "%");
                    break;
                case LookupType.ReportForResource:
                    cmd.Parameters.AddWithValue("@p1", "%" + SearchBox.Text + "%");
                    cmd.Parameters.AddWithValue("@p2", "%" + SearchBox.Text + "%");
                    cmd.Parameters.AddWithValue("@p3", "%" + SearchBox.Text + "%");
                    cmd.Parameters.AddWithValue("@p4", "%" + SearchBox.Text + "%");
                    cmd.Parameters.AddWithValue("@p5", "%" + SearchBox.Text + "%");
                    break;
                case LookupType.ReportXRef:
                    cmd.Parameters.AddWithValue("@p1", "%" + SearchBox.Text + "%");
                    cmd.Parameters.AddWithValue("@p2", "%" + SearchBox.Text + "%");
                    cmd.Parameters.AddWithValue("@p3", "%" + SearchBox.Text + "%");
                    cmd.Parameters.AddWithValue("@p4", "%" + SearchBox.Text + "%");
                    cmd.Parameters.AddWithValue("@p5", "%" + SearchBox.Text + "%");
                    break;
                case LookupType.PersonnelForMPE:
                    cmd.Parameters.AddWithValue("@p1", "%" + SearchBox.Text + "%");
                    cmd.Parameters.AddWithValue("@p2", "%" + SearchBox.Text + "%");
                    cmd.Parameters.AddWithValue("@p3", "%" + SearchBox.Text + "%");
                    cmd.Parameters.AddWithValue("@p4", "%" + SearchBox.Text + "%");
                    break;
                case LookupType.ResourceForMPE:
                    cmd.Parameters.AddWithValue("@p1", "%" + SearchBox.Text + "%");
                    cmd.Parameters.AddWithValue("@p2", "%" + SearchBox.Text + "%");
                    cmd.Parameters.AddWithValue("@p3", "%" + SearchBox.Text + "%");
                    cmd.Parameters.AddWithValue("@p4", "%" + SearchBox.Text + "%");
                    cmd.Parameters.AddWithValue("@p5", "%" + SearchBox.Text + "%");
                    break;

            }
        }

        private void LookupForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'pgeresourcesDataSet.lookupresourcexrefs' table. You can move, or remove it, as needed.
            this.lookupresourcexrefsTableAdapter.Fill(this.pgeresourcesDataSet.lookupresourcexrefs);

        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (AddRelation(true) == true)
            {
                this.Close();
            }

        }
        private bool AddRelation(Boolean OpenForm)
        {

            if (dgvList.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please select a row","Error",MessageBoxButtons.OK);
                return false;
            }
            string UpdateString;
            int AddKey;
            int NewKey;
            // NpgsqlCommand cmd = new NpgsqlCommand();
            // NpgsqlConnection conn = new NpgsqlConnection(PGELibraryAddin.Properties.Settings.Default.pgsqlConnectionString);
            // conn.Open();

            //check
            switch (_formPurpose)
            {
                case LookupType.ResourceForReport:
                    AddKey = Convert.ToInt32(dgvList.SelectedRows[0].Cells["resourceid"].Value);
                    _bd.AppendResourceReportEvent(_relationKey, AddKey);

                    break;
                case LookupType.ResourceXRef:
                    //check that relation has been selected
                    if (cboRelationship.SelectedText == "")
                    {
                        MessageBox.Show("Please select a relationship","No relation selected",MessageBoxButtons.OK);
                        return false;
                    }
                    AddKey = Convert.ToInt32(dgvList.SelectedRows[0].Cells["resourceid"].Value);
                    _bd.AppendResourceXRef(_relationKey, AddKey, Convert.ToInt32(cboRelationship.SelectedValue));
                    break;
                case LookupType.ReportForResource:
                    AddKey = Convert.ToInt32(dgvList.SelectedRows[0].Cells["reportid"].Value);
                    _bd.AppendResourceReportEvent(AddKey,_relationKey);
                    break;
                case LookupType.ReportXRef:
                    if (cboRelationship.SelectedText == "")
                    {
                        MessageBox.Show("Please select a relationship","No Relation Selected", MessageBoxButtons.OK);
                        return false;
                    }
                    AddKey = Convert.ToInt32(dgvList.SelectedRows[0].Cells["reportid"].Value);
                    _bd.AppendReportXRef(_relationKey, AddKey, Convert.ToInt32(cboRelationship.SelectedValue));
                    break;
                case LookupType.PersonnelForMPE:
                    AddKey = Convert.ToInt32(dgvList.SelectedRows[0].Cells["objectid"].Value);
                    _bd.AppendMPEPersonnel(_relationKey, AddKey, dgvList.SelectedRows[0].Cells["person"].Value.ToString(), dgvList.SelectedRows[0].Cells["company"].Value.ToString());
                    break;
                case LookupType.ResourceForMPE:
                    AddKey = Convert.ToInt32(dgvList.SelectedRows[0].Cells["resourceid"].Value);
                    
                    NewKey = _bd.AppendMPEResource(_relationKey, AddKey, dgvList.SelectedRows[0].Cells["primarylabel"].Value.ToString(),
                        dgvList.SelectedRows[0].Cells["trinomiallabel"].Value.ToString(),
                        dgvList.SelectedRows[0].Cells["fslabel"].Value.ToString(),
                        dgvList.SelectedRows[0].Cells["othername"].Value.ToString());
                    if (OpenForm)
                    {
                        //open resource detail with Resourceid to add details
                        FormMPEResourceDetail fres = new FormMPEResourceDetail();
                        fres.batchData = _bd;
                        
                        fres.ResourceLabel = Convert.ToString(dgvList.SelectedRows[0].Cells["primarylabel"].Value);
                        fres.ShowDialog();
                    }
                    break;
            }
            return true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            AddRelation(false);
        }
    }

}
