﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Runtime.InteropServices;
using ESRI.ArcGIS.Geodatabase;
using NLog;

namespace ArcDataBinding
{
	/// <summary>
	/// This class provides a wrapper for an ITable that allows it to be bound to
	/// a .NET control.
	/// </summary>
	/// <remarks>
	/// This class inherits from <see cref="BindingList"/> to provide a default
	/// implementation of a list of objects that can be bound to a .NET control.
	/// For the purposes of this sample, it is easier to use BindingList and add
	/// IRows to it than it is to implement all the interfaces required for a 
	/// bindable list. A more correct implementation would allow direct access to
	/// the wrapped ITable rather than simply adding all of its rows to a list.
	/// The class also implements <see cref="ITypedList"/> to allow a control to
	/// query it for any properties required to correctly display the data in a 
	/// control. Normally properties are determined by using reflection. We want
	/// the individual fields in the given ITable to look like properties of an
	/// IRow. As this is not the case, we need to create a collection of 'fake'
	/// properties with one for each field in the ITable. This is contained in the
	/// fakePropertiesList member and is used by the ITypedList implementation.
	/// </remarks>
	[Guid("5a239147-b06a-49e5-aa1c-e47f81adc10e")]
	[ClassInterface(ClassInterfaceType.None)]
	[ProgId("ArcDataBinding.TableWrapper")]
	public class TableWrapperFW : BindingListView<IRow>, ITypedList
	{
		#region Private Members
		/// <summary>
		/// Reference to the table we are wrapping
		/// </summary>
		private ITable wrappedTable;
        private bindingli
		/// <summary>
		/// This is a list of <see cref="PropertyDescriptor"/> instances with each one
		/// representing one field of the wrapped ITable.
		/// </summary>
		private List<PropertyDescriptor> fakePropertiesList = new List<PropertyDescriptor>();

		/// <summary>
		/// Used to start and stop editing when adding/updating/deleting rows
		/// </summary>
		private IWorkspaceEdit wkspcEdit;
		#endregion Private Members

		#region Construction/Destruction
		/// <summary>
		/// This constructor stores a reference to the wrapped ITable and uses it to
		/// generate a list of properties before adding the ITable's data to the binding
		/// list.
		/// </summary>
		/// <param name="tableToWrap">ITable that we wish to bind to .NET controls</param>
		public TableWrapperFW(ITable tableToWrap, IQueryFilter qf)
		{
			wrappedTable = tableToWrap;
			GenerateFakeProperties();
			AddData(qf);
			wkspcEdit = ((IDataset)wrappedTable).Workspace as IWorkspaceEdit;
			AllowNew = true;
			AllowRemove = true;
		}
		#endregion Construction/Destruction

		#region ITypedList Members

		/// <summary>
		/// Returns the <see cref="T:System.ComponentModel.PropertyDescriptorCollection"></see> 
		/// that represents the properties on each item used to bind data.
		/// </summary>
		/// <param name="listAccessors">An array of <see cref="T:System.ComponentModel.PropertyDescriptor"></see> 
		/// objects to find in the collection as bindable. This can be null.</param>
		/// <returns>
		/// The <see cref="T:System.ComponentModel.PropertyDescriptorCollection"></see> 
		/// that represents the properties on each item used to bind data.
		/// </returns>
		public PropertyDescriptorCollection GetItemProperties(PropertyDescriptor[] listAccessors)
		{
			PropertyDescriptorCollection propCollection = null;
			if (null == listAccessors)
			{
				// Return all properties
				propCollection = new PropertyDescriptorCollection(fakePropertiesList.ToArray());
			}
			else
			{
				// Return the requested properties by checking each item in listAccessors
				// to make sure it exists in our property collection.
				List<PropertyDescriptor> tempList = new List<PropertyDescriptor>();
				foreach (PropertyDescriptor curPropDesc in listAccessors)
				{
					if (fakePropertiesList.Contains(curPropDesc))
					{
						tempList.Add(curPropDesc);
					}
				}
				propCollection = new PropertyDescriptorCollection(tempList.ToArray());
			}

			return propCollection;
		}

		/// <summary>
		/// Returns the name of the list.
		/// </summary>
		/// <param name="listAccessors">An array of <see cref="T:System.ComponentModel.PropertyDescriptor"></see> 
		/// objects, the list name for which is returned. This can be null.</param>
		/// <returns>The name of the list.</returns>
		public string GetListName(PropertyDescriptor[] listAccessors)
		{
			return ((IDataset)wrappedTable).Name;
		}
		#endregion ITypedList Members

		public bool UseCVDomains
		{
			set
			{
				foreach (FieldPropertyDescriptor curPropDesc in fakePropertiesList)
				{
					if (curPropDesc.HasCVDomain)
					{
						// Field has a coded value domain so turn the usage of this on or off
						// as requested
						curPropDesc.UseCVDomain = value;
					}
				}
			}
		}

		#region Protected Overrides
		/// <summary>
		/// Raises the <see cref="E:System.ComponentModel.BindingList`1.AddingNew"></see> event.
		/// </summary>
		/// <remarks>
		/// This override sets the NewObject property of the event arguments parameter
		/// to be a new IRow.
		/// </remarks>
		/// <param name="e">An <see cref="T:System.ComponentModel.AddingNewEventArgs"></see> 
		/// that contains the event data.</param>
		protected override void OnAddingNew(AddingNewEventArgs e)
		{
			// Check that we can still add rows, this property could have been changed
			if (AllowNew)
			{
				bool weStartedEditing = StartEditOp();

				// Need to create a new IRow
				IRow newRow = wrappedTable.CreateRow();
				e.NewObject = newRow;

				// Loop through fields and set default values
				for (int fieldCount = 0; fieldCount < newRow.Fields.FieldCount; fieldCount++)
				{
					IField curField = newRow.Fields.get_Field(fieldCount);
					if (curField.Editable)
					{
						newRow.set_Value(fieldCount, curField.DefaultValue);
					}
				}

				// Save default values
				newRow.Store();
				StopEditOp(weStartedEditing);

				base.OnAddingNew(e);
			}
		}

		/// <summary>
		/// Removes the item at the specified index.
		/// </summary>
		/// <remarks>
		/// This override calls the Delete method of the IRow that is being removed
		/// </remarks>
		/// <param name="index">The zero-based index of the item to remove.</param>
		protected override void RemoveItem(int index)
		{
			// Check that we can still delete rows, this property could have been changed
			if (AllowRemove)
			{
				// Get the corresponding IRow
				IRow itemToRemove = Items[index];

				bool weStartedEditing = StartEditOp();

				// Delete the row
				itemToRemove.Delete();

				StopEditOp(weStartedEditing);

				base.RemoveItem(index);
			}
		}

		#endregion Protected Overrides

		#region Private Methods
		/// <summary>
		/// Generates 'fake' properties.
		/// </summary>
		/// <remarks>
		/// We need this method to create a list of properties for each field in the
		/// ITable as an IRow does not have a property for each field.
		/// </remarks>
		private void GenerateFakeProperties()
		{
			// Loop through fields in wrapped table
			for (int fieldCount = 0; fieldCount < wrappedTable.Fields.FieldCount; fieldCount++)
			{
				// Create a new property descriptor to represent the field
				FieldPropertyDescriptor newPropertyDesc = new FieldPropertyDescriptor(
				  wrappedTable, wrappedTable.Fields.get_Field(fieldCount).Name, fieldCount);
				fakePropertiesList.Add(newPropertyDesc);
			}

		}

		/// <summary>
		/// Adds the data to the binding list.
		/// </summary>
		/// <remarks>
		/// Note that this is a pretty inefficient way of accessing the data to be
		/// bound to a control. If we implemented each of the interfaces required for
		/// a bindable list rather than using BindingList, we could write code that
		/// only reads rows from the ITable as they need to be displayed rather than
		/// reading all of them.
		/// </remarks>
		private void AddData(IQueryFilter qf)
		{
			// Get a search cursor that returns all rows. Note we do not want to recycle the returned IRow otherwise all rows in the bound control will be identical to the last row read...
			ICursor cur = wrappedTable.Search(qf, false);
            int rowcount = 0;
			IRow curRow = cur.NextRow();
			while (null != curRow)
			{
				Add(curRow);
                rowcount++;
				curRow = cur.NextRow();
			}
            if (rowcount == 0)
            {
                Console.WriteLine ("No rows found");
            }
		}

		/// <summary>
		/// Starts an edit operation.
		/// </summary>
		/// <remarks>
		/// This method is used to start an edit operation before changing any data.
		/// It checks to see if we are in an edit session or not and starts a new
		/// one if appropriate. If we do start an edit session, the method will return
		/// true to indicate that we started an edit session and should therefore also
		/// stop it.
		/// </remarks>
		/// <returns>True if we started an edit session, false if we didn't</returns>
		private bool StartEditOp()
		{
			bool retVal = false;

			// Check to see if we're editing
			if (!wkspcEdit.IsBeingEdited())
			{
				// Not being edited so start here
				wkspcEdit.StartEditing(false);
				retVal = true;
			}

			// Start operation
			wkspcEdit.StartEditOperation();
			return retVal;
		}

		/// <summary>
		/// Stops the edit operation.
		/// </summary>
		/// <remarks>
		/// This method stops an edit operation started with a call to 
		/// <see cref="StartEditOp"/>. If the weStartedEditing parameter is true, this
		/// method will also end the edit session.
		/// </remarks>
		/// <param name="weStartedEditing">if set to <c>true</c> [we started editing].</param>
		private void StopEditOp(bool weStartedEditing)
		{
			// Stop edit operation
			wkspcEdit.StopEditOperation();

			if (weStartedEditing)
			{
				// We started the edit session so stop it here
				wkspcEdit.StopEditing(true);
			}
		}
		#endregion

		//#region Sorting
		private bool _isSorted;
		private ListSortDirection _sortDirection = ListSortDirection.Ascending;
		private PropertyDescriptor _sortProperty;
		private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

		/// <summary>
		/// Gets a value indicating whether the list is sorted.
		/// </summary>
		protected override bool IsSortedCore
		{
			get { return _isSorted; }
		}

		/// <summary>
		/// Gets the direction the list is sorted.
		/// </summary>
		protected override ListSortDirection SortDirectionCore
		{
			get { return _sortDirection; }
		}

		/// <summary>
		/// Gets the property descriptor that is used for sorting the list if sorting is implemented in a derived class; otherwise, returns null
		/// </summary>
		protected override PropertyDescriptor SortPropertyCore
		{
			get { return _sortProperty; }
		}

		/// <summary>
		/// Removes any sort applied with ApplySortCore if sorting is implemented
		/// </summary>
		protected override void RemoveSortCore()
		{
			_sortDirection = ListSortDirection.Ascending;
			_sortProperty = null;
			_isSorted = false;
		}

		#region Private variables for Searching
		private ArrayList selectedIndices;
		private int[] returnIndices;

		#endregion Private variables for Searching

		/// <summary>
		/// By default this property is set to false
		/// Set this property to true to indicate searching is implemented on list.
		/// </summary>

		#region Properties for Searching
		protected override bool SupportsSearchingCore
		{
			get
			{
				return true;
			}
		}
		#endregion Properties for Searching

		#region Properties for Sorting
		protected override bool SupportsSortingCore
		{
			get { return true; }
		}

		#endregion Properties for Sorting

		#region Methods for searching

		protected override int FindCore(int startIndex, PropertyDescriptor prop, object key)
		{
			// Get the property info for the specified property.
			PropertyInfo propInfo = typeof(IRow).GetProperty(prop.Name);
			IRow item;
			int found = -1;
			selectedIndices = new ArrayList();
			if (key != null)
			{
				// Loop through the items to see if the key
				// value matches the property value.
				for (int i = 0; i < Count; ++i)
				{
					item = (IRow)Items[i];
					if (propInfo.GetValue(item, null).Equals(key))
					{
						found = 0;
						selectedIndices.Add(i);
					}
				}
			}
			return found;
		}

		/// <summary>
		/// This method returns the indices of the business objects in the list based on the search criteria
		/// </summary>
		/// <param name="property">Name of the property of the Business Object</param>
		/// <param name="key">Value to be search with</param>
		/// <returns></returns>
		public int[] Find(string property, object key)
		{
			// Check the properties for a property with the specified name.
			PropertyDescriptorCollection properties =
				TypeDescriptor.GetProperties(typeof(IRow));
			PropertyDescriptor prop = properties.Find(property, true);
			// If there is not a match, return -1 otherwise pass search to
			// FindCore method.
			if (prop == null)
				returnIndices = null;
			else
			{
				if (FindCore(prop, key) >= 0)
				{
					returnIndices = (int[])(selectedIndices.ToArray(typeof(int)));
				}
			}
			return returnIndices;
		}
        private int PrepWhere(string FilterElements)
        {
            string[] sFilterParts;
            sFilterParts = Strings.Split(FilterElements, "|");
            string sFilterColumn;
            string sFilterValue;
            bool bString;

            int indxFld;
            sFilterColumn = sFilterParts[0];
            sFilterValue = sFilterParts[1];
            bString = System.Convert.ToBoolean(sFilterParts[2]);
            indxFld = wrappedTable.FindField(sFilterColumn);
            string sWhere;
            PropertyDescriptorCollection fldProps = base.GetItemProperties(null/* TODO Change to default(_) if this is not a reference type */);
            FieldPropertyDescriptor fldProp;
            fldProp = fldProps.Find(sFilterColumn, false);
            if (fldProp.PropertyType == typeof(string))
            {
                if (sFilterValue == "")
                    sWhere = "(" + _DelimiterPre + sFilterColumn + _DelimiterPost + " = '' or " + _DelimiterPre + sFilterColumn + _DelimiterPost + " IS Null)";
                else
                    sWhere = _DelimiterPre + sFilterColumn + _DelimiterPost + " LIKE '" + _Wildcard + sFilterValue + _Wildcard + "'";
            }
            else if (sFilterValue == "NULL")
                sWhere = sFilterColumn + " IS " + sFilterValue;
            else if (Strings.Left(sFilterValue, 2) == "IN")
                sWhere = sFilterColumn + " (" + sFilterValue + ")";
            else
                sWhere = _DelimiterPre + sFilterColumn + _DelimiterPost + " = " + sFilterValue;
            // if it was filtered before we'll add this filter
            if (_isFiltered)
                sWhere = _filterValue + " AND " + sWhere;
            return DoSearch(sWhere);
        }
        private int DoSearch(string sWhere)
        {
            IQueryFilter2 pqf;
            pqf = new QueryFilter();

            // set the where clause for our query filter
            pqf.WhereClause = sWhere;
            ICursor cur;
            cur = wrappedTable.Search(pqf, false);
            IRow curRow = cur.NextRow();
            int i;
            // if our where clause is successful then we'll clear the old table and reload
            if (!curRow == null)
            {
                base.Clear();
                _filterValue = sWhere;
                _isFiltered = true;
            }
            // reload
            while (!curRow == null)
            {
                i = i + 1;
                Add(curRow);
                curRow = cur.NextRow();
            }
            // returns the number of records 
            return i;
        }

        #endregion Methods for searching

        #region Methods for Sorting

        /// <summary>
        /// Sorts the items if overridden in a derived class
        /// </summary>
        /// <param name="prop"></param>
        /// <param name="direction"></param>
        protected override void ApplySortCore(System.ComponentModel.PropertyDescriptor prop, System.ComponentModel.ListSortDirection direction)
		{
			try
			{
				// currently sorting will fail on tables that do not have an OBJECTID column perhaps even those not registered with a geodatbase check find filter first
				if (_isSorted)
				{
					if (_sortProperty.Name == prop.Name)
					{
						if (_sortDirection == ListSortDirection.Ascending)
							_sortDirection = ListSortDirection.Descending;
						else
							_sortDirection = ListSortDirection.Ascending;
					}
				}
				else
					_sortDirection = direction;

				_sortProperty = prop;

				Type fldType;
				TableSort wrappedTableSort;
				fldType = prop.PropertyType;
				wrappedTableSort = new TableSort();
				wrappedTableSort.Table = (ITable)wrappedTable;
				wrappedTableSort.Fields = prop.Name;
				if (_sortDirection == ListSortDirection.Descending)
					wrappedTableSort.Ascending[prop.Name] = false;
				else
					wrappedTableSort.Ascending[prop.Name] = true;
				base.Clear();
				wrappedTableSort.Sort(new ESRI.ArcGIS.Carto.TrackCancel());
				ICursor cur = wrappedTableSort.Rows;
				IRow curRow = cur.NextRow();
				while (curRow != null)
				{
					base.Add(curRow);
					curRow = cur.NextRow();
				}
				_isSorted = true;
			}
			catch (Exception ex)
			{
				logger.Log(NLog.LogLevel.Error, string.Format("Exception in {0} reads {1}", System.Reflection.Assembly.GetExecutingAssembly().GetName(), ex.Message));
			}
		}


		private void SwapItems(int fromIndex, int toIndex)
		{
			IRow temp = this[fromIndex];
			this[fromIndex] = this[toIndex];
			this[toIndex] = temp;
		}

		public void ApplySort(string property, ListSortDirection direction)
		{
			PropertyDescriptorCollection properties =
				TypeDescriptor.GetProperties(typeof(IRow));
			PropertyDescriptor prop = properties.Find(property, true);
			if (prop != null)
				ApplySortCore(prop, direction);
			else
				throw new NotSupportedException("Cannot sort by " + prop.Name + ". This" + prop.Name + " does not exist.");
		}

		#endregion Methods for Sorting


	}
}