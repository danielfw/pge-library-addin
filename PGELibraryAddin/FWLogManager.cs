﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using System.Reflection;
using NLog.Config;

namespace PGELibraryAddin
{
    class FWLogManager
    {
        public static readonly LogFactory Instance = new LogFactory(new XmlLoggingConfiguration(GetNLogConfigFilePath()));



        private static string GetNLogConfigFilePath()
        {
            Assembly assemb;
            assemb = Assembly.GetExecutingAssembly();
            AppDomain ad = AppDomain.CurrentDomain;
            ad.ProcessExit += ProcessExitEvent;
            ad.DomainUnload += DomainUnloadEvent;
            return System.IO.Path.GetDirectoryName(assemb.Location) + @"\NLog.config";
        }

        private static void ProcessExitEvent(object sender, EventArgs e)
        {
            FWLogManager.Instance.Configuration = null;
        }

        private static void DomainUnloadEvent(object sender, EventArgs e)
        {
            FWLogManager.Instance.Configuration = null;
        }
    }
}
